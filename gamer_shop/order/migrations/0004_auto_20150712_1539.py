# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0005_auto_20150701_1611'),
        ('computer', '0005_auto_20150701_1833'),
        ('order', '0003_auto_20150701_1833'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComputerLine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('title', models.CharField(verbose_name='Title', max_length=255)),
                ('upc', models.CharField(verbose_name='UPC', max_length=128, blank=True, null=True)),
                ('quantity', models.PositiveIntegerField(verbose_name='Quantity', default=1)),
                ('line_price_incl_tax', models.DecimalField(verbose_name='Price (inc. tax)', decimal_places=2, max_digits=12)),
                ('line_price_excl_tax', models.DecimalField(verbose_name='Price (excl. tax)', decimal_places=2, max_digits=12)),
                ('line_price_before_discounts_incl_tax', models.DecimalField(verbose_name='Price before discounts (inc. tax)', decimal_places=2, max_digits=12)),
                ('line_price_before_discounts_excl_tax', models.DecimalField(verbose_name='Price before discounts (excl. tax)', decimal_places=2, max_digits=12)),
                ('unit_cost_price', models.DecimalField(verbose_name='Unit Cost Price', decimal_places=2, max_digits=12, blank=True, null=True)),
                ('unit_price_incl_tax', models.DecimalField(verbose_name='Unit Price (inc. tax)', decimal_places=2, max_digits=12, blank=True, null=True)),
                ('unit_price_excl_tax', models.DecimalField(verbose_name='Unit Price (excl. tax)', decimal_places=2, max_digits=12, blank=True, null=True)),
                ('unit_retail_price', models.DecimalField(verbose_name='Unit Retail Price', decimal_places=2, max_digits=12, blank=True, null=True)),
                ('status', models.CharField(verbose_name='Status', max_length=255, blank=True)),
                ('est_dispatch_date', models.DateField(verbose_name='Estimated Dispatch Date', blank=True, null=True)),
                ('computer', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, null=True, verbose_name='Computer', to='computer.Computer', blank=True)),
                ('order', models.ForeignKey(verbose_name='Order', to='order.Order', related_name='computer_lines')),
            ],
            options={
                'verbose_name': 'Order Computer Line',
                'verbose_name_plural': 'Order Computer Lines',
            },
        ),
        migrations.CreateModel(
            name='ComputerLineAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('type', models.CharField(verbose_name='Type', max_length=128)),
                ('value', models.CharField(verbose_name='Value', max_length=255)),
                ('line', models.ForeignKey(verbose_name='Line', to='order.ComputerLine', related_name='computer_attributes')),
                ('option', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, related_name='computer_line_attributes', null=True, verbose_name='Option', to='catalogue.Option')),
            ],
            options={
                'verbose_name': 'Line Attribute',
                'verbose_name_plural': 'Line Attributes',
            },
        ),
        migrations.CreateModel(
            name='ComputerLinePrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('quantity', models.PositiveIntegerField(verbose_name='Quantity', default=1)),
                ('price_incl_tax', models.DecimalField(verbose_name='Price (inc. tax)', decimal_places=2, max_digits=12)),
                ('price_excl_tax', models.DecimalField(verbose_name='Price (excl. tax)', decimal_places=2, max_digits=12)),
                ('shipping_incl_tax', models.DecimalField(verbose_name='Shiping (inc. tax)', decimal_places=2, default=0, max_digits=12)),
                ('shipping_excl_tax', models.DecimalField(verbose_name='Shipping (excl. tax)', decimal_places=2, default=0, max_digits=12)),
                ('line', models.ForeignKey(verbose_name='Line', to='order.ComputerLine', related_name='computer_prices')),
                ('order', models.ForeignKey(verbose_name='Option', to='order.Order', related_name='computer_line_prices')),
            ],
            options={
                'verbose_name': 'Computer Line Price',
                'ordering': ('id',),
                'verbose_name_plural': 'Computer Line Prices',
            },
        ),
        migrations.CreateModel(
            name='ComputerPaymentEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('amount', models.DecimalField(verbose_name='Amount', decimal_places=2, max_digits=12)),
                ('reference', models.CharField(verbose_name='Reference', max_length=128, blank=True)),
                ('date_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('event_type', models.ForeignKey(verbose_name='Event Type', to='order.PaymentEventType')),
            ],
            options={
                'verbose_name': 'Payment Event',
                'ordering': ['-date_created'],
                'verbose_name_plural': 'Payment Events',
            },
        ),
        migrations.CreateModel(
            name='ComputerPaymentEventQuantity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('quantity', models.PositiveIntegerField(verbose_name='Quantity')),
                ('event', models.ForeignKey(verbose_name='Event', to='order.ComputerPaymentEvent', related_name='computer_line_quantities')),
                ('line', models.ForeignKey(verbose_name='Computer Line', to='order.ComputerLine', related_name='computer_payment_event_quantities')),
            ],
            options={
                'verbose_name': 'Payment Event Quantity',
                'verbose_name_plural': 'Payment Event Quantities',
            },
        ),
        migrations.CreateModel(
            name='ComputerShippingEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('notes', models.TextField(verbose_name='Event notes', help_text='This could be the dispatch reference, or a tracking number', blank=True)),
                ('date_created', models.DateTimeField(verbose_name='Date Created', auto_now_add=True)),
                ('event_type', models.ForeignKey(verbose_name='Event Type', to='order.ShippingEventType')),
            ],
            options={
                'verbose_name': 'Shipping Event',
                'ordering': ['-date_created'],
                'verbose_name_plural': 'Shipping Events',
            },
        ),
        migrations.CreateModel(
            name='ComputerShippingEventQuantity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('quantity', models.PositiveIntegerField(verbose_name='Quantity')),
                ('event', models.ForeignKey(verbose_name='Event', to='order.ComputerShippingEvent', related_name='computer_line_quantities')),
                ('line', models.ForeignKey(verbose_name='Computer Line', to='order.ComputerLine', related_name='computer_shipping_event_quantities')),
            ],
            options={
                'verbose_name': 'Shipping Event Quantity',
                'verbose_name_plural': 'Shipping Event Quantities',
            },
        ),
        migrations.AddField(
            model_name='computershippingevent',
            name='lines',
            field=models.ManyToManyField(verbose_name='Computer Lines', related_name='computer_shipping_events', to='order.ComputerLine', through='order.ComputerShippingEventQuantity'),
        ),
        migrations.AddField(
            model_name='computershippingevent',
            name='order',
            field=models.ForeignKey(verbose_name='Order', to='order.Order', related_name='computer_shipping_events'),
        ),
        migrations.AddField(
            model_name='computerpaymentevent',
            name='lines',
            field=models.ManyToManyField(verbose_name='Computer Lines', to='order.ComputerLine', through='order.ComputerPaymentEventQuantity'),
        ),
        migrations.AddField(
            model_name='computerpaymentevent',
            name='order',
            field=models.ForeignKey(verbose_name='Order', to='order.Order', related_name='computer_payment_events'),
        ),
        migrations.AddField(
            model_name='computerpaymentevent',
            name='shipping_event',
            field=models.ForeignKey(null=True, to='order.ComputerShippingEvent', related_name='computer_payment_events'),
        ),
        migrations.AlterUniqueTogether(
            name='computershippingeventquantity',
            unique_together=set([('event', 'line')]),
        ),
        migrations.AlterUniqueTogether(
            name='computerpaymenteventquantity',
            unique_together=set([('event', 'line')]),
        ),
    ]
