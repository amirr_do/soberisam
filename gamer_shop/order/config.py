from oscar.apps.order import config


class OrderConfig(config.OrderConfig):
    name = 'gamer_shop.order'
