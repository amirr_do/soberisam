from oscar.apps.order.utils import *

ComputerLine = get_model('order', 'ComputerLine')


class OrderCreator(OrderCreator):

    def place_order(self, basket, total,  # noqa (too complex (12))
                    shipping_method, shipping_charge, user=None,
                    shipping_address=None, billing_address=None,
                    order_number=None, status=None, **kwargs):
        order = super(OrderCreator, self).place_order(basket=basket, total=total,  # noqa (too complex (12))
                    shipping_method=shipping_method, shipping_charge=shipping_charge, user=user,
                    shipping_address=shipping_address, billing_address=billing_address,
                    order_number=order_number, status=status, **kwargs)

        for computer_line in basket.all_computer_lines():
            self.create_computer_line_models(order, computer_line)
            self.update_computer_stock_records(computer_line)

        return order

    def create_computer_line_models(self, order, basket_computer_line, extra_line_fields=None):
        """
        Create the batch computer line model.

        You can set extra fields by passing a dictionary as the
        extra_line_fields value
        """
        computer = basket_computer_line.computer
        line_data = {
            'order': order,
            # Computer details
            'computer': computer,
            'title': computer.get_title(),
            'upc': computer.upc,
            'quantity': basket_computer_line.quantity,
            # Price details
            'line_price_excl_tax':
                basket_computer_line.line_price_excl_tax_incl_discounts,
            'line_price_incl_tax':
                basket_computer_line.line_price_incl_tax_incl_discounts,
            'line_price_before_discounts_excl_tax':
                basket_computer_line.line_price_excl_tax,
            'line_price_before_discounts_incl_tax':
                basket_computer_line.line_price_incl_tax,
            # Reporting details
            # TODO edit later
            'unit_cost_price': basket_computer_line.unit_price_excl_tax,
            'unit_price_incl_tax': basket_computer_line.unit_price_incl_tax,
            'unit_price_excl_tax': basket_computer_line.unit_price_excl_tax,
            # TODO edit later
            'unit_retail_price': basket_computer_line.unit_price_excl_tax,
            # Shipping details
            'est_dispatch_date':
                basket_computer_line.purchase_info.availability.dispatch_date
        }
        extra_line_fields = extra_line_fields or {}
        if hasattr(settings, 'OSCAR_INITIAL_LINE_STATUS'):
            if not (extra_line_fields and 'status' in extra_line_fields):
                extra_line_fields['status'] = getattr(
                    settings, 'OSCAR_INITIAL_LINE_STATUS')
        if extra_line_fields:
            line_data.update(extra_line_fields)

        order_computer_line = ComputerLine._default_manager.create(**line_data)
        self.create_computer_line_price_models(order, order_computer_line, basket_computer_line)
        self.create_computer_line_attributes(order, order_computer_line, basket_computer_line)
        self.create_additional_line_models(order, order_computer_line, basket_computer_line)

        return order_computer_line

    def create_computer_line_price_models(self, order, order_computer_line, basket_computer_line):
        """
        Creates the batch computer line price models
        """
        breakdown = basket_computer_line.get_price_breakdown()
        for price_incl_tax, price_excl_tax, quantity in breakdown:
            order_computer_line.computer_prices.create(
                order=order,
                quantity=quantity,
                price_incl_tax=price_incl_tax,
                price_excl_tax=price_excl_tax)

    def create_computer_line_attributes(self, order, order_computer_line, basket_computer_line):
        """
        Creates the batch line attributes.
        """
        for attr in basket_computer_line.computer_attributes.all():
            order_computer_line.computer_attributes.create(
                option=attr.option,
                type=attr.option.code,
                value=attr.value)

    def update_computer_stock_records(self, computer_line):
        """
        Update any relevant stock records for this order computer_line
        """
        if computer_line.computer.get_product_class().track_stock:
            pass
            # TODO edit later
            # computer_line.stockrecord.allocate(computer_line.quantity)
