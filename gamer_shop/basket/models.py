from oscar.apps.basket.abstract_models import AbstractBasket
from decimal import Decimal as D
import zlib

from django.db.models import Sum
from django.utils.timezone import now
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.utils.translation import ugettext_lazy as _
from oscar.core.utils import get_default_currency
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from oscar.templatetags.currency_filters import currency
from oscar.apps.offer import results


@python_2_unicode_compatible
class ComputerLine(models.Model):
    """
    A line of a basket (product and a quantity)
    """
    basket = models.ForeignKey('basket.Basket', related_name='computer_lines',
                               verbose_name=_("Basket"))

    # This is to determine which products belong to the same line
    # We can't just use product.id as you can have customised products
    # which should be treated as separate lines.  Set as a
    # SlugField as it is included in the path for certain views.
    line_reference = models.SlugField(
        _("Line Reference"), max_length=128, db_index=True)

    computer = models.ForeignKey(
        'computer.Computer', related_name='basket_lines',
        verbose_name=_("Computer"))

    # We store the stockrecord that should be used to fulfil this line.
    # stockrecord = models.ManyToManyField(
    #     'partner.StockRecord', related_name='basket_lines')

    quantity = models.PositiveIntegerField(_('Quantity'), default=1)

    # We store the unit price incl tax of the product when it is first added to
    # the basket.  This allows us to tell if a product has changed price since
    # a person first added it to their basket.
    price_currency = models.CharField(
        _("Currency"), max_length=12, default=get_default_currency)
    price_excl_tax = models.DecimalField(
        _('Price excl. Tax'), decimal_places=2, max_digits=12,
        null=True)
    price_incl_tax = models.DecimalField(
        _('Price incl. Tax'), decimal_places=2, max_digits=12, null=True)

    # Track date of first addition
    date_created = models.DateTimeField(_("Date Created"), auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(ComputerLine, self).__init__(*args, **kwargs)
        # Instance variables used to persist discount information
        self._discount_excl_tax = D('0.00')
        self._discount_incl_tax = D('0.00')
        self._affected_quantity = 0

    class Meta:
        app_label = 'basket'
        unique_together = ("basket", "line_reference")
        verbose_name = _('Basket computer line')
        verbose_name_plural = _('Basket computer lines')

    def __str__(self):
        return _(
            u"Basket #%(basket_id)d, Computer #%(computer_id)d, quantity"
            u" %(quantity)d") % {'basket_id': self.basket.pk,
                                 'computer_id': self.computer.pk,
                                 'quantity': self.quantity}

    def save(self, *args, **kwargs):
        if not self.basket.can_be_edited:
            raise PermissionDenied(
                _("You cannot modify a %s basket") % (
                    self.basket.status.lower(),))
        return super(ComputerLine, self).save(*args, **kwargs)

    # =============
    # Offer methods
    # =============

    def clear_discount(self):
        """
        Remove any discounts from this line.
        """
        self._discount_excl_tax = D('0.00')
        self._discount_incl_tax = D('0.00')
        self._affected_quantity = 0

    def discount(self, discount_value, affected_quantity, incl_tax=True):
        """
        Apply a discount to this line
        """
        if incl_tax:
            if self._discount_excl_tax > 0:
                raise RuntimeError(
                    "Attempting to discount the tax-inclusive price of a line "
                    "when tax-exclusive discounts are already applied")
            self._discount_incl_tax += discount_value
        else:
            if self._discount_incl_tax > 0:
                raise RuntimeError(
                    "Attempting to discount the tax-exclusive price of a line "
                    "when tax-inclusive discounts are already applied")
            self._discount_excl_tax += discount_value
        self._affected_quantity += int(affected_quantity)

    def consume(self, quantity):
        """
        Mark all or part of the line as 'consumed'

        Consumed items are no longer available to be used in offers.
        """
        if quantity > self.quantity - self._affected_quantity:
            inc = self.quantity - self._affected_quantity
        else:
            inc = quantity
        self._affected_quantity += int(inc)

    def get_price_breakdown(self):
        """
        Return a breakdown of line prices after discounts have been applied.

        Returns a list of (unit_price_incl_tax, unit_price_excl_tax, quantity)
        tuples.
        """
        if not self.is_tax_known:
            raise RuntimeError("A price breakdown can only be determined "
                               "when taxes are known")
        prices = []
        if not self.discount_value:
            prices.append((self.unit_price_incl_tax, self.unit_price_excl_tax,
                           self.quantity))
        else:
            # Need to split the discount among the affected quantity
            # of products.
            item_incl_tax_discount = (
                self.discount_value / int(self._affected_quantity))
            item_excl_tax_discount = item_incl_tax_discount * self._tax_ratio
            item_excl_tax_discount = item_excl_tax_discount.quantize(D('0.01'))
            prices.append((self.unit_price_incl_tax - item_incl_tax_discount,
                           self.unit_price_excl_tax - item_excl_tax_discount,
                           self._affected_quantity))
            if self.quantity_without_discount:
                prices.append((self.unit_price_incl_tax,
                               self.unit_price_excl_tax,
                               self.quantity_without_discount))
        return prices

    # =======
    # Helpers
    # =======

    @property
    def _tax_ratio(self):
        if not self.unit_price_incl_tax:
            return 0
        return self.unit_price_excl_tax / self.unit_price_incl_tax

    # ==========
    # Properties
    # ==========

    @property
    def has_discount(self):
        return self.quantity > self.quantity_without_discount

    @property
    def quantity_with_discount(self):
        return self._affected_quantity

    @property
    def quantity_without_discount(self):
        return int(self.quantity - self._affected_quantity)

    @property
    def is_available_for_discount(self):
        return self.quantity_without_discount > 0

    @property
    def discount_value(self):
        # Only one of the incl- and excl- discounts should be non-zero
        return max(self._discount_incl_tax, self._discount_excl_tax)

    @property
    def purchase_info(self):
        """
        Return the stock/price info
        """
        if not hasattr(self, '_info'):
            # Cache the PurchaseInfo instance.
            self._info = self.basket.strategy.fetch_for_computer(
                self.computer)
        return self._info

    @property
    def is_tax_known(self):
        return self.purchase_info.price.is_tax_known

    @property
    def unit_effective_price(self):
        """
        The price to use for offer calculations
        """
        return self.purchase_info.price.effective_price

    @property
    def unit_price_excl_tax(self):
        return self.purchase_info.price.excl_tax

    @property
    def unit_price_incl_tax(self):
        return self.purchase_info.price.incl_tax

    @property
    def unit_tax(self):
        return self.purchase_info.price.tax

    @property
    def line_price_excl_tax(self):
        return self.quantity * self.unit_price_excl_tax

    @property
    def line_price_excl_tax_incl_discounts(self):
        if self._discount_excl_tax:
            return self.line_price_excl_tax - self._discount_excl_tax
        if self._discount_incl_tax:
            # This is a tricky situation.  We know the discount as calculated
            # against tax inclusive prices but we need to guess how much of the
            # discount applies to tax-exclusive prices.  We do this by
            # assuming a linear tax and scaling down the original discount.
            return self.line_price_excl_tax \
                   - self._tax_ratio * self._discount_incl_tax
        return self.line_price_excl_tax

    @property
    def line_price_incl_tax_incl_discounts(self):
        # We use whichever discount value is set.  If the discount value was
        # calculated against the tax-exclusive prices, then the line price
        # including tax
        return self.line_price_incl_tax - self.discount_value

    @property
    def line_tax(self):
        return self.quantity * self.unit_tax

    @property
    def line_price_incl_tax(self):
        return self.quantity * self.unit_price_incl_tax

    @property
    def description(self):
        d = str(self.computer)
        ops = []
        for attribute in self.attributes.all():
            ops.append("%s = '%s'" % (attribute.option.name, attribute.value))
        if ops:
            d = "%s (%s)" % (d.decode('utf-8'), ", ".join(ops))
        return d

    def get_warning(self):
        """
        Return a warning message about this basket line if one is applicable

        This could be things like the price has changed
        """
        computer_stockrecord = self.basket.strategy.select_component_stockrecords(self.computer)
        for stockrecord in computer_stockrecord:
            if not stockrecord[1]:
                msg = u"'%(computer)s' is no longer available"
                return _(msg) % {'computer': self.computer.get_title()}

        # if not self.stockrecord:
        #     msg = u"'%(computer)s' is no longer available"
        #     return _(msg) % {'computer': self.computer.get_title()}

        if not self.price_incl_tax:
            return
        if not self.purchase_info.price.is_tax_known:
            return

        # Compare current price to price when added to basket
        current_price_incl_tax = self.purchase_info.price.incl_tax
        if current_price_incl_tax != self.price_incl_tax:
            computer_prices = {
                'computer': self.computer.get_title(),
                'old_price': currency(self.price_incl_tax),
                'new_price': currency(current_price_incl_tax)
            }
            if current_price_incl_tax > self.price_incl_tax:
                warning = _("The price of '%(computer)s' has increased from"
                            " %(old_price)s to %(new_price)s since you added"
                            " it to your basket")
                return warning % computer_prices
            else:
                warning = _("The price of '%(computer)s' has decreased from"
                            " %(old_price)s to %(new_price)s since you added"
                            " it to your basket")
                return warning % computer_prices


class ComputerLineAttribute(models.Model):
    """
    An attribute of a basket line
    """
    line = models.ForeignKey('basket.ComputerLine', related_name='computer_attributes',
                             verbose_name=_("Line"))
    option = models.ForeignKey('catalogue.Option', verbose_name=_("Option"))
    value = models.CharField(_("Value"), max_length=255)

    class Meta:
        app_label = 'basket'
        verbose_name = _('Line computer attribute')
        verbose_name_plural = _('Line computer attributes')


class Basket(AbstractBasket):

    def __init__(self, *args, **kwargs):
        super(AbstractBasket, self).__init__(*args, **kwargs)

        # We keep a cached copy of the basket lines as we refer to them often
        # within the same request cycle.  Also, applying offers will append
        # discount data to the basket lines which isn't persisted to the DB and
        # so we want to avoid reloading them as this would drop the discount
        # information.
        self._lines = None
        self._computer_lines = None
        self.offer_applications = results.OfferApplications()

    # ========
    # Strategy
    # ========

    def all_computer_lines(self):
        """
        Return a cached set of basket lines.

        This is important for offers as they alter the line models and you
        don't want to reload them from the DB as that information would be
        lost.
        """
        if self.id is None:
            return self.computer_lines.none()
        if self._computer_lines is None:
            self._computer_lines = (
                self.computer_lines
                .select_related('computer')
                .prefetch_related(
                    'computer_attributes', 'computer__images'))
        return self._computer_lines

    # ============
    # Manipulation
    # ============

    def flush(self):
        """
        Remove all lines from basket.
        """
        if self.status == self.FROZEN:
            raise PermissionDenied("A frozen basket cannot be flushed")
        self.lines.all().delete()
        self.computer_lines.all().delete()
        self._lines = None
        self._computer_lines = None

    def add_computer(self, computer, quantity=1, options=None):
        """
        Add a product to the basket

        'stock_info' is the price and availability data returned from
        a partner strategy class.

        The 'options' list should contains dicts with keys 'option' and 'value'
        which link the relevant product.Option model and string value
        respectively.

        Returns (line, created).
          line: the matching basket line
          created: whether the line was created or updated

        """
        if options is None:
            options = []
        if not self.id:
            self.save()

        # Ensure that all lines are the same currency
        price_currency = self.currency
        stock_info = self.strategy.fetch_for_computer(computer)
        if price_currency and stock_info.price.currency != price_currency:
            raise ValueError((
                                 "Basket lines must all have the same currency. Proposed "
                                 "line has currency %s, while basket has currency %s")
                             % (stock_info.price.currency, price_currency))

        computer_stockrecord = self.strategy.select_component_stockrecords(computer)
        for stockrecord in computer_stockrecord:
            if stockrecord[1] is None:
                raise ValueError((
                                     "Basket lines must all have stock records. Strategy hasn't "
                                     "found any stock record for product %s") % computer)

        # Line reference is used to distinguish between variations of the same
        # product (eg T-shirts with different personalisations)
        line_ref = self._create_computer_line_reference(
            computer, options)

        # Determine price to store (if one exists).  It is only stored for
        # audit and sometimes caching.
        defaults = {
            'quantity': quantity,
            'price_excl_tax': stock_info.price.excl_tax,
            'price_currency': stock_info.price.currency,
        }
        if stock_info.price.is_tax_known:
            defaults['price_incl_tax'] = stock_info.price.incl_tax

        line, created = self.computer_lines.get_or_create(
            line_reference=line_ref,
            computer=computer,
            defaults=defaults)
        if created:
            for option_dict in options:
                line.attributes.create(option=option_dict['option'],
                                       value=option_dict['value'])
        else:
            line.quantity += quantity
            line.save()
        self.reset_offer_applications()

        # Returning the line is useful when overriding this method.
        return line, created
    add_computer.alters_data = True
    add_c = add_computer

    def reset_offer_applications(self):
        """
        Remove any discounts so they get recalculated
        """
        self.offer_applications = results.OfferApplications()
        self._lines = None
        self._computer_lines = None

    def merge_computer_line(self, line, add_quantities=True):
        """
        For transferring a line from another basket to this one.

        This is used with the "Saved" basket functionality.
        """
        try:
            existing_line = self.computer_lines.get(line_reference=line.line_reference)
        except ObjectDoesNotExist:
            # Line does not already exist - reassign its basket
            line.basket = self
            line.save()
        else:
            # Line already exists - assume the max quantity is correct and
            # delete the old
            if add_quantities:
                existing_line.quantity += line.quantity
            else:
                existing_line.quantity = max(existing_line.quantity,
                                             line.quantity)
            existing_line.save()
            line.delete()
        finally:
            self._computer_lines = None
    merge_computer_line.alters_data = True

    def merge(self, basket, add_quantities=True):
        """
        Merges another basket with this one.

        :basket: The basket to merge into this one.
        :add_quantities: Whether to add line quantities when they are merged.
        """
        # Use basket.lines.all instead of all_lines as this function is called
        # before a strategy has been assigned.
        for line_to_merge in basket.lines.all():
            self.merge_line(line_to_merge, add_quantities)
        for line_to_merge in basket.computer_lines.all():
            self.merge_computer_line(line_to_merge, add_quantities)
        basket.status = self.MERGED
        basket.date_merged = now()
        basket._lines = None
        basket._computer_lines = None
        basket.save()
        # Ensure all vouchers are moved to the new basket
        for voucher in basket.vouchers.all():
            basket.vouchers.remove(voucher)
            self.vouchers.add(voucher)
    merge.alters_data = True

    def is_shipping_required(self):
        """
        Test whether the basket contains physical products that require
        shipping.
        """
        for line in self.all_lines():
            if line.product.is_shipping_required:
                return True
        for line in self.all_computer_lines():
            if line.computer.is_shipping_required:
                return True
        return False

    # =======
    # Helpers
    # =======

    def _create_computer_line_reference(self, product, options):
        """
        Returns a reference string for a line based on the item
        and its options.
        """
        base = '%s' % (product.id)
        if not options:
            return base
        return "%s_%s" % (base, zlib.crc32(repr(options).encode('utf8')))

    def _get_total(self, property):
        """
        For executing a named method on each line of the basket
        and returning the total.
        """
        total = D('0.00')
        for line in self.all_lines():
            try:
                total += getattr(line, property)
            except ObjectDoesNotExist:
                # Handle situation where the product may have been deleted
                pass
        for line in self.all_computer_lines():
            try:
                total += getattr(line, property)
            except ObjectDoesNotExist:
                # Handle situation where the product may have been deleted
                pass
        return total

    # ==========
    # Properties
    # ==========

    @property
    def is_tax_known(self):
        """
        Test if tax values are known for this basket
        """
        return (all([line.is_tax_known for line in self.all_lines()]) and
                all([line.is_tax_known for line in self.all_computer_lines()]))

    @property
    def num_lines(self):
        """Return number of lines"""
        return self.all_lines().count() + self.all_computer_lines().count()

    @property
    def num_items(self):
        """Return number of items"""
        return (sum(line.quantity for line in self.lines.all()) +
                sum(line.quantity for line in self.computer_lines.all()))

    @property
    def num_items_without_discount(self):
        num = 0
        for line in self.all_lines():
            num += line.quantity_without_discount
        for line in self.all_computer_lines():
            num += line.quantity_without_discount
        return num

    @property
    def num_items_with_discount(self):
        num = 0
        for line in self.all_lines():
            num += line.quantity_with_discount
        for line in self.all_computer_lines():
            num += line.quantity_with_discount
        return num

    @property
    def currency(self):
        # Since all lines should have the same currency, return the currency of
        # the first one found.
        for line in self.all_lines():
            return line.price_currency
        for line in self.all_computer_lines():
            return line.price_currency

    # =============
    # Query methods
    # =============

    def computer_quantity(self, computer):
        """
        Return the quantity of a product in the basket

        The basket can contain multiple lines with the same product, but
        different options and stockrecords. Those quantities are summed up.
        """
        matching_lines = self.computer_lines.filter(computer=computer)
        quantity = matching_lines.aggregate(Sum('quantity'))['quantity__sum']
        return quantity or 0

    def line_computer_quantity(self, computer, options=None):
        """
        Return the current quantity of a specific product and options
        """
        ref = self._create_line_reference(computer, options)
        try:
            return self.computer_lines.get(line_reference=ref).quantity
        except ObjectDoesNotExist:
            return 0

from oscar.apps.basket.models import *  # noqa
