# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import oscar.core.utils


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0003_auto_20141219_1545'),
        ('computer', '0001_initial'),
        ('basket', '0004_auto_20141007_2032'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComputerLine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('line_reference', models.SlugField(max_length=128, verbose_name='Line Reference')),
                ('quantity', models.PositiveIntegerField(default=1, verbose_name='Quantity')),
                ('price_currency', models.CharField(default=oscar.core.utils.get_default_currency, max_length=12, verbose_name='Currency')),
                ('price_excl_tax', models.DecimalField(null=True, verbose_name='Price excl. Tax', max_digits=12, decimal_places=2)),
                ('price_incl_tax', models.DecimalField(null=True, verbose_name='Price incl. Tax', max_digits=12, decimal_places=2)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('basket', models.ForeignKey(related_name='computer_lines', verbose_name='Basket', to='basket.Basket')),
                ('computer', models.ForeignKey(related_name='basket_lines', verbose_name='Computer', to='computer.Computer')),
            ],
            options={
                'verbose_name': 'Basket computer line',
                'verbose_name_plural': 'Basket computer lines',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerLineAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255, verbose_name='Value')),
                ('line', models.ForeignKey(related_name='computer_attributes', verbose_name='Line', to='basket.ComputerLine')),
                ('option', models.ForeignKey(verbose_name='Option', to='catalogue.Option')),
            ],
            options={
                'verbose_name': 'Line computer attribute',
                'verbose_name_plural': 'Line computer attributes',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='computerline',
            unique_together=set([('basket', 'line_reference')]),
        ),
    ]
