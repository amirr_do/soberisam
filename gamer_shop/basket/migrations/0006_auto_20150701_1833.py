# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0005_auto_20141227_1757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basket',
            name='vouchers',
            field=models.ManyToManyField(verbose_name='Vouchers', blank=True, to='voucher.Voucher'),
        ),
    ]
