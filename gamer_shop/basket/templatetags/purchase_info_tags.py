from oscar.templatetags.purchase_info_tags import *
# from django import template


# register = template.Library()


@register.assignment_tag
def purchase_info_for_computer(request, computer):

    return request.strategy.fetch_for_computer(computer)


@register.assignment_tag
def purchase_info_for_computer_line(request, line):
    return request.strategy.fetch_for_computer_line(line)
