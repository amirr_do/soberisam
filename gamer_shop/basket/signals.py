import django.dispatch

basket_addition_computer = django.dispatch.Signal(
    providing_args=["computer", "user", "request"])

from oscar.apps.basket.signals import *
