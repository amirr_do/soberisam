from django.http import HttpResponseRedirect
from django.utils.functional import curry
from oscar.apps.basket.views import *
from django.utils.translation import ugettext_lazy as _
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key

from gamer_shop.basket import signals

AddComputerToBasketForm = get_class('basket.forms', 'AddComputerToBasketForm')
BasketComputerLineFormSet = get_class('basket.forms', 'BasketComputerLineFormSet')
BasketComputerLineForm = get_class('basket.forms', 'BasketComputerLineForm')
Line = get_model('basket', 'Line')
ComputerLine = get_model('basket', 'ComputerLine')


class BasketAddView(BasketAddView):

    def form_valid(self, form):
        response = super().form_valid(form)
        cache.delete(make_template_fragment_key('basket_total', [self.request.basket_hash]))

        return response


class BasketComputerAddView(FormView):
    """
    Handles the add-to-basket submissions, which are triggered from various
    parts of the site. The add-to-basket form is loaded into templates using
    a templatetag from module basket_tags.py.
    """
    form_class = AddComputerToBasketForm
    computer_model = get_model('computer', 'Computer')
    add_signal = signals.basket_addition_computer
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        self.computer = shortcuts.get_object_or_404(
            self.computer_model, pk=kwargs['pk'])
        return super(BasketComputerAddView, self).post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(BasketComputerAddView, self).get_form_kwargs()
        kwargs['basket'] = self.request.basket
        kwargs['computer'] = self.computer
        return kwargs

    def form_invalid(self, form):
        msgs = []
        for error in form.errors.values():
            msgs.append(error.as_text())
        clean_msgs = [m.replace('* ', '') for m in msgs if m.startswith('* ')]
        messages.error(self.request, ",".join(clean_msgs))

        return redirect_to_referrer(self.request, 'basket:summary')

    def form_valid(self, form):
        offers_before = self.request.basket.applied_offers()

        self.request.basket.add_computer(
            form.computer, form.cleaned_data['quantity'],
            form.cleaned_options())

        messages.success(self.request, self.get_success_message(form),
                         extra_tags='safe noicon')

        # Check for additional offer messages
        BasketMessageGenerator().apply_messages(self.request, offers_before)

        cache.delete(make_template_fragment_key('basket_total', [self.request.basket_hash]))

    # Send signal for basket addition
        self.add_signal.send(
            sender=self, computer=form.computer, user=self.request.user,
            request=self.request)

        return super(BasketComputerAddView, self).form_valid(form)

    def get_success_message(self, form):
        return render_to_string(
            'basket/messages/addition_computer.html',
            {'computer': form.computer,
             'quantity': form.cleaned_data['quantity']})

    def get_success_url(self):
        post_url = self.request.POST.get('next')
        if post_url and is_safe_url(post_url, self.request.get_host()):
            return post_url
        return safe_referrer(self.request, 'basket:summary')


class BasketView(BasketView):
    model = None
    basket_model = get_model('basket', 'Basket')
    form_class = None
    formset_class = None
    # computer_model = get_model('basket', 'ComputerLine')
    extra = 0
    can_delete = True
    template_name = 'basket/basket.html'

    def get(self, request, *args, **kwargs):
        self.object_computer_list = self.get_computer_queryset()
        self.object_list = self.get_queryset()
        product_formset = BasketLineFormSet(strategy=self.request.strategy, prefix='product')
        computer_formset = BasketComputerLineFormSet(strategy=self.request.strategy, prefix='computer')
        return self.render_to_response(self.get_context_data(
            product_formset=product_formset,
            computer_formset=computer_formset))

    def post(self, request, *args, **kwargs):
        self.object_computer_list = self.get_computer_queryset()
        self.object_list = self.get_queryset()
        product_formset = self.construct_product_formset()
        computer_formset = self.construct_computer_formset()
        if product_formset.is_valid() and computer_formset.is_valid():
            return self.formset_all_valid(product_formset, computer_formset)
        else:
            return self.formset_all_invalid(product_formset, computer_formset)

    def construct_product_formset(self):
        """
        Returns an instance of the formset
        """
        formset_class = BasketLineFormSet
        extra_form_kwargs = self.get_extra_form_kwargs()

        # Hack to let as pass additional kwargs to each forms constructor. Be aware that this
        # doesn't let us provide *different* arguments for each form
        if extra_form_kwargs:
            formset_class.form = staticmethod(curry(formset_class.form, **extra_form_kwargs))

        return formset_class(**self.get_product_formset_kwargs())

    def construct_computer_formset(self):
        """
        Returns an instance of the formset
        """
        formset_class = BasketComputerLineFormSet
        extra_form_kwargs = self.get_extra_form_kwargs()

        # Hack to let as pass additional kwargs to each forms constructor. Be aware that this
        # doesn't let us provide *different* arguments for each form
        if extra_form_kwargs:
            formset_class.form = staticmethod(curry(formset_class.form, **extra_form_kwargs))

        return formset_class(**self.get_computer_formset_kwargs())

    def get_formset_kwargs(self):
        kwargs = super(BasketView, self).get_formset_kwargs()
        kwargs['strategy'] = self.request.strategy
        # kwargs['prefix'] = 'product'
        return kwargs

    def get_product_formset_kwargs(self):
        kwargs = {}
        initial = self.get_initial()
        if initial:
            kwargs['initial'] = initial

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        kwargs['queryset'] = self.get_queryset()
        kwargs['strategy'] = self.request.strategy
        kwargs['prefix'] = 'product'
        return kwargs

    def get_computer_formset_kwargs(self):
        kwargs = {}
        initial = self.get_initial()
        if initial:
            kwargs['initial'] = initial

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        kwargs['queryset'] = self.get_computer_queryset()
        kwargs['strategy'] = self.request.strategy
        kwargs['prefix'] = 'computer'
        return kwargs

    def get_queryset(self):
        return self.request.basket.all_lines()

    def get_computer_queryset(self):
        return self.request.basket.all_computer_lines()

    def get_context_data(self, **kwargs):
        context = super(BasketView, self).get_context_data(**kwargs)
        context['voucher_form'] = self.get_basket_voucher_form()
        context['product_formset'] = BasketLineFormSet(strategy=self.request.strategy,
                                                       queryset=self.get_queryset(),
                                                       prefix='product')
        context['product_form'] = BasketLineForm(strategy=self.request.strategy,
                                                 prefix='product')
        context['computer_formset'] = BasketComputerLineFormSet(strategy=self.request.strategy,
                                                                queryset=self.get_computer_queryset(),
                                                                prefix='computer')
        context['computer_form'] = BasketComputerLineForm(strategy=self.request.strategy,
                                                          prefix='computer')

        # Shipping information is included to give an idea of the total order
        # cost.  It is also important for PayPal Express where the customer
        # gets redirected away from the basket page and needs to see what the
        # estimated order total is beforehand.
        context['shipping_methods'] = self.get_shipping_methods(
            self.request.basket)
        method = self.get_default_shipping_method(self.request.basket)
        context['shipping_method'] = method
        shipping_charge = method.calculate(self.request.basket)
        context['shipping_charge'] = shipping_charge
        if method.is_discounted:
            excl_discount = method.calculate_excl_discount(shipping_charge)
            context['shipping_charge_excl_discount'] = excl_discount

        context['order_total'] = OrderTotalCalculator().calculate(
            self.request.basket, shipping_charge)
        context['basket_warnings'] = self.get_basket_warnings(
            self.request.basket)
        context['upsell_messages'] = self.get_upsell_messages(
            self.request.basket)

        if self.request.user.is_authenticated():
            try:
                saved_basket = self.basket_model.saved.get(
                    owner=self.request.user)
            except self.basket_model.DoesNotExist:
                pass
            else:
                saved_basket.strategy = self.request.basket.strategy
                if not saved_basket.is_empty:
                    saved_queryset = saved_basket.all_lines().select_related(
                        'product', 'product__stockrecord')
                    formset = SavedLineFormSet(strategy=self.request.strategy,
                                               basket=self.request.basket,
                                               queryset=saved_queryset,
                                               prefix='saved')
                    context['saved_formset'] = formset
        return context

    def json_response(self, ctx, flash_messages):
        basket_html = render_to_string(
            'basket/partials/basket_content.html',
            RequestContext(self.request, ctx))
        payload = {
            'content_html': basket_html,
            'messages': flash_messages.as_dict()}
        return HttpResponse(json.dumps(payload),
                            content_type="application/json")
    # def get_success_url(self):
    #     return safe_referrer(self.request.META, 'basket:summary')

    def formset_valid(self, formset):
        # Store offers before any changes are made so we can inform the user of
        # any changes
        offers_before = self.request.basket.applied_offers()
        save_for_later = False

        # Keep a list of messages - we don't immediately call
        # django.contrib.messages as we may be returning an AJAX response in
        # which case we pass the messages back in a JSON payload.
        flash_messages = ajax.FlashMessages()

        for form in formset:
            if (hasattr(form, 'cleaned_data') and
                    form.cleaned_data['save_for_later']):
                line = form.instance
                if self.request.user.is_authenticated():
                    self.move_line_to_saved_basket(line)

                    msg = render_to_string(
                        'basket/messages/line_saved.html',
                        {'line': line})
                    flash_messages.info(msg)

                    save_for_later = True
                else:
                    msg = _("You can't save an item for later if you're "
                            "not logged in!")
                    flash_messages.error(msg)
                    return redirect(self.get_success_url())

        if save_for_later:
            # No need to call super if we're moving lines to the saved basket
            response = redirect(self.get_success_url())
        else:
            # Save changes to basket as per normal
            self.object_list = formset.save()
            response = HttpResponseRedirect(self.get_success_url())

        # If AJAX submission, don't redirect but reload the basket content HTML
        if self.request.is_ajax():
            # Reload basket and apply offers again
            self.request.basket = get_model('basket', 'Basket').objects.get(
                id=self.request.basket.id)
            self.request.basket.strategy = self.request.strategy
            Applicator().apply(self.request, self.request.basket)
            offers_after = self.request.basket.applied_offers()

            for level, msg in BasketMessageGenerator().get_messages(
                    self.request.basket, offers_before,
                    offers_after, include_buttons=False):
                flash_messages.add_message(level, msg)

            # Reload formset - we have to remove the POST fields from the
            # kwargs as, if they are left in, the formset won't construct
            # correctly as there will be a state mismatch between the
            # management form and the database.
            kwargs = self.get_product_formset_kwargs()
            del kwargs['data']
            del kwargs['files']
            if 'queryset' in kwargs:
                del kwargs['queryset']
            # formset = self.get_formset()(queryset=self.get_queryset(),
            #                              **kwargs)
            formset = BasketLineFormSet(queryset=self.get_queryset(),
                                        **kwargs)
            ctx = self.get_context_data(product_formset=formset,
                                        basket=self.request.basket)
            return self.json_response(ctx, flash_messages)

        BasketMessageGenerator().apply_messages(self.request, offers_before)

        return response

    def formset_computer_valid(self, formset):
        # Store offers before any changes are made so we can inform the user of
        # any changes
        offers_before = self.request.basket.applied_offers()
        save_for_later = False

        # Keep a list of messages - we don't immediately call
        # django.contrib.messages as we may be returning an AJAX response in
        # which case we pass the messages back in a JSON payload.
        flash_messages = ajax.FlashMessages()

        for form in formset:
            if (hasattr(form, 'cleaned_data') and
                    form.cleaned_data['save_for_later']):
                line = form.instance
                if self.request.user.is_authenticated():
                    self.move_line_to_saved_basket(line)

                    msg = render_to_string(
                        'basket/messages/line_saved.html',
                        {'line': line})
                    flash_messages.info(msg)

                    save_for_later = True
                else:
                    msg = _("You can't save an item for later if you're "
                            "not logged in!")
                    flash_messages.error(msg)
                    return redirect(self.get_success_url())

        if save_for_later:
            # No need to call super if we're moving lines to the saved basket
            response = redirect(self.get_success_url())
        else:
            # Save changes to basket as per normal
            self.object_computer_list = formset.save()
            response = HttpResponseRedirect(self.get_success_url())

        # If AJAX submission, don't redirect but reload the basket content HTML
        if self.request.is_ajax():
            # Reload basket and apply offers again
            self.request.basket = get_model('basket', 'Basket').objects.get(
                id=self.request.basket.id)
            self.request.basket.strategy = self.request.strategy
            Applicator().apply(self.request, self.request.basket)
            offers_after = self.request.basket.applied_offers()

            for level, msg in BasketMessageGenerator().get_messages(
                    self.request.basket, offers_before,
                    offers_after, include_buttons=False):
                flash_messages.add_message(level, msg)

            # Reload formset - we have to remove the POST fields from the
            # kwargs as, if they are left in, the formset won't construct
            # correctly as there will be a state mismatch between the
            # management form and the database.
            kwargs = self.get_computer_formset_kwargs()
            del kwargs['data']
            del kwargs['files']
            if 'queryset' in kwargs:
                del kwargs['queryset']
            # formset = self.get_formset()(queryset=self.get_queryset(),
            #                              **kwargs)
            formset = BasketComputerLineFormSet(queryset=self.get_computer_queryset(),
                                                **kwargs)
            ctx = self.get_context_data(computer_formset=formset,
                                        basket=self.request.basket)
            return self.json_response(ctx, flash_messages)

        BasketMessageGenerator().apply_messages(self.request, offers_before)

        return response

    def formset_invalid(self, formset):
        flash_messages = ajax.FlashMessages()
        flash_messages.warning(_("Your basket couldn't be updated"))

        if self.request.is_ajax():
            ctx = self.get_context_data(formset=formset,
                                        basket=self.request.basket)
            return self.json_response(ctx, flash_messages)

        flash_messages.apply_to_request(self.request)
        return super(BasketView, self).formset_invalid(formset)

    def formset_all_valid(self, product_formset, computer_formset):
        # Store offers before any changes are made so we can inform the user of
        # any changes
        offers_before = self.request.basket.applied_offers()
        save_for_later = False

        # Keep a list of messages - we don't immediately call
        # django.contrib.messages as we may be returning an AJAX response in
        # which case we pass the messages back in a JSON payload.
        flash_messages = ajax.FlashMessages()

        for form in product_formset:
            if (hasattr(form, 'cleaned_data') and
                    form.cleaned_data['save_for_later']):
                line = form.instance
                if self.request.user.is_authenticated():
                    self.move_line_to_saved_basket(line)

                    # msg = render_to_string(
                    #     'basket/messages/line_saved.html',
                    #     {'line': line})
                    # flash_messages.info(msg)

                    save_for_later = True
                else:
                    msg = _("You can't save an item for later if you're "
                            "not logged in!")
                    flash_messages.error(msg)
                    return redirect(self.get_success_url())

        for form in computer_formset:
            if (hasattr(form, 'cleaned_data') and
                    form.cleaned_data['save_for_later']):
                line = form.instance
                if self.request.user.is_authenticated():
                    self.move_line_to_saved_basket(line)

                    # msg = render_to_string(
                    #     'basket/messages/line_saved.html',
                    #     {'line': line})
                    # flash_messages.info(msg)

                    save_for_later = True
                else:
                    msg = _("You can't save an item for later if you're "
                            "not logged in!")
                    flash_messages.error(msg)
                    return redirect(self.get_success_url())

        if save_for_later:
            # No need to call super if we're moving lines to the saved basket
            response = redirect(self.get_success_url())
        else:
            # Save changes to basket as per normal
            self.object_list = product_formset.save()
            self.object_computer_list = computer_formset.save()
            response = HttpResponseRedirect(self.get_success_url())

        # If AJAX submission, don't redirect but reload the basket content HTML
        if self.request.is_ajax():
            # Reload basket and apply offers again
            self.request.basket = get_model('basket', 'Basket').objects.get(
                id=self.request.basket.id)
            self.request.basket.strategy = self.request.strategy
            Applicator().apply(self.request.basket, self.request.user,
                               self.request)
            offers_after = self.request.basket.applied_offers()

            for level, msg in BasketMessageGenerator().get_messages(
                    self.request.basket, offers_before,
                    offers_after, include_buttons=False):
                flash_messages.add_message(level, msg)

            # Reload formset - we have to remove the POST fields from the
            # kwargs as, if they are left in, the formset won't construct
            # correctly as there will be a state mismatch between the
            # management form and the database.
            kwargs = self.get_formset_kwargs()
            del kwargs['data']
            del kwargs['files']
            if 'queryset' in kwargs:
                del kwargs['queryset']
            product_formset = BasketLineFormSet(queryset=self.get_queryset(),
                                                **kwargs)
            computer_formset = BasketComputerLineFormSet(queryset=self.get_computer_queryset(),
                                                         **kwargs)

            cache.delete(make_template_fragment_key('basket_total', [self.request.basket_hash]))

            ctx = self.get_context_data(product_formset=product_formset,
                                        computer_formset=computer_formset,
                                        basket=self.request.basket)
            return self.json_response(ctx, flash_messages)

        BasketMessageGenerator().apply_messages(self.request, offers_before)

        return response

    def formset_all_invalid(self, product_formset, computer_formset):
        flash_messages = ajax.FlashMessages()
        flash_messages.warning(_("Your basket couldn't be updated"))

        if self.request.is_ajax():
            ctx = self.get_context_data(product_formset=product_formset,
                                        computer_formset=computer_formset,
                                        basket=self.request.basket)
            return self.json_response(ctx, flash_messages)

        flash_messages.apply_to_request(self.request)
        return super(BasketView, self).formset_invalid(product_formset)
