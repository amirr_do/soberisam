from oscar.apps.basket import config


class BasketConfig(config.BasketConfig):
    name = 'gamer_shop.basket'
