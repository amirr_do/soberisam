from oscar.apps.promotions.views import *
from oscar.core.loading import get_model, get_class

from gamer_shop.mixins.mixins import DateContextMixin

Computer = get_model('computer', 'Computer')
ComputerCategory = get_model('computer', 'ComputerCategory')
CallbackRequestForm = get_class('customer.forms', 'CallbackRequestForm')
Category = get_model('catalogue', 'Category')


class HomeView(HomeView, DateContextMixin):
    """
    This is the home page and will typically live at /
    """

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        category = lambda slug: Category.objects.get(slug=slug)
        solarwind = {
            'solarwind_gamer': category(slug='solarwind-gamer'),
            'solarwind_office': category(slug='solarwind-office'),
            'solarwind_nettop': category(slug='solarwind-nettop'),
            'solarwind_extreme': category(slug='solarwind-extreme'),
        }
        ctx.update(solarwind)

        ctx['callback_form'] = CallbackRequestForm()
        ctx.update(self.get_date_context())

        date_for_count = ctx["today"], ctx['yesterday'], ctx['month'], ctx['year']

        ctx.update(self.get_computer_counts(*date_for_count))

        return ctx
