from oscar.apps.promotions import config


class PromotionsConfig(config.PromotionsConfig):
    name = 'gamer_shop.promotions'
