from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import InvalidPage
from django.core.urlresolvers import reverse
from django.http import HttpResponsePermanentRedirect, HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.template.loader import render_to_string
from django.utils.http import urlquote, is_safe_url
from django.views.generic import DetailView, TemplateView, FormView, DeleteView
from django.utils.translation import ugettext_lazy as _
from oscar.core.loading import get_model, get_class
from oscar.core.utils import redirect_to_referrer

from oscar.apps.catalogue.signals import product_viewed

Socket = get_model('build', 'Socket')
Product = get_model('catalogue', 'product')
ProductReview = get_model('reviews', 'ProductReview')
Category = get_model('catalogue', 'category')
ProductCategory = get_model('catalogue', 'ProductCategory')
ProductAlert = get_model('customer', 'ProductAlert')
ComputerAttributeValue = get_model('computer', 'ComputerAttributeValue')
ComputerAttribute = get_model('catalogue', 'ProductAttribute')
ProductAlertForm = get_class('customer.forms', 'ProductAlertForm')
get_product_search_handler_class = get_class(
        'catalogue.search_handlers', 'get_product_search_handler_class')
AddToBuildForm = get_class('build.forms', 'AddToBuildForm')
BuildProductSearchHandler = get_class('catalogue.search_handlers', 'BuildProductSearchHandler')


class BuildView(TemplateView):
    """
    Browse all products in the catalogue
    """
    context_object_name = "products"
    template_name = 'build/browse.html'

    def get(self, request, *args, **kwargs):
        try:
            self.search_handler = self.get_search_handler(
                    self.request.GET, request.get_full_path(), [])
        except InvalidPage:
            # Redirect to page one.
            messages.error(request, _('The given page number was invalid.'))
            return redirect('build:index')
        return super(BuildView, self).get(request, *args, **kwargs)

    def get_search_handler(self, *args, **kwargs):
        return get_product_search_handler_class()(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {}
        ctx['summary'] = _("All products")
        search_context = self.search_handler.get_search_context_data(
                self.context_object_name)
        ctx.update(search_context)
        return ctx


class BuildSocketView(TemplateView):
    """
    Browse all products in the catalogue
    """
    template_name = 'build/socket.html'

    @staticmethod
    def delete_for_socket(computer, attribute, socket):
        cpu = computer.cpu
        motherboard = computer.motherboard
        rams = computer.rams
        try:
            computer_socket = ComputerAttributeValue.objects.get(computer=computer, attribute=attribute)
        except ObjectDoesNotExist:
            computer_socket = None

        if computer_socket and computer_socket.value_text != socket:
            if not ('FM2' in computer_socket.value_text and 'FM2' in socket):
                if cpu:
                    computer.delete_product(cpu)
                if motherboard:
                    computer.delete_product(motherboard)
                for ram in rams:
                    computer.delete_product(ram.ram)

    def post(self, request, *args, **kwargs):
        socket = request.POST.get('socket')
        if not request.computer.id:
            request.computer.save()
        computer = request.computer
        attribute = ComputerAttribute.objects.get(code='socket_computer')
        self.delete_for_socket(computer, attribute, socket)
        ComputerAttributeValue.objects.update_or_create(
                computer=computer, defaults={
                    'value_text': socket,
                    'attribute': attribute})

        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        ctx = super(BuildSocketView, self).get_context_data(**kwargs)
        ctx['s1150'] = Socket.objects.get(code='LGA1150')
        ctx['s1151'] = Socket.objects.get(code='LGA1151')
        ctx['s1155'] = Socket.objects.get(code='LGA1155')
        ctx['s2011'] = Socket.objects.get(code='LGA2011')
        ctx['am3p'] = Socket.objects.get(code='AM3+')
        ctx['fm2p'] = Socket.objects.get(code='FM2+')
        ctx['fm2'] = Socket.objects.get(code='FM2')
        return ctx

    def get_success_url(self):
        return reverse('build:category',
                       kwargs={'category_slug': 'cpu'})


class BuildCategoryView(TemplateView):
    """
    Browse products in a given category
    """
    context_object_name = "products"
    template_folder = 'build/hardware'
    template_name = 'build/category.html'
    enforce_paths = False

    def get(self, request, *args, **kwargs):
        # Fetch the category; return 404 or redirect as needed
        if request.computer.id is None or not request.computer.is_socket:
            return HttpResponseRedirect(reverse('build:socket'))
        self.category = self.get_category()
        potential_redirect = self.redirect_if_necessary(
                request.path, self.category)
        if potential_redirect is not None:
            return potential_redirect

        try:
            computer_build = request.computer
            # computer_build.attr.socket_computer = 'LGA1150'
            self.search_handler = self.get_search_handler(
                    request.GET, request.get_full_path(), self.get_categories(), computer_build)
        except InvalidPage:
            messages.error(request, _('The given page number was invalid.'))
            return redirect(self.category.get_build_absolute_url())

        return super(BuildCategoryView, self).get(request, *args, **kwargs)

    def get_category(self):
        filters = {'slug': self.kwargs['category_slug']}
        return get_object_or_404(Category, **filters)

    def redirect_if_necessary(self, current_path, category):
        if self.enforce_paths:
            # Categories are fetched by primary key to allow slug changes.
            # If the slug has changed, issue a redirect.
            expected_path = category.get_build_absolute_url()
            if expected_path != urlquote(current_path):
                return HttpResponsePermanentRedirect(expected_path)

    def get_search_handler(self, *args, **kwargs):
        return BuildProductSearchHandler(*args, **kwargs)

    def get_categories(self):
        """
        Return a list of the current category and its ancestors
        """
        return self.category.get_descendants_and_self()

    def get_template_names(self):
        template = self.template_name
        category_template = '{folder}/{slug}/category.html'.format(folder=self.template_folder, slug=self.category.slug)
        return [category_template, template]

    def get_context_data(self, **kwargs):
        context = super(BuildCategoryView, self).get_context_data(**kwargs)
        context['category'] = self.category

        sort_by = self.request.GET.get('sort_by', '')
        context['sort_by'] = sort_by

        search_context = self.search_handler.get_search_context_data(
                self.context_object_name)
        context.update(search_context)

        computer = self.request.computer
        category_pk = self.category.pk

        current_product = None
        if category_pk == 2:
            current_product = computer.cpu
        elif category_pk == 3:
            current_product = computer.cooler
        elif category_pk == 4:
            current_product = computer.motherboard
        elif category_pk == 5 and computer.computerram_set.filter(computer=computer).exists():
            current_product = computer.computerram_set.get(computer=computer).ram
        elif category_pk == 6:
            if computer.computervideocard_set.filter(computer=computer).exists():
                current_product = computer.computervideocard_set.get(computer=computer).video_card
        elif category_pk == 7:
            if computer.computerhdd_set.filter(computer=computer).exists():
                current_product = computer.computerhdd_set.get(computer=computer).hdd
        elif category_pk == 8:
            if computer.computerssd_set.filter(computer=computer).exists():
                current_product = computer.computerssd_set.get(computer=computer).ssd
        elif category_pk == 9 and computer.computerodd_set.filter(computer=computer).exists():
            current_product = computer.computerodd_set.get(computer=computer).odd
        elif category_pk == 10:
            current_product = computer.case
        elif category_pk == 11:
            if computer.computerpsu_set.filter(computer=computer).exists():
                current_product = computer.computerpsu_set.get(computer=computer).psu

        if current_product:
            context['current_product'] = current_product

        return context


class BuildDeleteView(DeleteView):
    http_method_names = ['post']
    product_model = get_model('catalogue', 'product')

    def post(self, request, *args, **kwargs):
        self.product = get_object_or_404(
                self.product_model, pk=kwargs['pk'])
        return super(BuildDeleteView, self).post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.request.computer.delete_product(self.product)
        success_url = self.get_success_url()

        # messages.success(self.request, self.get_success_message(form),
        #                  extra_tags='safe noicon')

        return HttpResponseRedirect(success_url)

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')


class BuildAddView(FormView):
    """
    Handles the add-to-basket submissions, which are triggered from various
    parts of the site. The add-to-basket form is loaded into templates using
    a templatetag from module basket_tags.py.
    """
    form_class = AddToBuildForm
    product_model = get_model('catalogue', 'product')
    # add_signal = signals.basket_addition
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        self.product = get_object_or_404(
                self.product_model, pk=kwargs['pk'])
        return super(BuildAddView, self).post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(BuildAddView, self).get_form_kwargs()
        kwargs['computer'] = self.request.computer
        kwargs['product'] = self.product
        return kwargs

    def form_invalid(self, form):
        msgs = []
        for error in form.errors.values():
            msgs.append(error.as_text())
        clean_msgs = [m.replace('* ', '') for m in msgs if m.startswith('* ')]
        messages.error(self.request, ",".join(clean_msgs))

        return redirect_to_referrer(self.request, 'basket:summary')

    def form_valid(self, form):
        # offers_before = self.request.basket.applied_offers()

        add_product, msg = self.request.computer.add_product(
                form.product, form.cleaned_data['quantity'],
                form.cleaned_options())

        if not add_product:
            messages.error(self.request, msg)
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

        messages.success(self.request, self.get_success_message(form),
                         extra_tags='safe noicon')

        # Check for additional offer messages
        # apply_messages(self.request, offers_before)

        # Send signal for basket addition
        # self.add_signal.send(
        #     sender=self, product=form.product, user=self.request.user,
        #     request=self.request)

        return super(BuildAddView, self).form_valid(form)

    def get_success_message(self, form):
        return render_to_string(
                'basket/messages/addition.html',
                {'product': form.product,
                 'quantity': form.cleaned_data['quantity']})

    def get_success_url(self):
        post_url = self.request.POST.get('next')
        if post_url and is_safe_url(post_url, self.request.get_host()):
            return post_url

        product_category = ProductCategory.objects.get(product=self.product)
        category = product_category.category
        category_pk = category.pk
        if category_pk == 11:
            response = reverse('computer:edit')
            return response
        if category_pk == 10 and hasattr(self.product.attr, 'power') and self.product.attr.power:
            response = reverse('computer:edit')
            return response
        next_category = Category.objects.get(pk=category_pk + 1)
        response = next_category.get_build_absolute_url()
        add_path = self.request.META.get('HTTP_REFERER', '')
        add_path = add_path.split('?')[-1]
        try:
            sort_str = list(filter(lambda x: 'sort_by' == x.split('=')[0], add_path.split('&')))[0].split('=')[-1]
        except IndexError:
            return response
        return '{}?sort_by={}'.format(response, sort_str)


class BuildDetailView(DetailView):
    context_object_name = 'product'
    model = Product
    view_signal = product_viewed
    template_folder = "build"
    template_name = "build/detail.html"

    # Whether to redirect to the URL with the right path
    enforce_paths = True

    # Whether to redirect child products to their parent's URL
    enforce_parent = True

    def get_context_data(self, **kwargs):
        ctx = super(BuildDetailView, self).get_context_data()
        ctx['is_current'] = self.request.computer.is_current(self.object)
        ctx['reviews'] = self.get_reviews()

        return ctx

    def get_reviews(self):
        return self.object.reviews.filter(status=ProductReview.APPROVED)
