from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class BuildConfig(AppConfig):
    label = 'build'
    name = 'gamer_shop.build'
    verbose_name = _('Build')
