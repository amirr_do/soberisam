from oscar.templatetags.product_tags import *
# from django import template
from django.template.loader import select_template
#
#
# register = template.Library()


@register.simple_tag(takes_context=True)
def render_build_product(context, product):
    """
    Render a product snippet as you would see in a browsing display.

    This templatetag looks for different templates depending on the UPC and
    product class of the passed product.  This allows alternative templates to
    be used for different product classes.
    """
    if not product:
        # Search index is returning products that don't exist in the
        # database...
        return ''

    names = ['build/partials/product/upc-%s.html' % product.upc,
             'build/partials/product/class-%s.html'
             % product.get_product_class().slug,
             'build/partials/product.html']
    template_ = select_template(names)
    # Ensure the passed product is in the context as 'product'
    context['product'] = product
    return template_.render(context)


@register.simple_tag(takes_context=True)
def render_build_current_product(context, product):
    """
    Render a product snippet as you would see in a browsing display.

    This templatetag looks for different templates depending on the UPC and
    product class of the passed product.  This allows alternative templates to
    be used for different product classes.
    """
    if not product:
        # Search index is returning products that don't exist in the
        # database...
        return ''

    names = ['build/partials/current_product/upc-%s.html' % product.upc,
             'build/partials/current_product/class-%s.html'
             % product.get_product_class().slug,
             'build/partials/current_product.html']
    template_ = select_template(names)
    # Ensure the passed product is in the context as 'product'
    context['product'] = product
    return template_.render(context)
