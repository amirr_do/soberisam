# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Socket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=20)),
                ('short_description', models.TextField(max_length=255)),
                ('description', models.TextField(max_length=1000)),
                ('code', models.CharField(max_length=10, default='')),
                ('structure', models.CharField(verbose_name='Socket vendor', max_length=10, choices=[('INTEL', 'Intel'), ('AMD', 'AMD')])),
                ('image', models.ImageField(verbose_name='Image', upload_to='images/products/%Y/%m/', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SocketAttributes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('code', models.CharField(max_length=10, default='')),
                ('title', models.CharField(max_length=20)),
                ('value_text', models.CharField(max_length=50)),
                ('socket', models.ForeignKey(related_name='attr', to='build.Socket')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
