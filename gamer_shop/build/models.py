from django.db import models
from django.utils.translation import ugettext_lazy as _
from gamer_shop import settings


class Socket(models.Model):
    title = models.CharField(max_length=20)
    short_description = models.TextField(max_length=255)
    description = models.TextField(max_length=1000)
    code = models.CharField(max_length=10, default='')

    VENDOR = (
        ('INTEL', 'Intel'),
        ('AMD', 'AMD')
    )
    structure = models.CharField(_("Socket vendor"), max_length=10, choices=VENDOR)
    image = models.ImageField(_("Image"), upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255)

    def get_cpu(self):
        return SocketAttributes.objects.get(code='cpu', socket=self)

    def get_fsb(self):
        return SocketAttributes.objects.get(code='fsb', socket=self)

    def get_freq(self):
        return SocketAttributes.objects.get(code='freq', socket=self)

    def get_support(self):
        return SocketAttributes.objects.get(code='support', socket=self)

    def get_date(self):
        return SocketAttributes.objects.get(code='date', socket=self)


class SocketAttributes(models.Model):
    socket = models.ForeignKey(Socket, related_name='attr')
    code = models.CharField(max_length=10, default='')
    title = models.CharField(max_length=20)
    value_text = models.CharField(max_length=50)
