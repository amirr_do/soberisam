from django.conf.urls import url, include
from django.views.generic import RedirectView

from oscar.core.application import Application
from oscar.core.loading import get_class
from oscar.apps.catalogue.reviews.app import application as reviews_app


class BaseBuildApplication(Application):
    name = 'build'
    detail_view = get_class('build.views', 'BuildDetailView')
    catalogue_view = get_class('build.views', 'BuildView')
    category_view = get_class('build.views', 'BuildCategoryView')
    range_view = get_class('offer.views', 'RangeDetailView')
    add_view = get_class('build.views', 'BuildAddView')
    delete_view = get_class('build.views', 'BuildDeleteView')
    socket_view = get_class('build.views', 'BuildSocketView')

    def get_urls(self):
        urlpatterns = super(BaseBuildApplication, self).get_urls()
        urlpatterns += [
            url(r'^$', self.socket_view.as_view(), name='socket'),
            url(r'^hardware/$', RedirectView.as_view(pattern_name='socket', permanent=False)),
            url(r'^add/(?P<pk>\d+)/$', self.add_view.as_view(), name='add'),
            url(r'^delete/(?P<pk>\d+)/$', self.delete_view.as_view(), name='delete'),
            # url(r'^socket/$', self.socket_view.as_view(), name='socket'),
            url(r'^(?P<product_slug>[\w-]*)_(?P<pk>\d+)/$',
                self.detail_view.as_view(), name='detail'),
            # Fallback URL if a user chops of the last part of the URL
            url(r'^(?P<category_slug>[\w-]+(/[\w-]+)*)/$',
                self.category_view.as_view(), name='category')
            # url(r'^ranges/(?P<slug>[\w-]+)/$',
            #     self.range_view.as_view(), name='range')
        ]
        return self.post_process_urls(urlpatterns)


class ReviewsApplication(Application):
    name = None
    reviews_app = reviews_app

    def get_urls(self):
        urlpatterns = super(ReviewsApplication, self).get_urls()
        urlpatterns += [
            url(r'^(?P<product_slug>[\w-]*)_(?P<product_pk>\d+)/reviews/',
                include(self.reviews_app.urls)),
        ]
        return self.post_process_urls(urlpatterns)


class CatalogueApplication(BaseBuildApplication, ReviewsApplication):
    """
    Composite class combining Products with Reviews
    """


application = CatalogueApplication()
