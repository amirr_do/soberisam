from django.contrib import admin
from oscar.core.loading import get_model

Socket = get_model('build', 'Socket')
SocketAttributes = get_model('build', 'SocketAttributes')


class AttributesInline(admin.TabularInline):
    model = SocketAttributes


class SocketAdmin(admin.ModelAdmin):
    inlines = [AttributesInline]
    list_display = ('title', 'code')


admin.site.register(Socket, SocketAdmin)
