from datetime import datetime
import calendar

from oscar.core.loading import get_model

Computer = get_model('computer', 'Computer')
ComputerCategory = get_model('computer', 'ComputerCategory')


class DateContextMixin:

    @staticmethod
    def get_date_context():
        ctx = dict()
        year = datetime.today().timetuple()[0]
        month = datetime.today().timetuple()[1]
        today = datetime.today().timetuple()[2]
        if today == 1:
            ctx["month_yesterday"] = month - 1
            if month == 1:
                yesterday = calendar.monthrange(year - 1, 12)[1]
                ctx["year_yesterday"] = year - 1
                ctx["month_yesterday"] = 12
            else:
                yesterday = calendar.monthrange(year, month - 1)[1]
        else:
            yesterday = today - 1

        ctx["today"] = today
        ctx["yesterday"] = yesterday
        ctx["month"] = month
        ctx["year"] = year

        return ctx

    @staticmethod
    def get_computer_counts(today, yesterday, month, year):
        ctx = dict()
        computers = Computer.saved.filter(status='Saved')
        month_computers = computers.filter(date_updated__year=year, date_updated__month=month)
        today_computers = month_computers.filter(date_updated__day=today)
        if today == 1:
            month -= 1
            if month == 0:
                year -= 1
                month = 12
        yesterday_computers = computers.filter(date_updated__year=year, date_updated__month=month,
                                               date_updated__day=yesterday)

        user_computer_category = ComputerCategory.objects.filter(category__id=12)

        today_count = user_computer_category.filter(computer__in=today_computers).count()
        yesterday_count = user_computer_category.filter(computer__in=yesterday_computers).count()
        month_count = user_computer_category.filter(computer__in=month_computers).count()
        total_count = user_computer_category.filter(computer__in=computers).count()

        ctx["today_count"] = today_count
        ctx["yesterday_count"] = yesterday_count
        ctx["month_count"] = month_count
        ctx["total_count"] = total_count

        return ctx
