from oscar.apps.checkout.views import *
from django.utils.translation import ugettext as _


class PaymentMethodView(PaymentMethodView):
    def get_success_response(self):
        return redirect('checkout:preview')


class ShippingAddressView(ShippingAddressView):
    success_url = reverse_lazy('checkout:payment-method')

    def get_initial(self):
        initial = super(ShippingAddressView, self).get_initial()
        if not initial:
            initial = {}
        country = Country.objects.get(iso_3166_1_a2='UA')
        initial['country'] = country
        if hasattr(self.request, 'user') and self.request.user and hasattr(self.request.user, 'first_name'):
            initial['first_name'] = self.request.user.first_name

        # if self.checkout_session.shipping_method_code(None) == 'no-shipping-required':
        #     initial['line1'] = 'Доставка не требуется '
        return initial

    def get_form_kwargs(self):
        kwargs = super(ShippingAddressView, self).get_form_kwargs()
        kwargs['code'] = self.checkout_session.shipping_method_code(None)
        return kwargs


class IndexView(IndexView):
    success_url = reverse_lazy('checkout:shipping-method')


class ShippingMethodView(ShippingMethodView):

    def get(self, request, *args, **kwargs):
        # These pre-conditions can't easily be factored out into the normal
        # pre-conditions as they do more than run a test and then raise an
        # exception on failure.

        # Check that shipping is required at all
        if not request.basket.is_shipping_required():
            # No shipping required - we store a special code to indicate so.
            self.checkout_session.use_shipping_method(
                NoShippingRequired().code)
            return self.get_success_response()

        # Save shipping methods as instance var as we need them both here
        # and when setting the context vars.
        self._methods = self.get_available_shipping_methods()
        if len(self._methods) == 0:
            # No shipping methods available for given address
            messages.warning(request, _(
                "Shipping is unavailable for your chosen address - please "
                "choose another"))
            return redirect('checkout:shipping-address')
        elif len(self._methods) == 1:
            # Only one shipping method - set this and redirect onto the next
            # step
            self.checkout_session.use_shipping_method(self._methods[0].code)
            return self.get_success_response()

        # Must be more than one available shipping method, we present them to
        # the user to make a choice.
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_success_response(self):
        return redirect('checkout:shipping-address')
