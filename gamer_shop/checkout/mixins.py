from oscar.apps.checkout.mixins import *


class OrderPlacementMixin(OrderPlacementMixin):

    def get_message_context(self, order):
        ctx = super().get_message_context(order)
        ctx['computer_lines'] = order.computer_lines.all()
        return ctx
