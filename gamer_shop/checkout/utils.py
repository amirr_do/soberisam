from oscar.apps.checkout.utils import CheckoutSessionData as CS


class CheckoutSessionData(CS):

    def ship_to_user_address(self, address):
        """
        Use an user address (from an address book) as the shipping address.
        """
        code = self.shipping_method_code(None)
        self.reset_shipping_data()
        self._set('shipping', 'user_address_id', address.id)
        self._set('shipping', 'method_code', code)
