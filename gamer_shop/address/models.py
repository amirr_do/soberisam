from oscar.apps.address.abstract_models import AbstractUserAddress


class UserAddress(AbstractUserAddress):
    def clean(self):
        # Strip all whitespace
        for field in ['first_name', 'last_name', 'line1', 'line2', 'line3',
                      'line4', 'state', 'postcode']:
            if self.__dict__[field]:
                self.__dict__[field] = self.__dict__[field].strip()
    #
    # @property
    # def city(self):
    #     # Common alias
    #     return self.line1

from oscar.apps.address.models import *  # noqa
