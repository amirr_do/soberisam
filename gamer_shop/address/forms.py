from django.forms import Textarea
from django.utils.translation import ugettext_lazy as _

from oscar.apps.address.forms import *

Country = get_model('address', 'Country')


class UserAddressForm(UserAddressForm):

    class Meta:
        model = UserAddress
        fields = ['first_name', 'line1', 'phone_number', 'country', 'notes']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.adjust_country_field()

    def adjust_country_field(self):

        countries = Country._default_manager.filter(
            is_shipping_country=True)

        # No need to show country dropdown if there is only one option
        if len(countries) == 1:
            self.fields.pop('country', None)
            self.instance.country = countries[0]
        else:
            self.fields['country'].queryset = countries
            self.fields['country'].empty_label = None
        self.fields['line1'].label = _('Address')
        self.fields['first_name'].label = _('FIO')
        self.fields['line1'].widget = Textarea()
