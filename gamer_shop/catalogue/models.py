import os
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.conf import settings
from django.core.cache.utils import make_template_fragment_key

from oscar.apps.catalogue.abstract_models import AbstractProduct, AbstractCategory  # noqa


class Category(AbstractCategory):
    def get_build_absolute_url(self):
        return reverse('build:category',
                       kwargs={'category_slug': self.slug})

    def get_missing_image(self):
        return MissingCategoryImage()

    def primary_image(self):
        """
        Returns the primary image for a product. Usually used when one can
        only display one product image, e.g. in a list of products.
        """
        if self.image.name:
            return self.image
        else:
            return self.get_missing_image()


class MissingCategoryImage(object):
    def __init__(self, name=None):
        self.name = name if name else settings.OSCAR_MISSING_IMAGE_URL
        media_file_path = os.path.join(settings.MEDIA_ROOT, self.name)
        # don't try to symlink if MEDIA_ROOT is not set (e.g. running tests)
        if settings.MEDIA_ROOT and not os.path.exists(media_file_path):
            self.symlink_missing_image(media_file_path)

    def symlink_missing_image(self, media_file_path):
        static_file_path = find('oscar/img/%s' % self.name)
        if static_file_path is not None:
            try:
                os.symlink(static_file_path, media_file_path)
            except OSError:
                raise ImproperlyConfigured((
                                               "Please copy/symlink the "
                                               "'missing image' image at %s into your MEDIA_ROOT at %s. "
                                               "This exception was raised because Oscar was unable to "
                                               "symlink it for you.") % (media_file_path,
                                                                         settings.MEDIA_ROOT))
            else:
                logging.info((
                                 "Symlinked the 'missing image' image at %s into your "
                                 "MEDIA_ROOT at %s") % (media_file_path,
                                                        settings.MEDIA_ROOT))


class Product(AbstractProduct):
    short_description = models.TextField(_('Short description'), blank=True)

    def get_build_absolute_url(self):
        """
        Return a product's absolute url
        """
        return reverse('build:detail',
                       kwargs={'product_slug': self.slug, 'pk': self.id})

    def cache_delete(self, computers):
        cache_delete = lambda k, vs: cache.delete(make_template_fragment_key(k, vs))

        for computer in computers:
            cache_delete('current_comp_price', [computer.pk])
            cache_delete('current_build_price', [computer.pk])
            cache_delete('comp_price', [computer.pk])
            cache_delete('comp_button_buy', [computer.pk])
            cache_delete('short_comp_info', [computer.pk])
            # cache_delete('basket_total', [self.request.basket_hash])

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        from gamer_shop.computer import models as computer_models
        computers = list()
        if self.product_class_id == 1:
            computers = computer_models.Computer.objects.filter(cpu=self)
        elif self.product_class_id == 2:
            computers = computer_models.Computer.objects.filter(cooler=self)
        elif self.product_class_id == 3:
            computers = computer_models.Computer.objects.filter(motherboard=self)
        elif self.product_class_id == 9:
            computers = computer_models.Computer.objects.filter(case=self)
        elif self.product_class_id == 4:
            computers = list(map(lambda comp: comp.computer, computer_models.ComputerRAM.objects.filter(ram=self)))
        elif self.product_class_id == 5:
            computers = list(
                map(lambda comp: comp.computer, computer_models.ComputerVideoCard.objects.filter(video_card=self)))
        elif self.product_class_id == 6:
            computers = list(map(lambda comp: comp.computer, computer_models.ComputerHDD.objects.filter(hdd=self)))
        elif self.product_class_id == 7:
            computers = list(map(lambda comp: comp.computer, computer_models.ComputerSSD.objects.filter(ssd=self)))
        elif self.product_class_id == 8:
            computers = list(map(lambda comp: comp.computer, computer_models.ComputerODD.objects.filter(odd=self)))
        elif self.product_class_id == 10:
            computers = list(map(lambda comp: comp.computer, computer_models.ComputerPSU.objects.filter(psu=self)))

        self.cache_delete(computers)


class CPU(Product):
    class Meta:
        verbose_name = _('Processor')
        verbose_name_plural = _('Processors')


class Cooler(Product):
    class Meta:
        verbose_name = _('Cooler')
        verbose_name_plural = _('Coolers')


class Motherboard(Product):
    class Meta:
        verbose_name = _('Motherboard')
        verbose_name_plural = _('Motherboards')


class RAM(Product):
    class Meta:
        verbose_name = _('RAM')
        verbose_name_plural = _('RAMs')


class VideoCard(Product):
    class Meta:
        verbose_name = _('Video card')
        verbose_name_plural = _('Video cards')


class HDD(Product):
    class Meta:
        verbose_name = _('HDD')
        verbose_name_plural = _('HDDs')


class SSD(Product):
    class Meta:
        verbose_name = _('SSD')
        verbose_name_plural = _('SSDs')


class ODD(Product):
    class Meta:
        verbose_name = _('ODD')
        verbose_name_plural = _('ODDs')


class Case(Product):
    class Meta:
        verbose_name = _('Case')
        verbose_name_plural = _('Cases')


class PSU(Product):
    class Meta:
        verbose_name = _('PSU')
        verbose_name_plural = _('PSUs')


from oscar.apps.catalogue.models import *  # noqa
