from oscar.apps.catalogue import config


class CatalogueConfig(config.CatalogueConfig):
    name = 'gamer_shop.catalogue'
