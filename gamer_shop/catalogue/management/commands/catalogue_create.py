from __future__ import unicode_literals
# -*- coding: utf-8 -*-
import time
import json
from os import listdir
from os import remove
import os

from django.db.utils import IntegrityError

try:
    from urllib.request import urlretrieve
except ImportError:
    from urllib import urlretrieve
from io import BytesIO
from django.core.files import File
import requests
import re

from django.core import management

from django.core.management.base import BaseCommand

from gamer_shop.catalogue.models import CPU, Cooler, Motherboard, RAM, VideoCard, HDD, SSD, ODD, Case, PSU, Product
from gamer_shop.catalogue.models import ProductAttributeValue, ProductAttribute
from gamer_shop.catalogue.models import ProductClass, ProductCategory, Category, ProductImage
from gamer_shop.partner.models import StockRecord, Partner, ExchangeRates


class Profiler(object):
    """
    Simple profiler.
    """

    def __enter__(self):
        self._startTime = time.time()

    def __exit__(self, type, value, traceback):
        print('Elapsed time: {:.3f} sec'.format(
                time.time() - self._startTime
        ))


def func_time(func):
    def wrap(*args, **kwargs):
        t = time.time()
        result = func(*args, **kwargs)
        print("{}: {}".format(func.__name__, round(time.time() - t, 4)))
        return result

    return wrap


def adapt_category(fn):
    def wrapper(load):
        data = fn(load)
        return normalize_code(data)

    return wrapper


def authorization(func):
    def wrapper(*args, **kwargs):
        sid = auth('soberisamvse', '4164cedcf7f7d8eac70a4d054796148b')
        result = func(sid, *args, **kwargs)
        if result:
            return result
        logout(sid)

    return wrapper


def normalize_code(data):
    for product in data[0]:
        if int(product['category']) == 1097:
            product['category'] = 1
        elif int(product['category']) == 1108:
            product['category'] = 2
        elif int(product['category']) == 1264:
            product['category'] = 3
        elif int(product['category']) == 1334:
            product['category'] = 4
        elif int(product['category']) == 1403:
            product['category'] = 5
        elif int(product['category']) == 1361:
            product['category'] = 6
        elif int(product['category']) == 1235:
            product['category'] = 7
        elif int(product['category']) == 1377:
            product['category'] = 8
        elif int(product['category']) == 1441:
            product['category'] = 9
        elif int(product['category']) == 1442:
            product['category'] = 10
    return data


def print_attr(attr):
    if attr:
        print(attr)


def print_attr_none(attr):
    print(attr)


def ld(p, encoding="utf8"):
    """загрузка объекта"""
    with open(p, "rt", encoding=encoding) as f:
        return json.load(f)


def parts(product_class):
    if int(product_class) == 1:
        return 'cpu'
    elif int(product_class) == 2:
        return 'cooler'
    elif int(product_class) == 3:
        return 'motherboard'
    elif int(product_class) == 4:
        return 'ram'
    elif int(product_class) == 5:
        return 'videocard'
    elif int(product_class) == 6:
        return 'hdd'
    elif int(product_class) == 7:
        return 'ssd'
    elif int(product_class) == 8:
        return 'odd'
    elif int(product_class) == 9:
        return 'case'
    elif int(product_class) == 10:
        return 'psu'


def is_correct_ram(name):
    bad_ram_types = ('SODIMM', 'DDR2', 'DDR-2', 'DDR ')
    return not any(list(map(lambda x: x in name.upper(), bad_ram_types)))


@adapt_category
def load_brain_price(load=False):
    if load:
        load_price()
    brain_list = list()
    brain_codes = list()
    my_ids = (1441, 1442, 1097, 1264, 1334, 1108, 1361, 1235, 1377, 1403)
    price = ld('price.json')
    for product_id in price.keys():
        param = price[product_id]
        category_id = param["CategoryID"]
        if int(category_id) in my_ids and is_correct_ram(param["Name"]):
            product_item = {
                'warranty': param["Warranty"],
                'code': param["Code"],
                'group': param["Group"],
                'name': param["Name"],
                'category_name': param["CategoryName"],
                'category': category_id,
                'url': param["URL"],
                'bonus': param["Bonus"],
                'day_delivery': param["DayDelivery"],
                'price_usd': param["PriceUSD"],
                'note': param["Note"],
                'vendor': param["Vendor"],
                'price_ind': param["Price_ind"],
                'ddp': param["DDP"],
                'article': param["Article"],
                'model': param["Model"],
                'stock': param["Stock"],
                'recommended_price': param["RecommendedPrice"],
                'product': param["ProductID"],
                'description': param["Description"],
            }
            brain_list.append(product_item.copy())
            brain_codes.append(param["Code"])
    return brain_list, brain_codes


def auth(login, password):
    return requests.post('http://api.brain.com.ua/auth', data={'login': login, 'password': password}).json()["result"]


@authorization
def download(sid, directory):
    price = requests.get('http://api.brain.com.ua/pricelists/29/json/' + str(sid) + '?full=1')
    url = price.json()['url']

    files = listdir(directory)
    jsonfiles = filter(lambda x: x.endswith('.json') and x.startswith('price.'), files)
    for i in jsonfiles:
        remove(i)

    destination = 'price.json'
    urlretrieve(url, destination)


def logout(sid):
    print(requests.post('http://api.brain.com.ua/logout/' + str(sid)).text)


def load_price():
    directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), '')
    download(directory=directory)


def get_more_info(self, product_id):
    try:
        info = requests.get('http://api.brain.com.ua/product/{}/{}'.format(product_id, self.sid))
        return info.json()['result']
    except ValueError as err:
        print(info.text)
        raise ValueError(err)


def get_price(price, attr):
    if attr['category'] == 1:
        if price < 150:
            price += 6
        elif price < 250:
            price += 9
        else:
            price += 12
    elif attr['category'] == 2:
        if price < 15:
            price += 3
        elif price < 25:
            price += 4
        elif price < 45:
            price += 5
        else:
            price += 7
    elif attr['category'] == 3:
        if price < 60:
            price += 4
        elif price < 100:
            price += 5
        elif price < 200:
            price += 6
        elif price < 300:
            price += 7
        elif price < 500:
            price += 8
        else:
            price += 10
    elif attr['category'] == 4:
        if price < 30:
            price += 3
        elif price < 60:
            price += 4
        elif price < 90:
            price += 5
        elif price < 150:
            price += 6
        else:
            price += 8
    elif attr['category'] == 5:
        if price < 50:
            price += 3
        elif price < 80:
            price += 4
        elif price < 120:
            price += 5
        elif price < 200:
            price += 6
        elif price < 400:
            price += 7
        elif price < 600:
            price += 9
        elif price < 800:
            price += 12
        else:
            price += 15
    elif attr['category'] == 6:
        if price < 50:
            price += 3
        elif price < 80:
            price += 4
        elif price < 120:
            price += 5
        elif price < 150:
            price += 6
        else:
            price += 8
    elif attr['category'] == 7:
        if price < 50:
            price += 3
        elif price < 80:
            price += 4
        elif price < 150:
            price += 5
        elif price < 250:
            price += 6
        elif price < 350:
            price += 7
        else:
            price += 9
    elif attr['category'] == 8:
        if price < 30:
            price += 3
        elif price < 50:
            price += 4
        elif price < 80:
            price += 5
        elif price < 120:
            price += 6
        else:
            price += 7
    elif attr['category'] == 9:
        if price < 40:
            price += 4
        elif price < 70:
            price += 5
        elif price < 110:
            price += 6
        else:
            price += 8
    elif attr['category'] == 10:
        if price < 50:
            price += 4
        elif price < 80:
            price += 6
        elif price < 120:
            price += 7
        else:
            price += 8
    return price


def get_images(image):
    images = list()
    image_item = image
    i = 2
    while str(requests.get(image_item).status_code) == '200':
        images.append(image_item)
        image_item = image.replace('big', '{}big'.format(i))
        i += 1
    return images


def get_product_type(category):
    if int(category) == 1:
        return CPU
    elif int(category) == 2:
        return Cooler
    elif int(category) == 3:
        return Motherboard
    elif int(category) == 4:
        return RAM
    elif int(category) == 5:
        return VideoCard
    elif int(category) == 6:
        return HDD
    elif int(category) == 7:
        return SSD
    elif int(category) == 8:
        return ODD
    elif int(category) == 9:
        return Case
    elif int(category) == 10:
        return PSU
    return


def set_rate(self):
    partner = Partner.objects.get(code='brain')
    pending = Partner.objects.get(code='pending')
    soberisam = Partner.objects.get(code='soberisam')
    rate_raw = requests.get('http://api.brain.com.ua/currencies/{}'.format(self.sid))
    for i in rate_raw.json()['result']:
        if i['name'] == 'грн нал':
            rate = float(i['value'])
            diff_rate = 1

            brain_rate = ExchangeRates.objects.filter(partner=partner)
            soberisam_rate = ExchangeRates.objects.filter(partner=soberisam)
            pending_rate = ExchangeRates.objects.filter(partner=pending)
            if brain_rate:
                brain_rate = brain_rate.latest('date_created')
                if brain_rate.rate != rate:
                    ExchangeRates.objects.create(rate=rate, partner=partner)
            else:
                ExchangeRates.objects.create(rate=rate, partner=partner)
            if soberisam_rate:
                soberisam_rate = soberisam_rate.latest('date_created')
                if soberisam_rate.rate != rate + diff_rate:
                    ExchangeRates.objects.create(rate=rate + diff_rate, partner=soberisam)
            else:
                ExchangeRates.objects.create(rate=rate + diff_rate, partner=soberisam)
            if pending_rate:
                pending_rate = pending_rate.latest('date_created')
                if pending_rate.rate != rate + diff_rate:
                    ExchangeRates.objects.create(rate=rate + diff_rate, partner=pending)
            else:
                ExchangeRates.objects.create(rate=rate + diff_rate, partner=pending)

            return
    print('ERROR RATE')


def get_options(c, options):
    if not options:
        try:
            option = c.attr.options.replace('"', '').replace("'", '"').replace(' None}', ' "None"}')
            return json.loads(option, encoding='uft8')
        except AttributeError:
            return None
        except ValueError as err:
            print(c.attr.options)
            print(err)
    return options


def get_attribute(c, options, attr_name):
    options = get_options(c, options)
    if not options:
        return None

    for o in options:
        if o['name'] == attr_name:
            return o['value']


def get_attributes(c, options, attr_name, separator=','):
    options = get_options(c, options)
    if not options:
        return None

    values = list()
    for o in options:
        if o['name'] == attr_name:
            for value in o['value'].split(','):
                values.append(value.replace(separator, '').strip())
    return values


def get_cpu_core(c, options=None):

    cpu_core = get_attribute(c, options, 'Количество ядер')
    if cpu_core:
        pattern = '\d+'
        found = re.findall(pattern, cpu_core)
    else:
        return None

    if len(found) == 1:
        try:
            return int(found[0])
        except TypeError as err:
            print(err)
            return None
    else:
        print('Error: More or less values')
        return None


def get_cpu_socket(c, options=None):
    cpu_socket = get_attribute(c, options, 'Тип сокета')
    return cpu_socket.replace('s', 'LGA').replace('S', 'LGA') if cpu_socket else None


def get_cpu_box(c, options=None):
    cpu_box = get_attribute(c, options, 'Тип поставки')
    if cpu_box:
        if cpu_box.upper() == 'BOX':
            return True
        if cpu_box.upper() == 'TRAY':
            return False
    return None


def get_cpu_nm(c, options=None):
    cpu_nm = get_attribute(c, options, 'Технология CMOS')
    return cpu_nm.replace(' ', '') if cpu_nm else None


def get_cpu_power(c, options=None):
    cpu_power = get_attribute(c, options, 'Теплопакет')
    return cpu_power.replace('W', '') if cpu_power else None


def get_cpu_freq(c, options=None):
    cpu_freq = get_attribute(c, options, 'Тактовая частота ядра')
    if cpu_freq:
        pattern = '\d+[\.,]\d+(?=\s*GHz)'
        found = re.findall(pattern, cpu_freq)
        if len(found) == 1:
            freq = float(found[0].replace(',', '.'))
            return freq
    return None


def get_cooler_socket(c, options=None):
    pattern = '(?<=;)\s*s?(1155|1150|2011|AM3\+?|[FA]M1|FM2\+?)\s{0,3}(?=;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found):
        return found
    return None


def get_cooler_circulation(c, options=None):
    circulation = get_attribute(c, options, 'Частота вращения')
    if circulation:
        pattern = '\s{0,3}(\d{1,5})\s{0,3}\-?\s{0,3}(\d{0,5})\s{0,3}(?=об[/минхв± 10%]*)'
        found = re.findall(pattern, circulation)
        if len(found) == 1:
            circulation = found[0]
            if circulation[0] and circulation[1]:
                return '{}'.format('-'.join(circulation))
            else:
                return '{}'.format(circulation[0])
    return None


def get_cooler_dB(c, options=None):
    dB = get_attribute(c, options, 'Уровень шума')
    if dB:
        pattern = '\s{0,3}[<>]{0,3}\s{0,3}(\d{1,3}[.]?\d{0,3})\s{0,3}[-]' \
                  '?\s{0,3}(\d{1,3}[.]?\d{0,3})?\s{0,3}(?=[dд][BBбБ])'
        found = re.findall(pattern, dB)
        if len(found) == 1:
            dB = found[0]
            if dB[0] and dB[1]:
                return '{}'.format('-'.join(dB))
            else:
                return '{}'.format(dB[0])
    return None


def get_cooler_material(c, options=None):
    return get_attribute(c, options, 'Материал радиатора')


def get_cooler_connect(c, options=None):
    connect = get_attribute(c, options, 'Тип коннектора')
    if connect:
        pattern = '\d+'
        found = re.findall(pattern, connect)
        if len(found) == 1:
            return '{}'.format(found[0])
    return None


def get_cooler_type(c, options=None):
    cooler_type = get_attribute(c, options, 'Тип системы охлаждения')
    if cooler_type:
        if 'жидкост' in cooler_type:
            return 'жидкостное охлаждение'
        if 'активн' in cooler_type:
            return 'активное охлаждение (кулер)'
        if 'пасивн' in cooler_type or 'радиатор' in cooler_type:
            return 'пасивное охлаждение (радиатор)'
    return None


def get_motherboard_socket(c, options=None):
    socket = get_attribute(c, options, 'Сокет')
    if socket:
        socket = socket.replace('Socket ', '').replace('С встроенным процессором', 'встроенный').strip()
        return '{}'.format(socket)
    return None


def get_motherboard_chipset(c, options=None):
    return get_attribute(c, options, 'Чипсет (северный мост)')


def get_motherboard_size(c, options=None):
    form_factor = get_attribute(c, options, 'Форм-фактор')
    atx, mm = 'unknown', None
    if form_factor:
        pattern = '([\w\d\s-]*[AI]TX[\w\d\s]*)'
        found = re.findall(pattern, form_factor)
        if found:
            size = found[-1].strip()
            atx = '{}'.format(size)
            pattern = 'm[.]?ATX|E\s{0,3}ATX|Micro ATX'
            found = re.findall(pattern, atx, re.IGNORECASE)
            if found:
                atx = 'Micro-ATX'
            else:
                atx = 'Mini-ATX' if atx == 'Mini ATX' else atx
                atx = 'Mini-ITX' if atx == 'Mini ITX' else atx

    size = get_attribute(c, options, 'Физические размеры')
    if size:
        pattern = '(\d+\.?\d+)'
        found = re.findall(pattern, size)
        found = [str(int(float(f) * 10)) if '.' in f or 'cm' in size or 'см' in size else f for f in found]
        mm = ' x '.join(found)
    if atx in ('Micro-ATX', 'Mini-ATX', 'ATX', 'Mini-ITX', 'Flex-ATX', 'Ultra-ATX', 'unknown'):
        return (atx, mm), True
    else:
        return (atx, mm), False


def get_ram_type(c, options=None):
    type_ram = get_attribute(c, options, 'Тип памяти')
    if type_ram:
        return type_ram.replace(' ', '')
    return None


def get_ram_qty_and_size(c, options=None):
    qty = get_attribute(c, options, 'Количество модулей в наборе')
    if qty:
        try:
            qty = int(qty)
        except ValueError:
            try:
                qty = int(re.search('\d+', qty).group())
            except ValueError:
                qty = None
            except TypeError:
                qty = None
    size = get_attribute(c, options, 'Объем памяти')
    if size:
        size = int(re.search('\d+', size).group())
    return qty, size


def get_ram_frequency(c, options=None):
    frequency = get_attribute(c, options, 'Частота памяти')
    if frequency:
        return int(re.search('\d+', frequency).group())
    return None


def get_ram_latency(c, options=None):
    return get_attribute(c, options, 'Тайминги')


def get_ram_radiator(c, options=None):
    return get_attribute(c, options, 'Охлаждение')


def get_video_size(c, options=None):
    size = get_attribute(c, options, 'Объем памяти')
    if size:
        return int(re.search('\d+', size).group())
    return None


def get_video_type(c, options=None):
    type_ram = get_attribute(c, options, 'Тип памяти')
    if type_ram:
        type_ram = type_ram.replace(' ', '')
        type_ram = 'G' + type_ram if type_ram[:3] == 'DDR' else type_ram
        return 'GDDR3' if 'III' in type_ram else type_ram
    return None


def get_video_bit(c, options=None):
    bit = get_attribute(c, options, 'Битность')
    if bit:
        return int(re.search('\d+', bit).group())
    return None


def get_video_cooling(c, options=None):
    return get_attribute(c, options, 'Охлаждение')


def get_video_gpu(c, options=None):
    gpu = get_attribute(c, options, 'Графический чипсет')
    if gpu:
        pattern = '\s*GeForce\s*(G?T?X?)\s*(\d)\d+\s?[\s\w\d]*'
        found = re.findall(pattern, gpu)
        if len(found):
            return 'GeForce {}{}00 series'.format(found[-1][0], found[-1][1])

        pattern = '\s*Radeon\s*(R\d)[\d\w\s]+'
        found = re.findall(pattern, gpu)
        if len(found):
            return 'Radeon {} series'.format(found[-1])
        pattern = '\s*Radeon\s*(HD\s*\d)[\d\w\s]*'
        found = re.findall(pattern, gpu)
        if len(found):
            return 'Radeon {}000 series'.format(found[-1])

        pattern = 'Quadro|QUADRO'
        found = re.search(pattern, gpu)
        if found:
            return 'NVIDIA QUADRO'
    return None


def get_hdd_size(c, options=None):
    size = get_attribute(c, options, 'Объем')
    if size:
        size = re.search('\d+', size)
        if size:
            size = int(size.group())
            return size if size > 22 else int(float(size) * 1000)
    return None


def get_hdd_form_factor(c, options=None):
    form_factor = get_attribute(c, options, 'Модель')
    if form_factor:
        form_factor = re.search('\d+\.\d+', form_factor)
        if form_factor:
            form_factor = form_factor.group()
            return form_factor
    return None


def get_hdd_speed(c, options=None):
    speed = get_attribute(c, options, 'Скорость вращения шпинделя')
    if speed:
        if speed == 'IntelliPower':
            return speed
        pattern = '\s*([\d-]+)\s*'
        found = re.search(pattern, speed)
        if found:
            return '{}'.format(found.group())
    return None


def get_hdd_buffer_size(c, options=None):
    buffer = get_attribute(c, options, 'Размер буфера')
    if buffer:
        buffer = re.search('\d+', buffer)
        if buffer:
            return int(buffer.group())
    return None


def get_ssd_size(c, options=None):
    size = get_attribute(c, options, 'Объём памяти')
    if not size:
        size = get_attribute(c, options, 'Объем памяти')
    if size:
        size = re.search('\d+', size)
        if size:
            size = int(size.group())
            return size if size > 5 else int(float(size) * 1000)
    return None


def get_ssd_interface(c, options=None):
    interface = get_attribute(c, options, 'Интерфейс')
    if interface:
        pattern = '\s*(mSATA|SAS)\s*'
        msata = re.findall(pattern, interface)
        if len(msata):
            return '{}'.format(msata[-1])
        pattern = '\s*(SATA)\s*(.*?)'
        sata = re.findall(pattern, interface)
        if len(sata) == 1:
            t = 'III' if sata[0][1] == '3' or '6Gb/s' else sata[0][1]
            return '{} {}'.format(sata[0][0], t)
        pattern = '\s*(PCI-Express)\s*'
        pci = re.findall(pattern, interface)
        if len(pci) == 1:
            return '{}'.format(pci[0])
    return None


def get_ssd_io(c, options=None):
    read = get_attribute(c, options, 'Скорость чтения, макс.')
    write = get_attribute(c, options, 'Скорость записи, макс.')
    if read:
        read = re.search('\d{2,4}', read)
        if read:
            read = int(read.group())
    if write:
        write = re.search('\d{2,4}', write)
        if write:
            write = int(write.group())
    return read, write


def get_case_class(c, options=None):
    return get_attribute(c, options, 'Класс корпуса')


def get_case_type(c, options=None):
    type_case = get_attribute(c, options, 'Типоразмер')
    if type_case:
        if type_case == 'ATX':
            return 'Miditower'
        else:
            return type_case
    return None


def get_case_form_factor(c, options=None):
    form_factor = get_attributes(c, options, 'Форм-фактор МП')
    if form_factor:
        return ','.join(form_factor)
    return None


def get_case_power(c, options=None):
    power = get_attribute(c, options, 'Мощность блока питания')
    if power:
        power = re.search('\d+', power)
        if power:
            power = int(power.group())
            return power
        else:
            return 0
    return None


def get_psu_power(c, options=None):
    power = get_attribute(c, options, 'Мощность')
    if power:
        power = re.search('\d+', power)
        if power:
            power = int(power.group())
            return power
    return None


'''
=== Main block ===
'''


def update_or_create_product(self, brain_product, code):
    Product = get_product_type(brain_product['category'])
    name = brain_product['name'].replace('"', "'")
    short_description = brain_product['description'].replace('"', "'")
    product_class = ProductClass.objects.get(pk=int(brain_product['category']))

    product, created = Product.objects.get_or_create(upc=code, defaults={
        'structure': 'standalone',
        'title': name,
        'slug': code,
        'short_description': short_description,
        'is_discountable': True,
        'product_class': product_class
    })

    if created:
        create_product(self, product, brain_product)
    else:
        product.short_description = short_description
        product.save()

    return product, created


@func_time
def create_product(self, catalogue_product, product):
    try:
        more_info = get_more_info(self, product['product'])
    except ValueError as err:
        print(err)
    else:
        catalogue_product.description = more_info['description']
        catalogue_product.save()

        set_images(catalogue_product, more_info)

        set_attributes(catalogue_product, product, more_info)


# @func_time
def set_images(product, info):
    images = get_images(info['large_image'])
    if images:
        create_product_image(product, images)


def create_product_image(product, images):
    for i, link in enumerate(images):
        image = requests.get(link)
        img = BytesIO(image.content)

        image_name = link.split('/')[-1]
        image_file = File(img)

        product_image = ProductImage.objects.create(product=product, display_order=i)
        product_image.original.save(image_name, image_file)


def get_correct_mb_atx(height, width):
    if 307 > height > 285:
        if width < 247:
            return 'ATX'
        else:
            return 'Ultra-ATX'
    if height > 245:
        if width < 208:
            return 'Mini-ATX'
        elif width < 246:
            return 'ATX'
    if height > 170:
        if width > 191:
            return 'Micro-ATX'
        else:
            return 'Micro-ATX'
    if height < 171:
        return 'Mini-ITX'
    return 'Ultra-ATX'


# @func_time
def set_attributes(product, attr, more_info):
    attrs = {
        'warranty': attr['warranty'],
        'vendor': attr['vendor'],
        'article': attr['article'],
        'model': attr['model'],
        'options': more_info['options']
    }
    for key in attrs.keys():
        ProductAttributeValue.objects.get_or_create(
                product=product, attribute=ProductAttribute.objects.get(code=key), defaults={'value_text': attrs[key]})
    attributes = dict()
    if attr['category'] == 1:
        attributes = {
            'core': get_cpu_core(product, attrs['options']),
            'socket': get_cpu_socket(product, attrs['options']),
            'traybox': get_cpu_box(product, attrs['options']),
            'nm': get_cpu_nm(product, attrs['options']),
            'power': get_cpu_power(product, attrs['options']),
            'freq': get_cpu_freq(product, attrs['options'])
        }
    elif attr['category'] == 2:
        attributes = {
            'circulation': get_cooler_circulation(product, attrs['options']),
            'dB': get_cooler_dB(product, attrs['options']),
            'material': get_cooler_material(product, attrs['options']),
            'connect': get_cooler_connect(product, attrs['options']),
            'type_cooler': get_cooler_type(product, attrs['options'])
        }
    elif attr['category'] == 3:
        size, _ = get_motherboard_size(product, attrs['options'])
        attributes = {
            'socket': get_motherboard_socket(product, attrs['options']),
            'chipset': get_motherboard_chipset(product, attrs['options']),
            'form_factor': size[0],
            'size': size[1],
        }
    elif attr['category'] == 4:
        qty, volume = get_ram_qty_and_size(product, attrs['options'])
        attributes = {
            'type_ram': get_ram_type(product, attrs['options']),
            'frequency': get_ram_frequency(product, attrs['options']),
            'latency': get_ram_latency(product, attrs['options']),
            'radiator': get_ram_radiator(product, attrs['options']),
            'qty': qty,
            'volume': volume,
        }
    elif attr['category'] == 5:
        attributes = {
            'cooling': get_video_cooling(product, attrs['options']),
            'gpu': get_video_gpu(product, attrs['options']),
            'volume_memory': get_video_size(product, attrs['options']),
            'video_type': get_video_type(product, attrs['options']),
            'bit': get_video_bit(product, attrs['options']),
        }
    elif attr['category'] == 6:
        attributes = {
            'volume': get_hdd_size(product, attrs['options']),
            'form_factor': get_hdd_form_factor(product, attrs['options']),
            'speed': get_hdd_speed(product, attrs['options']),
            'buffer_size': get_hdd_buffer_size(product, attrs['options']),
        }
    elif attr['category'] == 7:
        read, write = get_ssd_io(product, attrs['options'])
        attributes = {
            'volume': get_ssd_size(product, attrs['options']),
            'interface': get_ssd_interface(product, attrs['options']),
            'read_speed': read,
            'write_speed': write,
        }
    elif attr['category'] == 9:
        attributes = {
            'class_case': get_case_class(product, attrs['options']),
            'type_case': get_case_type(product, attrs['options']),
            'power': get_case_power(product, attrs['options']),
        }
    elif attr['category'] == 10:
        attributes = {
            'power': get_psu_power(product, attrs['options'])
        }
    add_attr(product, attributes)
    print_none_attr(product, attributes, attrs['options'])


def add_attr(product, attr):
    for key in attr.keys():
        if attr[key] is not None:
            setattr(product.attr, key, attr[key])
    product.attr.save()


def print_none_attr(product, attr, options=None):
    options = get_options(product, options)
    if options is None:
        return
    attr_none = []
    for k in attr.keys():
        if attr[k] is None:
            attr_none.append(k)

    if attr_none:
        print()
        print([k for k in attr_none])

        print(product.pk, product.upc, options)


def del_products(brain_codes):
    zz = list()
    products = Product.objects.all()
    print(len(brain_codes), sorted(brain_codes))
    pending = Partner.objects.get(code='pending')
    soberisam = Partner.objects.get(code='soberisam')
    for product in products:
        if product.upc not in brain_codes:
            # print(product.upc, product.get_title())
            StockRecord.objects.filter(product=product, partner=pending).update(num_in_stock=0)
            StockRecord.objects.filter(product=product, partner=soberisam).update(num_in_stock=0)
            zz.append(product.upc)
    print(len(zz), sorted(zz))


# @func_time
def product_stock_record(product, attr):
    bad = Partner.objects.get(code='bad')
    partner = Partner.objects.get(code='brain')
    pending = Partner.objects.get(code='pending')
    soberisam = Partner.objects.get(code='soberisam')
    code = attr['code']
    stock = attr["stock"] if attr['stock'] > 0 else int(attr['day_delivery'] > 0)
    price_brain = float(attr['price_usd'])
    price = get_price(price_brain, attr)

    partner_stockrecord, _ = StockRecord.objects.update_or_create(
            product=product, partner=partner, defaults={
                'partner_sku': code,
                'price_excl_tax': price_brain,
                'num_in_stock': stock * 20,
                'price_currency': 'UAH'
            })

    soberisam_stockrecord = StockRecord.objects.filter(product=product, partner=soberisam)
    if soberisam_stockrecord.exists():
        soberisam_stockrecord.update(partner_sku=code, price_excl_tax=price,
                                     num_in_stock=stock * 20, price_currency='UAH')
        StockRecord.objects.filter(product=product, partner=pending).delete()
    elif not StockRecord.objects.filter(product=product, partner=bad).exists():
        pending_stockrecord, _ = StockRecord.objects.update_or_create(
                product=product, partner=pending, defaults={
                    'partner_sku': code,
                    'price_excl_tax': price,
                    'num_in_stock': stock * 20,
                    'price_currency': 'UAH'
                })
    return attr['name']


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.sid = auth('soberisamvse', '4164cedcf7f7d8eac70a4d054796148b')

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(Command, self).__exit__(exc_type, exc_val, exc_tb)
        logout(self.sid)

    def handle(self, *args, **options):

        if 'db' in args:
            brain_price, brain_codes = load_brain_price(True)
            for brain_product in brain_price:

                # Product Info
                try:
                    product, created = update_or_create_product(self, brain_product, brain_product['code'])
                except IntegrityError as err:
                    print(err)
                else:
                    # Stock Record
                    product_stock_record(product, brain_product)

                    # Product Category
                    if created:
                        category = Category.objects.get(pk=int(brain_product['category']) + 1)
                        ProductCategory.objects.get_or_create(category=category, product=product)

            del_products(brain_codes)

            # set_rate(self)
            management.call_command('update_index')

        if 'cpu' in args:
            processors = CPU.objects.all()
            for cpu in processors:
                attr = {
                    'core': get_cpu_core(cpu),
                    'socket': get_cpu_socket(cpu),
                    'traybox': get_cpu_box(cpu),
                    'nm': get_cpu_nm(cpu),
                    'power': get_cpu_power(cpu),
                    'freq': get_cpu_freq(cpu),
                }

                add_attr(cpu, attr)
                print_none_attr(cpu, attr)

        if 'cooler' in args:
            coolers = Cooler.objects.all()
            for cooler in coolers:

                # socket = get_cooler_socket(cooler)

                attr = {
                    'material': get_cooler_material(cooler),
                    'type_cooler': get_cooler_type(cooler)
                }

                if attr['type_cooler'] and 'радиатор' in attr['type_cooler']:
                    attr['dB'] = 'нет'
                    attr['circulation'] = 'нет'
                    attr['connect'] = 'нет'
                else:
                    attr['dB'] = get_cooler_dB(cooler)
                    attr['circulation'] = get_cooler_circulation(cooler),
                    attr['connect'] = get_cooler_connect(cooler)

                add_attr(cooler, attr)
                print_none_attr(cooler, attr)

                # print(socket)
                pass

        if 'mb' in args:
            motherboards = Motherboard.objects.all()
            # all_sizes = defaultdict(list)
            for motherboard in motherboards:
                size, _ = get_motherboard_size(motherboard)
                attr = {
                    'socket': get_motherboard_socket(motherboard),
                    'chipset': get_motherboard_chipset(motherboard),
                    'form_factor': size[0],
                    'size': size[1],
                }

                # add_attr(motherboard, attr)
                # print_none_attr(motherboard, attr)

        if 'ram' in args:
            rams = RAM.objects.all()
            for ram in rams:
                qty, volume = get_ram_qty_and_size(ram)
                attr = {
                    'type_ram': get_ram_type(ram),
                    'frequency': get_ram_frequency(ram),
                    'latency': get_ram_latency(ram),
                    'radiator': get_ram_radiator(ram),
                    'qty': qty,
                    'volume': volume,
                }

                add_attr(ram, attr)
                print_none_attr(ram, attr)

        if 'video' in args:
            video_card = VideoCard.objects.all()
            for video in video_card:
                attr = {
                    'cooling': get_video_cooling(video),
                    'gpu': get_video_gpu(video),
                    'volume_memory': get_video_size(video),
                    'type_video': get_video_type(video),
                    'bit': get_video_bit(video),
                }

                add_attr(video, attr)
                print_none_attr(video, attr)

        if 'hdd' in args:
            hard_disk = HDD.objects.all()
            for hdd in hard_disk:
                attr = {
                    'volume': get_hdd_size(hdd),
                    'form_factor': get_hdd_form_factor(hdd),
                    'speed': get_hdd_speed(hdd),
                    'buffer_size': get_hdd_buffer_size(hdd),
                }

                add_attr(hdd, attr)
                print_none_attr(hdd, attr)

        if 'ssd' in args:
            solid_disk = SSD.objects.all()
            for ssd in solid_disk:
                read, write = get_ssd_io(ssd)
                attr = {
                    'volume': get_ssd_size(ssd),
                    'interface': get_ssd_interface(ssd),
                    'read_speed': read,
                    'write_speed': write,
                }

                add_attr(ssd, attr)
                print_none_attr(ssd, attr)

        if 'case' in args:
            cases = Case.objects.all()
            # all_sizes = list()
            for case in cases:
                # form_factor = get_case_form_factor(case)
                attr = {
                    'class_case': get_case_class(case),
                    'type_case': get_case_type(case),
                    'power': get_case_power(case),
                }

                add_attr(case, attr)
                print_none_attr(case, attr)

        if 'psu' in args:
            psus = PSU.objects.all()
            for psu in psus:
                attr = {
                    'power': get_psu_power(psu)
                }

                add_attr(psu, attr)
                print_none_attr(psu, attr)
