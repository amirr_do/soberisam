from django.core.management.base import BaseCommand
from gamer_shop.catalogue.models import Product, AttributeOption, ProductAttributeValue
import random


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for product in Product.objects.all():
            attrs = product.attr.get_all_attributes()
            a = dict()
            for attr in attrs:
                a[attr] = AttributeOption.objects.filter(group=attr.option_group.id)
                option = random.choice(a[attr])
                self.stdout.write('{} # {} # {}'.format(product, attr, option))
                new_el, _ = ProductAttributeValue.objects.get_or_create(product=product, attribute=attr,
                                                                        defaults={'value_option': option})
                self.stdout.write('{} & {}'.format(new_el, _))
