from oscar.apps.catalogue.admin import *  # noqa

CPU = get_model('catalogue', 'CPU')
Cooler = get_model('catalogue', 'Cooler')
Motherboard = get_model('catalogue', 'Motherboard')
RAM = get_model('catalogue', 'RAM')
VideoCard = get_model('catalogue', 'VideoCard')
HDD = get_model('catalogue', 'HDD')
SSD = get_model('catalogue', 'SSD')
ODD = get_model('catalogue', 'ODD')
Case = get_model('catalogue', 'Case')
PSU = get_model('catalogue', 'PSU')


class CPUAdmin(ProductAdmin):
    pass


class CoolerAdmin(ProductAdmin):
    pass


class MotherboardAdmin(ProductAdmin):
    pass


class RAMAdmin(ProductAdmin):
    pass


class VideoCardAdmin(ProductAdmin):
    pass


class HDDAdmin(ProductAdmin):
    pass


class SSDAdmin(ProductAdmin):
    pass


class ODDAdmin(ProductAdmin):
    pass


class CaseAdmin(ProductAdmin):
    pass


class PSUAdmin(ProductAdmin):
    pass

admin.site.register(CPU, CPUAdmin)
admin.site.register(Cooler, CoolerAdmin)
admin.site.register(Motherboard, MotherboardAdmin)
admin.site.register(RAM, RAMAdmin)
admin.site.register(VideoCard, VideoCardAdmin)
admin.site.register(HDD, HDDAdmin)
admin.site.register(SSD, SSDAdmin)
admin.site.register(ODD, ODDAdmin)
admin.site.register(Case, CaseAdmin)
admin.site.register(PSU, PSUAdmin)
