# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Case',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'Project-Id-Version: Django\nReport-Msgid-Bugs-To: \nPOT-Creation-Date: 2013-05-02 16:18+0200\nPO-Revision-Date: 2010-05-13 15:35+0200\nLast-Translator: Django team\nLanguage-Team: English <en@li.org>\nLanguage: en\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\n',
                'verbose_name_plural': 's',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='Cooler',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'Cooler',
                'verbose_name_plural': 'Coolers',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='CPU',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'Processor',
                'verbose_name_plural': 'Processors',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='HDD',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'HDD',
                'verbose_name_plural': 'HDDs',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='Motherboard',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'Motherboard',
                'verbose_name_plural': 'Motherboards',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='ODD',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'ODD',
                'verbose_name_plural': 'ODDs',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='PSU',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'Project-Id-Version: Django\nReport-Msgid-Bugs-To: \nPOT-Creation-Date: 2013-05-02 16:18+0200\nPO-Revision-Date: 2010-05-13 15:35+0200\nLast-Translator: Django team\nLanguage-Team: English <en@li.org>\nLanguage: en\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\n',
                'verbose_name_plural': 's',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='RAM',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'RAM',
                'verbose_name_plural': 'RAMs',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='SSD',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'SSD',
                'verbose_name_plural': 'SSDs',
            },
            bases=('catalogue.product',),
        ),
        migrations.CreateModel(
            name='VideoCard',
            fields=[
                ('product_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'Video card',
                'verbose_name_plural': 'Video cards',
            },
            bases=('catalogue.product',),
        ),
    ]
