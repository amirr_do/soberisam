# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import django.db.models.deletion
import oscar.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0004_product_short_description'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name_plural': 'Categories', 'verbose_name': 'Category', 'ordering': ['path']},
        ),
        migrations.RemoveField(
            model_name='category',
            name='full_name',
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(verbose_name='Slug', max_length=255),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_class',
            field=models.ForeignKey(help_text='Choose what type of product this is', blank=True, verbose_name='Product type', related_name='products', on_delete=django.db.models.deletion.PROTECT, null=True, to='catalogue.ProductClass'),
        ),
        migrations.AlterField(
            model_name='productattribute',
            name='code',
            field=models.SlugField(verbose_name='Code', max_length=128, validators=[django.core.validators.RegexValidator(message="Code can only contain the letters a-z, A-Z, digits, and underscores, and can't start with a digit", regex='^[a-zA-Z_][0-9a-zA-Z_]*$'), oscar.core.validators.non_python_keyword]),
        ),
    ]
