# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0002_case_cooler_cpu_hdd_motherboard_odd_psu_ram_ssd_videocard'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='case',
            options={'verbose_name': 'Case', 'verbose_name_plural': 'Cases'},
        ),
        migrations.AlterModelOptions(
            name='psu',
            options={'verbose_name': 'PSU', 'verbose_name_plural': 'PSUs'},
        ),
    ]
