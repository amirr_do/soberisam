from __future__ import unicode_literals
# -*- coding: utf-8 -*-
from django.core import management
from celery.task import task


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def update_index():
    management.call_command('update_index')
