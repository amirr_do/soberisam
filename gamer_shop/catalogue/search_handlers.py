from oscar.apps.catalogue.search_handlers import SolrProductSearchHandler as SProductSearchHandler

from oscar.core.loading import get_model
from oscar.core.decorators import deprecated

CPU = get_model('catalogue', 'CPU')
Cooler = get_model('catalogue', 'Cooler')
Motherboard = get_model('catalogue', 'Motherboard')
RAM = get_model('catalogue', 'RAM')
VideoCard = get_model('catalogue', 'VideoCard')
HDD = get_model('catalogue', 'HDD')
SSD = get_model('catalogue', 'SSD')
ODD = get_model('catalogue', 'ODD')
Case = get_model('catalogue', 'Case')
PSU = get_model('catalogue', 'PSU')
Product = get_model('catalogue', 'Product')


class SolrProductSearchHandler(SProductSearchHandler):
    model_whitelist = [CPU, Cooler, Motherboard, RAM, VideoCard, HDD, SSD, ODD, Case, PSU]
    computer_parts = ('case', 'cooler', 'cpu', 'dvd', 'hdd', 'motherboard', 'psu', 'ram', 'ssd', 'videocard')

    def __init__(self, request_data, full_path, categories, computer_build=None):
        self.computer_build = computer_build
        super(SolrProductSearchHandler, self).__init__(request_data, full_path, categories)

    def get_search_queryset(self):
        for category in self.categories:
            if category.slug not in self.computer_parts:
                self.model_whitelist = [CPU, Cooler, Motherboard, RAM, VideoCard, HDD, SSD, ODD, Case, PSU, Product]
                break
            self.model_whitelist = [CPU, Cooler, Motherboard, RAM, VideoCard, HDD, SSD, ODD, Case, PSU]
        sqs = super(SolrProductSearchHandler, self).get_search_queryset()
        good_partners = ('soberisam',)
        if good_partners:
            pattern = ' OR '.join(['"%s"' % partner for partner in good_partners])
            sqs = sqs.narrow('partner_exact:(%s)' % pattern)

        return sqs.order_by('-num_in_stock')


# Deprecated name. TODO: Remove in Oscar 1.2
ProductSearchHandler = deprecated(SolrProductSearchHandler)


class BuildProductSearchHandler(SProductSearchHandler):
    model_whitelist = [CPU, Cooler, Motherboard, RAM, VideoCard, HDD, SSD, ODD, Case, PSU]

    def __init__(self, request_data, full_path, categories, computer_build=None):
        self.computer_build = computer_build
        super(BuildProductSearchHandler, self).__init__(request_data, full_path, categories)

    def get_search_queryset(self):
        sqs = super(BuildProductSearchHandler, self).get_search_queryset()
        good_partners = ('soberisam',)
        if good_partners:
            pattern = ' OR '.join(['"%s"' % partner for partner in good_partners])
            sqs = sqs.narrow('partner_exact:(%s)' % pattern)
        socket = self.computer_build.attr.socket_computer
        try:
            if self.computer_build.attr.socket_computer:
                for category in self.categories:
                    if category.slug in ('cpu', 'motherboard'):
                        if category.slug == 'cpu' and socket == 'FM2+':
                            sqs = sqs.narrow('socket_exact:{} OR socket_exact:FM2'.format(socket))
                        else:
                            sqs = sqs.narrow('socket_exact:{}'.format(socket))
                    if category.slug in ('ram',):
                        if socket in ('LGA1151', 'LGA2011'):
                            sqs = sqs.narrow('type_ram_exact:DDR4')
                        elif socket in ('LGA1150', 'LGA1155', 'AM3+', 'FM2+'):
                            sqs = sqs.narrow('type_ram_exact:DDR3')
        except AttributeError:
            pass
        # sqs = sqs.exclude(pk='1518')

        computer = self.computer_build
        category_pk = self.categories[0].pk

        current_product = None
        if category_pk == 2:
            current_product = computer.cpu
        elif category_pk == 3:
            current_product = computer.cooler
        elif category_pk == 4:
            current_product = computer.motherboard
        elif category_pk == 5 and computer.computerram_set.filter(computer=computer).exists():
            current_product = computer.computerram_set.get(computer=computer).ram
        elif category_pk == 6:
            if computer.computervideocard_set.filter(computer=computer).exists():
                current_product = computer.computervideocard_set.get(computer=computer).video_card
        elif category_pk == 7:
            if computer.computerhdd_set.filter(computer=computer).exists():
                current_product = computer.computerhdd_set.get(computer=computer).hdd
        elif category_pk == 8:
            if computer.computerssd_set.filter(computer=computer).exists():
                current_product = computer.computerssd_set.get(computer=computer).ssd
        elif category_pk == 9 and computer.computerodd_set.filter(computer=computer).exists():
            current_product = computer.computerodd_set.get(computer=computer).odd
        elif category_pk == 10:
            current_product = computer.case
        elif category_pk == 11:
            if computer.computerpsu_set.filter(computer=computer).exists():
                current_product = computer.computerpsu_set.get(computer=computer).psu

        if current_product:
            title = current_product.title
            sqs = sqs.exclude(title=title)

        return sqs.order_by('-num_in_stock')
