from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from oscar.apps.catalogue.views import ProductDetailView, CatalogueView
from oscar.apps.catalogue.views import ProductCategoryView as BaseProductCategoryView
from gamer_shop.catalogue.tasks import update_index

__all__ = ['ProductDetailView', 'CatalogueView', 'ProductCategoryView', 'update_index_view']


class ProductCategoryView(BaseProductCategoryView):
    template_folder = 'catalogue/category'

    def get_template_names(self):
        template = self.template_name
        category_template = '{folder}/{slug}/category.html'.format(folder=self.template_folder, slug=self.category.slug)
        return [category_template, template]


@csrf_protect
def update_index_view(request):
    if request.method == 'POST':
        update_index.delay()
    return HttpResponseRedirect('/dashboard/')
