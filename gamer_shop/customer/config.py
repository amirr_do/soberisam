from oscar.apps.customer import config


class CustomerConfig(config.CustomerConfig):
    name = 'gamer_shop.customer'
