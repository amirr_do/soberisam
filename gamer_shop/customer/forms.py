import re

from django.utils.translation import ugettext_lazy as _
from oscar.apps.customer.forms import *
ComputerAlert = get_model('customer', 'ComputerAlert')


class ComputerAlertForm(forms.ModelForm):
    email = forms.EmailField(required=True, label=_(u'Send notification to'),
                             widget=forms.TextInput(attrs={
                                 'placeholder': _('Enter your email')
                             }))

    def __init__(self, user, computer, *args, **kwargs):
        self.user = user
        self.computer = computer
        super(ComputerAlertForm, self).__init__(*args, **kwargs)

        # Only show email field to unauthenticated users
        if user and user.is_authenticated():
            self.fields['email'].widget = forms.HiddenInput()
            self.fields['email'].required = False

    def save(self, commit=True):
        alert = super(ComputerAlertForm, self).save(commit=False)
        if self.user.is_authenticated():
            alert.user = self.user
        alert.computer = self.computer
        if commit:
            alert.save()
        return alert

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get('email')
        if email:
            try:
                ComputerAlert.objects.get(
                    computer=self.computer, email__iexact=email,
                    status=ComputerAlert.ACTIVE)
            except ComputerAlert.DoesNotExist:
                pass
            else:
                raise forms.ValidationError(_(
                    "There is already an active stock alert for %s") % email)
        elif self.user.is_authenticated():
            try:
                ComputerAlert.objects.get(computer=self.computer,
                                          user=self.user,
                                          status=ComputerAlert.ACTIVE)
            except ComputerAlert.DoesNotExist:
                pass
            else:
                raise forms.ValidationError(_(
                    "You already have an active alert for this product"))
        return cleaned_data

    class Meta:
        model = ComputerAlert
        exclude = ('user', 'key',
                   'status', 'date_confirmed', 'date_cancelled', 'date_closed',
                   'computer')


class CallbackRequestForm(forms.ModelForm):
    class Meta:
        model = get_model('customer', 'CallbackRequest')
        fields = ('phone_number', 'notes')

    def clean_phone_number(self):
        phone_raw = self.cleaned_data['phone_number']

        pattern = '(\+?3?8)?[\- ]?(\(?0\d{2,4}\)?[\- ]?[\d\- ]{5,11})'
        found = re.findall(pattern, phone_raw)
        if found:
            phone = found[0][1].replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
            if phone and len(phone) > 9:
                phone = "+38({}){}-{}-{}{}".format(phone[:3], phone[3:6], phone[6:8], phone[8:10], phone[10:])
            else:
                raise forms.ValidationError("%s is not an phone number" % phone_raw)

            return phone
        raise forms.ValidationError("%s is not an phone number" % phone_raw)
