import re

from django.core.exceptions import ValidationError
from django.db import models
from oscar.core.compat import AUTH_USER_MODEL
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.urlresolvers import reverse
import hashlib
import random

from oscar.models.fields import PhoneNumberField


class ComputerAlert(models.Model):
    """
    An alert for when a product comes back in stock
    """
    computer = models.ForeignKey('computer.Computer')

    # A user is only required if the notification is created by a
    # registered user, anonymous users will only have an email address
    # attached to the notification
    user = models.ForeignKey(AUTH_USER_MODEL, db_index=True, blank=True,
                             null=True, related_name="computer_alerts",
                             verbose_name=_('User'))
    email = models.EmailField(_("Email"), db_index=True, blank=True)

    # This key are used to confirm and cancel alerts for anon users
    key = models.CharField(_("Key"), max_length=128, blank=True, db_index=True)

    # An alert can have two different statuses for authenticated
    # users ``ACTIVE`` and ``INACTIVE`` and anonymous users have an
    # additional status ``UNCONFIRMED``. For anonymous users a confirmation
    # and unsubscription key are generated when an instance is saved for
    # the first time and can be used to confirm and unsubscribe the
    # notifications.
    UNCONFIRMED, ACTIVE, CANCELLED, CLOSED = (
        'Unconfirmed', 'Active', 'Cancelled', 'Closed')
    STATUS_CHOICES = (
        (UNCONFIRMED, _('Not yet confirmed')),
        (ACTIVE, _('Active')),
        (CANCELLED, _('Cancelled')),
        (CLOSED, _('Closed')),
    )
    status = models.CharField(_("Status"), max_length=20,
                              choices=STATUS_CHOICES, default=ACTIVE)

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)
    date_confirmed = models.DateTimeField(_("Date confirmed"), blank=True,
                                          null=True)
    date_cancelled = models.DateTimeField(_("Date cancelled"), blank=True,
                                          null=True)
    date_closed = models.DateTimeField(_("Date closed"), blank=True, null=True)

    class Meta:
        app_label = 'customer'
        verbose_name = _('Computer alert')
        verbose_name_plural = _('Computer alerts')

    @property
    def is_anonymous(self):
        return self.user is None

    @property
    def can_be_confirmed(self):
        return self.status == self.UNCONFIRMED

    @property
    def can_be_cancelled(self):
        return self.status == self.ACTIVE

    @property
    def is_cancelled(self):
        return self.status == self.CANCELLED

    @property
    def is_active(self):
        return self.status == self.ACTIVE

    def confirm(self):
        self.status = self.ACTIVE
        self.date_confirmed = timezone.now()
        self.save()
    confirm.alters_data = True

    def cancel(self):
        self.status = self.CANCELLED
        self.date_cancelled = timezone.now()
        self.save()
    cancel.alters_data = True

    def close(self):
        self.status = self.CLOSED
        self.date_closed = timezone.now()
        self.save()
    close.alters_data = True

    def get_email_address(self):
        if self.user:
            return self.user.email
        else:
            return self.email

    def save(self, *args, **kwargs):
        if not self.id and not self.user:
            self.key = self.get_random_key()
            self.status = self.UNCONFIRMED
        # Ensure date fields get updated when saving from modelform (which just
        # calls save, and doesn't call the methods cancel(), confirm() etc).
        if self.status == self.CANCELLED and self.date_cancelled is None:
            self.date_cancelled = timezone.now()
        if not self.user and self.status == self.ACTIVE \
                and self.date_confirmed is None:
            self.date_confirmed = timezone.now()
        if self.status == self.CLOSED and self.date_closed is None:
            self.date_closed = timezone.now()

        return super(ComputerAlert, self).save(*args, **kwargs)

    def get_random_key(self):
        """
        Get a random generated key based on SHA-1 and email address
        """
        salt = hashlib.sha1(str(random.random()).encode('utf8')).hexdigest()
        return hashlib.sha1((salt + self.email).encode('utf8')).hexdigest()

    def get_confirm_url(self):
        return reverse('customer:alerts-confirm', kwargs={'key': self.key})

    def get_cancel_url(self):
        return reverse('customer:alerts-cancel-by-key', kwargs={'key': self.key})


def validate_phone(value):

    pattern = '(\+?3?8)?[\- ]?(\(?0\d{2,4}\)?[\- ]?[\d\- ]{5,11})'

    found = re.findall(pattern, value.raw_input)
    if not found or not len(found[0][1]) > 9:
        raise ValidationError('%s is not an phone number' % value)


class PhoneNumberField(PhoneNumberField):
    default_validators = [validate_phone]


class CallbackRequest(models.Model):
    phone_number = PhoneNumberField(
        _("Phone number"), blank=False, null=False,
        help_text=_("In case we need to call you about your query"))

    notes = models.TextField(
        blank=True, verbose_name=_('Instructions'),
        help_text=_("Tell us anything we should know when processing "
                    "your query."))

    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='callback_request', verbose_name=_("User"), null=True, blank=True)


from oscar.apps.customer.models import *  # noqa
