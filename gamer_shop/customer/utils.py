from oscar.apps.customer.utils import *


class Dispatcher(Dispatcher):

    def send_user_email_messages(self, user, messages):

        super().send_user_email_messages(user, messages)

        if hasattr(settings, 'OSCAR_FROM_EMAIL'):
            self.send_email_messages(settings.OSCAR_FROM_EMAIL, messages)
        else:
            self.logger.warning("Unable to send email messages, OSCAR_FROM_EMAIL == None")
            return
