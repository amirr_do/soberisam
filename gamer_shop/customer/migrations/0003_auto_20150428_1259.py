# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0002_computeralert'),
    ]

    operations = [
        migrations.AlterField(
            model_name='computeralert',
            name='status',
            field=models.CharField(default='Active', verbose_name='Status', choices=[('Unconfirmed', 'Not yet confirmed'), ('Active', 'Active'), ('Cancelled', 'Cancelled'), ('Closed', 'Closed')], max_length=20),
            preserve_default=True,
        ),
    ]
