# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import oscar.models.fields.autoslugfield


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0003_auto_20150428_1259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='communicationeventtype',
            name='code',
            field=oscar.models.fields.autoslugfield.AutoSlugField(max_length=128, blank=True, populate_from='name', separator='_', validators=[django.core.validators.RegexValidator(message="Code can only contain the letters a-z, A-Z, digits, and underscores, and can't start with a digit.", regex='^[a-zA-Z_][0-9a-zA-Z_]*$')], verbose_name='Code', unique=True, help_text='Code used for looking up this event programmatically', editable=False),
        ),
        migrations.AlterField(
            model_name='computeralert',
            name='email',
            field=models.EmailField(db_index=True, max_length=254, blank=True, verbose_name='Email'),
        ),
    ]
