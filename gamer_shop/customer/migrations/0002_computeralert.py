# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('computer', '0001_initial'),
        ('customer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComputerAlert',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(db_index=True, max_length=75, verbose_name='Email', blank=True)),
                ('key', models.CharField(db_index=True, max_length=128, verbose_name='Key', blank=True)),
                ('status', models.CharField(default=b'Active', max_length=20, verbose_name='Status', choices=[(b'Unconfirmed', 'Not yet confirmed'), (b'Active', 'Active'), (b'Cancelled', 'Cancelled'), (b'Closed', 'Closed')])),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('date_confirmed', models.DateTimeField(null=True, verbose_name='Date confirmed', blank=True)),
                ('date_cancelled', models.DateTimeField(null=True, verbose_name='Date cancelled', blank=True)),
                ('date_closed', models.DateTimeField(null=True, verbose_name='Date closed', blank=True)),
                ('computer', models.ForeignKey(to='computer.Computer')),
                ('user', models.ForeignKey(related_name='computer_alerts', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Computer alert',
                'verbose_name_plural': 'Computer alerts',
            },
            bases=(models.Model,),
        ),
    ]
