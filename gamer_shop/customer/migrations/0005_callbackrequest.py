# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import gamer_shop.customer.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customer', '0004_auto_20150701_1833'),
    ]

    operations = [
        migrations.CreateModel(
            name='CallbackRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('phone_number', gamer_shop.customer.models.PhoneNumberField(verbose_name='Phone number', help_text='In case we need to call you about your query')),
                ('notes', models.TextField(help_text='Tell us anything we should know when processing your query.', verbose_name='Instructions', blank=True)),
                ('user', models.ForeignKey(null=True, related_name='callback_request', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
