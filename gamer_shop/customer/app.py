from django.conf.urls import url
from oscar.apps.customer.app import application
from oscar.core.application import Application
from oscar.core.loading import get_class

__all__ = ['application', 'callback_app']


class CallbackApplication(Application):
    name = 'callback'
    callback_view = get_class('customer.views', 'CallbackRequestView')
    callback_thanks_view = get_class('customer.views', 'CallbackRequestThanksView')

    def get_urls(self):
        urls = [
            url(r'^$', self.callback_view.as_view(), name='request'),
            url(r'^thanks/$', self.callback_thanks_view.as_view(), name='thanks'),
        ]
        return self.post_process_urls(urls)

callback_app = CallbackApplication()
