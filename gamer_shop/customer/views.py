from django.http import HttpResponseRedirect
from django.views.generic import FormView, TemplateView
from oscar.apps.customer.views import *

from gamer_shop.customer.forms import CallbackRequestForm


class CallbackRequestView(FormView):
    communication_type_code = 'SEND_CALLBACK_REQUEST'
    form_class = CallbackRequestForm
    template_name = 'customer/callback_detail.html'
    success_url = reverse_lazy('callback:thanks')

    def form_invalid(self, form):
        url = reverse_lazy('promotions:home')
        return HttpResponseRedirect(url)

    def form_valid(self, form):
        user = self.request.user
        ctx = {
            'phone': form.cleaned_data.get('phone_number', ''),
            'notes': form.cleaned_data.get('notes', ''),
        }
        if user and user.is_authenticated():
            ctx.update({'user': user})
            form.instance.user = user
        else:
            user = None

        form.save()

        self.create_email(ctx, user)

        return HttpResponseRedirect(self.get_success_url())

    def create_email(self, ctx, user=None):
        code = self.communication_type_code
        ctx.update({'site': get_current_site(self.request)})

        messages = CommunicationEventType.objects.get_and_render(code, ctx)

        if user:
            Email._default_manager.create(user=user, subject=messages['subject'], body_text=messages['body'],
                                          body_html=messages['html'])
            self.send_email(messages, user.email)

        for manager_email in settings.MANAGERS:
            self.send_email(messages, manager_email[1])

    def send_email(self, messages, email):
        if messages['subject'] and (messages['body'] or messages['html']) and email:
            Dispatcher().send_email_messages(email, messages)


class CallbackRequestThanksView(TemplateView):
    template_name = 'customer/callback_success.html'
