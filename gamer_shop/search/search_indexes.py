from __future__ import unicode_literals
# -*- coding: utf-8 -*-
from haystack import indexes
from django.core.exceptions import ObjectDoesNotExist
from oscar.core.loading import get_model
from gamer_shop.catalogue.models import ProductAttributeValue, ProductAttribute, ProductClass

from .abstract_indexes import ProductIndexes, ComputerIndexes  # noqa


class CPUIndexes(ProductIndexes):
    socket = indexes.CharField(null=True, faceted=True)
    core = indexes.IntegerField(null=True, faceted=True)
    freq = indexes.DecimalField(null=True, faceted=True)
    nm = indexes.IntegerField(null=True, faceted=True)
    tray = indexes.BooleanField(null=True, faceted=True)
    power = indexes.IntegerField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'CPU')

    def prepare_socket(self, obj):
        try:
            product_class = ProductClass.objects.get(slug='cpu_type')
            return ProductAttributeValue.objects.get(product=obj,
                                                     attribute=ProductAttribute.objects.get(
                                                             code='socket', product_class=product_class)).value_text
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist socket ', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_core(self, obj):
        try:
            core = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='core')).value_integer
            return int(core)
        except TypeError:
            # print('TypeError core ', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist core', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_freq(self, obj):
        try:
            freq = ProductAttributeValue.objects.get(product=obj,
                                                     attribute=ProductAttribute.objects.get(code='freq')).value_float
            return float(freq)
        except TypeError:
            # print('TypeError freq ', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print(ObjectDoesNotExist freq obj.pk)
            return None
        except ValueError:
            return None

    def prepare_nm(self, obj):
        try:
            nm = ProductAttributeValue.objects.get(product=obj,
                                                   attribute=ProductAttribute.objects.get(code='nm')).value_text
            nm = nm.replace('nm', '')
            return int(nm)
        except TypeError:
            # print('TypeError nm ', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print(ObjectDoesNotExist nm obj.pk)
            return None
        except ValueError:
            return None

    def prepare_tray(self, obj):
        try:
            return ProductAttributeValue.objects.get(product=obj,
                                                     attribute=ProductAttribute.objects.get(
                                                             code='traybox')).value_boolean
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist tray ', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_power(self, obj):
        try:
            power = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='power',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='cpu_type'))).value_integer
            return int(power)
        except TypeError:
            # print('TypeError power ', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist power ', obj.pk)
            return None
        except ValueError:
            return None


class CoolerIndexes(ProductIndexes):
    circulation = indexes.CharField(null=True, faceted=True)
    connect = indexes.CharField(null=True, faceted=True)
    dB = indexes.CharField(null=True, faceted=True)
    material = indexes.CharField(null=True, faceted=True)
    type_cooler = indexes.CharField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'Cooler')

    def prepare_circulation(self, obj):
        try:
            circulation = ProductAttributeValue. \
                objects.get(product=obj, attribute=ProductAttribute.objects.get(code='circulation')).value_text
            return '{} об/мин'.format(circulation) if circulation != 'нет' else None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist circulation ', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_dB(self, obj):
        try:
            dB = ProductAttributeValue. \
                objects.get(product=obj, attribute=ProductAttribute.objects.get(code='dB')).value_text
            return '{} дБ'.format(dB) if dB != 'нет' else None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist dB ', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_connect(self, obj):
        try:
            connect = ProductAttributeValue. \
                objects.get(product=obj, attribute=ProductAttribute.objects.get(code='connect')).value_text
            return '{}-pin'.format(connect) if connect != 'нет' else None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist connect ', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_material(self, obj):
        try:
            material = ProductAttributeValue. \
                objects.get(product=obj, attribute=ProductAttribute.objects.get(code='material')).value_text
            return material
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist material ', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_type_cooler(self, obj):
        try:
            type_cooler = ProductAttributeValue. \
                objects.get(product=obj, attribute=ProductAttribute.objects.get(code='type_cooler')).value_text
            return type_cooler
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist type ', obj.pk)
            return None
        except ValueError:
            return None


class MotherboardIndexes(ProductIndexes):
    chipset = indexes.CharField(null=True, faceted=True)
    form_factor_mb = indexes.CharField(null=True, faceted=True)
    size_mb = indexes.CharField(null=True, faceted=True)
    socket = indexes.CharField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'Motherboard')

    def prepare_chipset(self, obj):
        try:
            chipset = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='chipset')).value_text
            return chipset
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist chipset ', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_form_factor_mb(self, obj):
        try:
            form_factor = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='form_factor',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='motherboard_type'))).value_text
            return form_factor
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist form_factor', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_size_mb(self, obj):
        try:
            size = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='size')).value_text
            return size
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist size', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_socket(self, obj):
        try:
            socket = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='socket',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='motherboard_type'))).value_text

            return 'LGA{}'.format(socket) if socket in ('1151', '1150', '1155', '2011') else socket
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist socket', obj.pk)
            return None
        except ValueError:
            return None


class RamIndexes(ProductIndexes):
    volume_ram = indexes.IntegerField(null=True, faceted=True)
    type_ram = indexes.CharField(null=True, faceted=True)
    radiator = indexes.CharField(null=True, faceted=True)
    qty = indexes.IntegerField(null=True, faceted=True)
    latency = indexes.IntegerField(null=True, faceted=True)
    frequency = indexes.IntegerField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'RAM')

    def prepare_volume_ram(self, obj):
        try:
            volume = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='volume',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ram_type'))).value_integer
            return int(volume)
        except TypeError:
            # print('TypeError volume', obj.pk)
            return None
        except ValueError:
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist volume', obj.pk)
            return None

    def prepare_type_ram(self, obj):
        try:
            type_ram = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='type_ram',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ram_type'))).value_text
            return type_ram
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist type_ram', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_radiator(self, obj):
        try:
            radiator = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='radiator',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ram_type'))).value_text
            return radiator
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist radiator', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_qty(self, obj):
        try:
            qty = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='qty',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ram_type'))).value_integer
            return int(qty)
        except TypeError:
            # print('TypeError qty', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist qty', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_latency(self, obj):
        try:
            latency = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='latency',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ram_type'))).value_text
            latency = latency.replace('CL', '')
            return int(latency)
        except TypeError:
            # print('TypeError latency', obj.pk)
            return None
        except ValueError:
            # print('ValueError latency', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist latency', obj.pk)
            return None

    def prepare_frequency(self, obj):
        try:
            frequency = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='frequency',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ram_type'))).value_integer
            return int(frequency)
        except TypeError:
            # print('TypeError frequency', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist frequency', obj.pk)
            return None
        except ValueError:
            return None


class VideoIndexes(ProductIndexes):
    volume_video = indexes.IntegerField(null=True, faceted=True)
    type_video = indexes.CharField(null=True, faceted=True)
    gpu = indexes.CharField(null=True, faceted=True)
    cooling = indexes.CharField(null=True, faceted=True)
    bit = indexes.IntegerField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'VideoCard')

    def prepare_volume_video(self, obj):
        try:
            volume_memory = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='volume_memory',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='videocard_type'))).value_integer
            return int(volume_memory)
        except TypeError:
            # print('TypeError volume memory', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist volume_memory', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_type_video(self, obj):
        try:
            type_video = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='type_video',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='videocard_type'))).value_text
            return type_video
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist type_video', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_gpu(self, obj):
        try:
            gpu = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='gpu',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='videocard_type'))).value_text
            return gpu
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist gpu', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_cooling(self, obj):
        try:
            cooling = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='cooling',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='videocard_type'))).value_text
            return cooling
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist cooling', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_bit(self, obj):
        try:
            bit = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='bit',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='videocard_type'))).value_integer
            return int(bit)
        except TypeError:
            # print('TypeError bit', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist bit', obj.pk)
            return None
        except ValueError:
            return None


class HddIndexes(ProductIndexes):
    volume_hdd = indexes.IntegerField(null=True, faceted=True)
    speed = indexes.CharField(null=True, faceted=True)
    form_factor_hdd = indexes.CharField(null=True, faceted=True)
    buffer_size = indexes.IntegerField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'HDD')

    def prepare_volume_hdd(self, obj):
        try:
            volume = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='volume',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='hdd_type'))).value_integer
            return int(volume)
        except TypeError:
            # print('TypeError volume', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist volume', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_speed(self, obj):
        try:
            speed = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='speed',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='hdd_type'))).value_text
            return '{} об/мин'.format(speed) if speed[-1] == 0 else speed
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist speed', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_form_factor_hdd(self, obj):
        try:
            form_factor = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='form_factor',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='hdd_type'))).value_text
            return form_factor
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist form_factor', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_buffer_size(self, obj):
        try:
            buffer_size = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='buffer_size',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='hdd_type'))).value_integer
            return int(buffer_size)
        except TypeError:
            # print('TypeError buffer_size', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist buffer_size', obj.pk)
            return None
        except ValueError:
            return None


class SsdIndexes(ProductIndexes):
    volume_ssd = indexes.IntegerField(null=True, faceted=True)
    interface = indexes.CharField(null=True, faceted=True)
    read_speed = indexes.IntegerField(null=True, faceted=True)
    write_speed = indexes.IntegerField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'SSD')

    def prepare_volume_ssd(self, obj):
        try:
            volume = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='volume',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ssd_type'))).value_integer
            return int(volume)
        except TypeError:
            # print('TypeError volume', obj.pk)
            return None
        except ValueError:
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist volume', obj.pk)
            return None

    def prepare_interface(self, obj):
        try:
            interface = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='interface',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ssd_type'))).value_text
            return interface
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist interface', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_read_speed(self, obj):
        try:
            read_speed = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='read_speed',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ssd_type'))).value_integer
            return int(read_speed)
        except TypeError:
            # print('TypeError read speed', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist read_speed', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_write_speed(self, obj):
        try:
            write_speed = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='write_speed',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='ssd_type'))).value_integer
            return int(write_speed)
        except TypeError:
            # print('TypeError write speed', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist write_speed', obj.pk)
            return None
        except ValueError:
            return None


class CaseIndexes(ProductIndexes):
    type_case = indexes.CharField(null=True, faceted=True)
    class_case = indexes.CharField(null=True, faceted=True)
    power = indexes.IntegerField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'Case')

    def prepare_type_case(self, obj):
        try:
            type_case = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='type_case',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='case_type'))).value_text
            return type_case
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist type_case', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_class_case(self, obj):
        try:
            class_case = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='class_case',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='case_type'))).value_text
            return class_case
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist class_case', obj.pk)
            return None
        except ValueError:
            return None

    def prepare_power(self, obj):
        try:
            power = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='power',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='case_type'))).value_integer
            return int(power)
        except TypeError:
            # print('TypeError power', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist power', obj.pk)
            return None
        except ValueError:
            return None


class PsuIndexes(ProductIndexes):
    power = indexes.IntegerField(null=True, faceted=True)

    def get_model(self):
        return get_model('catalogue', 'PSU')

    def prepare_power(self, obj):
        try:
            power = ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='power',
                                                                        product_class=ProductClass.objects.get(
                                                                                slug='psu_type'))).value_integer
            return int(power)
        except TypeError:
            # print('TypeError power', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist power', obj.pk)
            return None
        except ValueError:
            return None


class OddIndexes(ProductIndexes):
    def get_model(self):
        return get_model('catalogue', 'ODD')
