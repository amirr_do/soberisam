from oscar.apps.search import config


class SearchConfig(config.SearchConfig):
    name = 'gamer_shop.search'
