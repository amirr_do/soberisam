from __future__ import unicode_literals
# -*- coding: utf-8 -*-
from haystack import indexes
from celery_haystack.indexes import CelerySearchIndex
from django.core.exceptions import ObjectDoesNotExist
from gamer_shop.catalogue.models import ProductAttributeValue, ProductAttribute, ProductClass
from gamer_shop.computer.models import ComputerAttributeValue

from oscar.core.loading import get_model, get_class

# Load default strategy (without a user/request)
is_solr_supported = get_class('search.features', 'is_solr_supported')
Selector = get_class('partner.strategy', 'Selector')
StockRecord = get_model('partner', 'StockRecord')
strategy = Selector().strategy()


class ProductIndexes(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(
            document=True, use_template=True,
            template_name='search/indexes/cpu/item_text.txt')

    upc = indexes.CharField(model_attr="upc", null=True)
    title = indexes.EdgeNgramField(model_attr='title', null=True)

    # Fields for faceting
    product_class = indexes.CharField(null=True, faceted=True)
    category = indexes.MultiValueField(null=True, faceted=True)
    partner = indexes.MultiValueField(null=True, faceted=True)
    price = indexes.FloatField(null=True, faceted=True)
    vendor = indexes.CharField(null=True, faceted=True)
    rating = indexes.IntegerField(null=True, faceted=True)
    num_in_stock = indexes.BooleanField(null=True, faceted=True)

    # Spelling suggestions
    suggestions = indexes.FacetCharField()

    date_created = indexes.DateTimeField(model_attr='date_created')
    date_updated = indexes.DateTimeField(model_attr='date_updated')

    def get_model(self):
        return get_model('catalogue', 'Product')

    def index_queryset(self, using=None):
        # Only index browsable products (not each individual child product)
        return self.get_model().browsable.order_by('-date_updated')

    def read_queryset(self, using=None):
        return self.get_model().browsable.base_queryset()

    def prepare_product_class(self, obj):
        return obj.get_product_class().name

    def prepare_category(self, obj):
        categories = obj.categories.all()
        if len(categories) > 0:
            return [category.full_name for category in categories]

    def prepare_partner(self, obj):
        stockrecords = StockRecord.objects.filter(product=obj)
        if len(stockrecords) > 0:
            return [stockrecord.partner.code for stockrecord in stockrecords]

    def prepare_rating(self, obj):
        if obj.rating is not None:
            return int(obj.rating)

    def prepare_price(self, obj):
        result = None
        if obj.is_parent:
            result = strategy.fetch_for_parent(obj)
        elif obj.has_stockrecords:
            result = strategy.fetch_for_product(obj)

        if result:
            if result.price.is_tax_known:
                return result.price.incl_tax
            return result.price.excl_tax

    def prepare_num_in_stock(self, obj):
        if obj.is_parent:
            # Don't return a stock level for parent products
            return False
        elif obj.has_stockrecords:
            result = strategy.fetch_for_product(obj)
            # print(False if result.stockrecord.net_stock_level == 0 else True)
            try:
                if result and hasattr(result, 'stockrecord') and result.stockrecord:
                    is_stock = False if result.stockrecord.net_stock_level == 0 else True
                    return is_stock
                else:
                    return False
            except AttributeError:
                return False

    def prepare_vendor(self, obj):
        try:
            return ProductAttributeValue.objects.get(
                    product=obj, attribute=ProductAttribute.objects.get(code='vendor')).value_text
        except ObjectDoesNotExist:
            print('vendor', obj.pk)
            return None

    def prepare(self, obj):
        prepared_data = super(ProductIndexes, self).prepare(obj)

        # We use Haystack's dynamic fields to ensure that the title field used
        # for sorting is of type "string'.
        if is_solr_supported():
            prepared_data['title_s'] = prepared_data['title']

        # Use title to for spelling suggestions
        prepared_data['suggestions'] = prepared_data['text']

        return prepared_data

    def get_updated_field(self):
        """
        Used to specify the field used to determine if an object has been
        updated

        Can be used to filter the query set when updating the index
        """
        return 'date_updated'


class ComputerIndexes(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(
            document=True, use_template=True,
            template_name='search/indexes/cpu/item_text.txt')

    upc = indexes.CharField(model_attr="upc", null=True)
    title = indexes.EdgeNgramField(model_attr='title', null=True)

    # Fields for faceting
    product_class = indexes.CharField(null=True, faceted=True)
    category = indexes.MultiValueField(null=True, faceted=True)
    price = indexes.FloatField(null=True, faceted=True)
    rating = indexes.IntegerField(null=True, faceted=True)

    # Spelling suggestions
    suggestions = indexes.FacetCharField()

    owner = indexes.IntegerField(null=True)
    status = indexes.CharField(null=True)

    socket = indexes.CharField(null=True, faceted=True)
    core = indexes.IntegerField(null=True, faceted=True)
    freq = indexes.DecimalField(null=True, faceted=True)

    volume_ram = indexes.IntegerField(null=True, faceted=True)

    has_video_card = indexes.BooleanField(null=True, faceted=True)

    volume_hdd = indexes.IntegerField(null=True, faceted=True)
    volume_ssd = indexes.IntegerField(null=True, faceted=True)

    has_odd = indexes.BooleanField(null=True, faceted=True)

    type_case = indexes.CharField(null=True, faceted=True)
    class_case = indexes.CharField(null=True, faceted=True)
    power = indexes.IntegerField(null=True, faceted=True)

    date_created = indexes.DateTimeField(model_attr='date_created')
    date_updated = indexes.DateTimeField(model_attr='date_updated')

    def get_model(self):
        return get_model('computer', 'Computer')

    def index_queryset(self, using=None):
        # Only index browsable products (not each individual child product)
        return self.get_model().browsable.order_by('-date_updated')

    def read_queryset(self, using=None):
        return self.get_model().browsable.base_queryset()

    def prepare_owner(self, obj):
        if obj.owner:
            return int(obj.owner.pk)

    def prepare_status(self, obj):
        if obj.status:
            return obj.status

    def prepare_product_class(self, obj):
        return obj.get_product_class().name

    def prepare_category(self, obj):
        categories = obj.categories.all()
        if len(categories) > 0:
            return [category.full_name for category in categories]

    def prepare_rating(self, obj):
        if obj.rating is not None:
            return int(obj.rating)

    def prepare_price(self, obj):
        result = strategy.fetch_for_computer(obj)

        if result:
            if result.price.is_tax_known:
                return result.price.incl_tax
            return result.price.excl_tax

    def prepare_socket(self, obj):
        try:
            return ComputerAttributeValue.objects.get(computer=obj,
                                                      attribute=ProductAttribute.objects.get(
                                                              code='socket_computer')).value_text
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist socket ', obj.pk)
            return None

    def prepare_core(self, obj):
        try:
            core = ProductAttributeValue.objects.get(
                    product=obj.cpu, attribute=ProductAttribute.objects.get(code='core')).value_integer
            return int(core)
        except TypeError:
            # print('TypeError core ', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist core', obj.pk)
            return None

    def prepare_freq(self, obj):
        try:
            freq = ProductAttributeValue.objects.get(product=obj.cpu,
                                                     attribute=ProductAttribute.objects.get(code='freq')).value_float
            return float(freq)
        except TypeError:
            # print('TypeError freq ', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print(ObjectDoesNotExist freq obj.pk)
            return None

    def prepare_volume_ram(self, obj):
        try:
            sum_volume = 0
            for computer_ram in obj.rams:
                volume = ProductAttributeValue.objects.get(
                        product=computer_ram.ram,
                        attribute=ProductAttribute.objects.get(code='volume',
                                                               product_class=ProductClass.objects.get(
                                                                       slug='ram_type'))).value_integer
                sum_volume += int(volume)
            return sum_volume
        except TypeError:
            # print('TypeError volume', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist volume', obj.pk)
            return None

    def prepare_has_video_card(self, obj):
        return obj.video_cards.exists()

    def prepare_volume_hdd(self, obj):
        try:
            sum_volume = 0
            for computer_hdd in obj.hdds:
                volume = ProductAttributeValue.objects.get(
                        product=computer_hdd.hdd,
                        attribute=ProductAttribute.objects.get(code='volume',
                                                               product_class=ProductClass.objects.get(
                                                                       slug='hdd_type'))).value_integer
                sum_volume += int(volume)
            return sum_volume
        except TypeError:
            # print('TypeError volume', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist volume', obj.pk)
            return None

    def prepare_volume_ssd(self, obj):
        try:
            sum_volume = 0
            for computer_ssd in obj.ssds:
                volume = ProductAttributeValue.objects.get(
                        product=computer_ssd.ssd,
                        attribute=ProductAttribute.objects.get(code='volume',
                                                               product_class=ProductClass.objects.get(
                                                                       slug='ssd_type'))).value_integer
                sum_volume += int(volume)
            return sum_volume
        except TypeError:
            # print('TypeError volume', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist volume', obj.pk)
            return None

    def prepare_type_case(self, obj):
        try:
            type_case = ProductAttributeValue.objects.get(
                    product=obj.case, attribute=ProductAttribute.objects.get(code='type_case',
                                                                             product_class=ProductClass.objects.get(
                                                                                     slug='case_type'))).value_text
            return type_case
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist type_case', obj.pk)
            return None

    def prepare_class_case(self, obj):
        try:
            class_case = ProductAttributeValue.objects.get(
                    product=obj.case, attribute=ProductAttribute.objects.get(code='class_case',
                                                                             product_class=ProductClass.objects.get(
                                                                                     slug='case_type'))).value_text
            return class_case
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist class_case', obj.pk)
            return None

    def prepare_power(self, obj):
        try:
            if obj.psus.exists():
                sum_power = 0
                # print(obj.psus)
                for computer_psu in obj.psus:
                    power = ProductAttributeValue.objects.get(
                            product=computer_psu.psu,
                            attribute=ProductAttribute.objects.get(code='power',
                                                                   product_class=ProductClass.objects.get(
                                                                           slug='psu_type'))).value_integer
                    # print(power, 'c')
                    sum_power += int(power)
                return sum_power
            power = ProductAttributeValue.objects.get(
                    product=obj.case, attribute=ProductAttribute.objects.get(code='power',
                                                                             product_class=ProductClass.objects.get(
                                                                                     slug='case_type'))).value_integer
            # print(power, 'p')
            return int(power)
        except TypeError:
            # print('TypeError power', obj.pk)
            return None
        except ObjectDoesNotExist:
            # print('ObjectDoesNotExist power', obj.pk)
            return None
        except:
            print(obj.pk)

    def prepare_has_odd(self, obj):
        return obj.odds.exists()

    def prepare(self, obj):
        prepared_data = super(ComputerIndexes, self).prepare(obj)

        # We use Haystack's dynamic fields to ensure that the title field used
        # for sorting is of type "string'.
        if is_solr_supported():
            prepared_data['title_s'] = prepared_data['title']

        # Use title to for spelling suggestions
        prepared_data['suggestions'] = prepared_data['text']

        return prepared_data

    def get_updated_field(self):
        """
        Used to specify the field used to determine if an object has been
        updated

        Can be used to filter the query set when updating the index
        """
        return 'date_updated'
