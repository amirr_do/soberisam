from collections import defaultdict
from oscar.apps.search.search_handlers import *


class SearchHandler(SearchHandler):

    def get_facet_munger(self):
        return FacetMunger(
            self.full_path,
            self.search_form.selected_multi_facets,
            self.correct_facet_counts(self.results.facet_counts(), self.results))

    def correct_facet_counts(self, facet_counts, results):
        narrow_queries = results.query.narrow_queries
        _narrow_queries = defaultdict(list)
        for narrow_query in narrow_queries:
            if narrow_query.split(':', 1)[0] not in ('django_ct', 'category_exact'):
                for append_query in narrow_queries:
                    if append_query != narrow_query:
                        _narrow_queries[narrow_query.split(':', 1)[0]].append(append_query)
        for key in _narrow_queries.keys():
            _results = results
            _results.query.narrow_queries = set(_narrow_queries[key])
            _results.query.run()
            key_field = key.replace('_exact', '')
            if key_field in facet_counts['fields'].keys():
                facet_counts['fields'][key_field] = _results.facet_counts()['fields'][key_field]
            else:
                for key_facet in facet_counts['queries'].keys():
                    if key in key_facet:
                        facet_counts['queries'][key_facet] = _results.facet_counts()['queries'][key_facet]

        return facet_counts
