from oscar.apps.search.views import FacetedSearchView as FacetedSearchViewOscar
from oscar.core.loading import get_model

Computer = get_model('computer', 'Computer')
Product = get_model('catalogue', 'Product')


class FacetedSearchView(FacetedSearchViewOscar):
    def get_results(self):
        return self.form.search().models(Computer, Product)
