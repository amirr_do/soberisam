from __future__ import unicode_literals
# -*- coding: utf-8 -*-
"""
Django settings for gamer_shop project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import djcelery
from django.utils.translation import ugettext_lazy as _
from oscar import OSCAR_MAIN_TEMPLATE_DIR
from oscar import get_core_apps

location = lambda x: os.path.join(
    os.path.dirname(os.path.realpath(__file__)), x)

TEMPLATE_DIRS = (
    location('templates'),
    OSCAR_MAIN_TEMPLATE_DIR,
)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
DEFAULT_SECRET_KEY = '3iy-!-d$!pc_ll$#$elg&cpr@*tfn-d5&n9ag=)%#()t$$5%5^'
SECRET_KEY = os.environ.get('SECRET_KEY', DEFAULT_SECRET_KEY)

# SECURITY WARNING: don't run with debug turned on in production!
#
# if 'ENVIRONMENT' in os.environ and os.environ['ENVIRONMENT'] == 'dev':
#     DEBUG = DEBUG
shell = os.environ.get('SHELL')
DEBUG = False
if shell == '/bin/zsh':
    DEBUG = True

TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Anton Fornolyak', 'a_vs@ukr.net'),
)

MANAGERS = ADMINS

ALLOWED_HOSTS = ['freeday.com.ua', 'sobsam.com', 'amrr.name']

# Application definition

INSTALLED_APPS = [
    'flat',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'widget_tweaks',
    'gamer_shop.computer',
    'gamer_shop.computer.reviews_computer',
    'gamer_shop.build',
    'compressor',
    # 'debug_toolbar',
    'celery_haystack',
    'djcelery',
]

INSTALLED_APPS += get_core_apps([
    'gamer_shop.address',
    'gamer_shop.catalogue',
    'gamer_shop.checkout',
    'gamer_shop.customer',
    'gamer_shop.promotions',
    'gamer_shop.order',
    'gamer_shop.partner',
    'gamer_shop.basket',
    'gamer_shop.search',
    'gamer_shop.shipping',
    'gamer_shop.dashboard.catalogue',
])

SITE_ID = 1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'oscar.apps.basket.middleware.BasketMiddleware',
    'gamer_shop.computer.middleware.ComputerMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'oscar.apps.search.context_processors.search_form',
    'oscar.apps.promotions.context_processors.promotions',
    'oscar.apps.checkout.context_processors.checkout',
    'oscar.apps.customer.notifications.context_processors.notifications',
    'oscar.core.context_processors.metadata',
)

AUTHENTICATION_BACKENDS = (
    'oscar.apps.customer.auth_backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'gamer_shop.urls'
MEDIA_URL = '/media/'
STATIC_URL = '/static/'

WSGI_APPLICATION = 'gamer_shop.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'django',
        'USER': 'django',
        'PASSWORD': '6dKWMvXiRe',
        'HOST': 'localhost',
        'PORT': '',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'localhost:6379',
    },
}
# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
#     }
# }

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8983/solr',
        'INCLUDE_SPELLING': True,
        # 'EXCLUDED_INDEXES': ['oscar.apps.search.search_indexes.ProductIndex'],
    },
}

HAYSTACK_SIGNAL_PROCESSOR = 'celery_haystack.signals.CelerySignalProcessor'

# адрес redis сервера
BROKER_URL = 'redis://localhost:6379/0'
# храним результаты выполнения задач так же в redis
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Kiev'
# в течение какого срока храним результаты, после чего они удаляются
CELERY_TASK_RESULT_EXPIRES = 7 * 86400  # 7 days
# это нужно для мониторинга наших воркеров
CELERY_SEND_EVENTS = True
# место хранения периодических задач (данные для планировщика)
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"

djcelery.setup_loader()

OSCAR_INITIAL_ORDER_STATUS = 'Pending'
OSCAR_INITIAL_LINE_STATUS = 'Pending'
OSCAR_ORDER_STATUS_PIPELINE = {
    'Pending': ('Being processed', 'Cancelled',),
    'Being processed': ('Processed', 'Cancelled',),
    'Cancelled': (),
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = location("public/media")
STATIC_ROOT = location("public/static")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATICFILES_DIRS = (
    location("static"),
)

STATICFILES_FINDERS = (
    "compressor.finders.CompressorFinder",
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

LOCALE_PATHS = (
    location("locale"),
)

from oscar.defaults import *
THUMBNAIL_DEBUG = True
EMAIL_SUBJECT_PREFIX = 'Gamer shop '
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.soberisam.com'
EMAIL_PORT = '25'
EMAIL_HOST_USER = 'info@soberisam.com'
EMAIL_HOST_PASSWORD = 'tls01Lkzx'
# EMAIL_USE_SSL = True

OSCAR_SHOP_NAME = 'Собери Сам'
OSCAR_DEFAULT_CURRENCY = 'UAH'
OSCAR_CURRENCY_FORMAT = '# грн.'
OSCAR_REQUIRED_ADDRESS_FIELDS = ('first_name', 'phone_number')
OSCAR_COMPUTER_COOKIE_OPEN = 'oscar_open_computer'
OSCAR_PRODUCTS_PER_PAGE = 23
OSCAR_ALLOW_ANON_CHECKOUT = True
OSCAR_FROM_EMAIL = 'info@soberisam.com'

BUILD_COST = 400

OSCAR_SEARCH_FACETS = {
    'fields': {
        # The key for these dicts will be used when passing facet data
        # to the template. Same for the 'queries' dict below.
        'rating': {
            'name': _('Rating'),
            'field': 'rating',
            'options': {'sort': 'index'}
        },
        'vendor': {
            'name': _('Vendor'),
            'field': 'vendor',
            'options': {'sort': 'index'}
            # 'options': {'missing': 'true'}
        },
        'socket': {
            'name': _('Socket'),
            'field': 'socket',
            'options': {'sort': 'index'}
            # 'options': {'missing': 'true'}
        },
        'core': {
            'name': _('Core'),
            'field': 'core',
            'options': {'sort': 'index'}
            # 'options': {'missing': 'true'}
        },
        'nm': {
            'name': _('nm'),
            'field': 'nm',
            'options': {'sort': 'index'}
            # 'options': {'missing': 'true'}
        },
        'tray': {
            'name': _('Tray/BOX'),
            'field': 'tray',
            'options': {'sort': 'index'}
            # 'options': {'missing': 'true'}
        },
        'has_video_card': {
            'name': _('Videocard'),
            'field': 'has_video_card',
            'options': {'sort': 'index'}
            # 'options': {'missing': 'true'}
        },
        'has_odd': {
            'name': _('ODD'),
            'field': 'has_odd',
            'options': {'sort': 'index'}
            # 'options': {'missing': 'true'}
        },
        # 'circulation': {
        #     'name': _('Circulation'),
        #     'field': 'circulation',
        # },
        # 'dB': {
        #     'name': _('Noise'),
        #     'field': 'dB',
        # },
        'connect': {
            'name': _('Connect'),
            'field': 'connect',
            'options': {'sort': 'index'}
        },
        'material': {
            'name': _('Material'),
            'field': 'material',
            'options': {'sort': 'index'}
        },
        'type_cooler': {
            'name': _('Cooler type'),
            'field': 'type_cooler',
            'options': {'sort': 'index'}
        },
        'chipset': {
            'name': _('Chipset'),
            'field': 'chipset',
            'options': {'sort': 'index'}
        },
        'form_factor_mb': {
            'name': _('Form factor'),
            'field': 'form_factor_mb',
            'options': {'sort': 'index'}
        },
        # 'size_mb': {
        #     'name': _('Size'),
        #     'field': 'size_mb',
        # },
        'volume_ram': {
            'name': _('Volume ram'),
            'field': 'volume_ram',
            'options': {'sort': 'index'}
        },
        'type_ram': {
            'name': _('Type ram'),
            'field': 'type_ram',
            'options': {'sort': 'index'}
        },
        'qty': {
            'name': _('Quantity'),
            'field': 'qty',
            'options': {'sort': 'index'}
        },
        'volume_video': {
            'name': _('Volume memory'),
            'field': 'volume_video',
            'options': {'sort': 'index'}
        },
        'type_video': {
            'name': _('Type memory'),
            'field': 'type_video',
            'options': {'sort': 'index'}
        },
        'gpu': {
            'name': _('GPU'),
            'field': 'gpu',
            'options': {'sort': 'index'}
        },
        'bit': {
            'name': _('Bit'),
            'field': 'bit',
            'options': {'sort': 'index'}
        },
        'cooling': {
            'name': _('Cooling'),
            'field': 'cooling',
            'options': {'sort': 'index'}
        },
        'volume_hdd': {
            'name': _('Volume disk'),
            'field': 'volume_hdd',
            'options': {'sort': 'index'}
        },
        'speed': {
            'name': _('Speed'),
            'field': 'speed',
            'options': {'sort': 'index'}
        },
        'form_factor_hdd': {
            'name': _('Form factor'),
            'field': 'form_factor_hdd',
            'options': {'sort': 'index'}
        },
        'interface': {
            'name': _('Interface'),
            'field': 'interface',
            'options': {'sort': 'index'}
        },
        'type_case': {
            'name': _('Type case'),
            'field': 'type_case',
            'options': {'sort': 'index'}
        },
        'class_case': {
            'name': _('Class case'),
            'field': 'class_case',
            'options': {'sort': 'index'}
        },
        'power': {
            'name': _('Power'),
            'field': 'power',
            'options': {'sort': 'index'}
        },
        'read_speed': {
            'name': _('Read speed'),
            'field': 'read_speed',
            'options': {'sort': 'index'}
        },
        'write_speed': {
            'name': _('Write speed'),
            'field': 'write_speed',
            'options': {'sort': 'index'}
        },
    },
    'queries': {
        'computer_psu_range': {
            'name': _('Power PSU'),
            'field': 'power',
            'queries': [
                (_('Empty'), u'[0 TO 49]'),
                (_('50 to 399 '), u'[50 TO 399]'),
                (_('400 to 449 '), u'[400 TO 449]'),
                (_('450 to 499 '), u'[450 TO 499]'),
                (_('500 to 599 '), u'[500 TO 599]'),
                (_('600 to 699 '), u'[600 TO 699]'),
                (_('700 to 799 '), u'[700 TO 799]'),
                (_('800 to 999 '), u'[800 TO 999]'),
                (_('1000 +'), u'[1000 TO *]'),
            ]
        },
        'power_psu_range': {
            'name': _('Power PSU'),
            'field': 'power',
            'queries': [
                (_('50 to 399 '), u'[50 TO 399]'),
                (_('400 to 449 '), u'[400 TO 449]'),
                (_('450 to 499 '), u'[450 TO 499]'),
                (_('500 to 599 '), u'[500 TO 599]'),
                (_('600 to 699 '), u'[600 TO 699]'),
                (_('700 to 799 '), u'[700 TO 799]'),
                (_('800 to 999 '), u'[800 TO 999]'),
                (_('1000 +'), u'[1000 TO *]'),
            ]
        },
        'power_case_range': {
            'name': _('Power case'),
            'field': 'power',
            'queries': [
                (_('No psu'), u'[0 TO 10]'),
                (_('50 to 400 '), u'[50 TO 400]'),
                (_('401 to 500 '), u'[401 TO 500]'),
                (_('501 +'), u'[501 TO *]'),
            ]
        },
        'read_speed_range': {
            'name': _('Read speed'),
            'field': 'read_speed',
            'queries': [
                (_('0 to 449 Mb/s'), u'[0 TO 449]'),
                (_('450 to 549 Mb/s'), u'[450 TO 549]'),
                (_('550 to 700 Mb/s'), u'[550 TO 700]'),
                (_('701 Mb/s+'), u'[701 TO *]'),
            ]
        },
        'write_speed_range': {
            'name': _('Write speed'),
            'field': 'write_speed',
            'queries': [
                (_('0 to 449 Mb/s'), u'[0 TO 449]'),
                (_('450 to 519 Mb/s'), u'[450 TO 519]'),
                (_('520 to 700 Mb/s'), u'[520 TO 700]'),
                (_('701 Mb/s+'), u'[701 TO *]'),
            ]
        },
        'frequency_range': {
            'name': _('Frequency'),
            'field': 'frequency',
            'queries': [
                (_('to 1333MHz'), u'[1 TO 1333]'),
                (_('1334 to 1600MHz'), u'[1334 TO 1600]'),
                (_('1700 to 1866MHz'), u'[1700 TO 1866]'),
                (_('2000 to 2133MHz'), u'[2000 TO 2133]'),
                (_('2400 to 2666MHz'), u'[2400 TO 2666]'),
                (_('2800MHz+'), u'[2800 TO *]'),
            ]
        },
        'freq_range': {
            'name': _('CPU frequency'),
            'field': 'freq',
            'queries': [
                (_('1.0 to 2.0GHz'), u'[1 TO 2]'),
                (_('2.1 to 2.8GHz'), u'[2.1 TO 2.8]'),
                (_('2.9 to 3.6GHz'), u'[2.9 TO 3.6]'),
                (_('3.7 to 4.4GHz'), u'[3.7 TO 4.4]'),
                (_('4.5GHz+'), u'[4.5 TO *]'),
            ]
        },
        'price_range': {
            'name': _('Price range'),
            'field': 'price',
            'queries': [
                (_('0 to 1000'), u'[0 TO 1000]'),
                (_('1000 to 2000'), u'[1000 TO 2000]'),
                (_('2000 to 4000'), u'[2000 TO 4000]'),
                (_('4000+'), u'[4000 TO *]'),
            ]
        },
        'volume_ssd_range': {
            'name': _('Volume SSD'),
            'field': 'volume_ssd',
            'queries': [
                (_('0 to 31'), u'[0 TO 31]'),
                (_('32 to 63'), u'[32 TO 63]'),
                (_('64 to 127'), u'[64 TO 127]'),
                (_('128 to 255'), u'[128 TO 255]'),
                (_('256 to 511'), u'[256 TO 511]'),
                (_('512 to 999'), u'[512 TO 999]'),
                (_('1000+'), u'[1000 TO *]'),
            ]
        },
    }
}
