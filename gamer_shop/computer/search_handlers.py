import datetime
from django.conf import settings
from django.views.generic.list import MultipleObjectMixin

from oscar.core.loading import get_class, get_model

BrowseCategoryForm = get_class('search.forms', 'BrowseCategoryForm')
SearchHandler = get_class('search.search_handlers', 'SearchHandler')
is_solr_supported = get_class('search.features', 'is_solr_supported')
Computer = get_model('computer', 'Computer')


def get_computer_search_handler_class():
    """
    Determine the search handler to use.

    Currently only Solr is supported as a search backend, so it falls
    back to rudimentary category browsing if that isn't enabled.
    """
    # Use get_class to ensure overridability
    if is_solr_supported():
        return get_class('computer.search_handlers', 'ComputerSearchHandler')
    else:
        return get_class(
            'computer.search_handlers', 'SimpleComputerSearchHandler')
    # return get_class(
    #     'computer.search_handlers', 'SimpleComputerSearchHandler')


class ComputerSearchHandler(SearchHandler):
    """
    Search handler specialised for searching products.  Comes with optional
    category filtering.
    """
    form_class = BrowseCategoryForm
    model_whitelist = [Computer]
    paginate_by = settings.OSCAR_PRODUCTS_PER_PAGE

    def __init__(self, request_data, full_path, categories=None, comp_date=None, user=None):
        self.date = comp_date
        self.categories = categories
        self.user = user
        super(ComputerSearchHandler, self).__init__(request_data, full_path)

    def get_search_queryset(self):
        sqs = super(ComputerSearchHandler, self).get_search_queryset()
        if self.categories:
            # We use 'narrow' API to ensure Solr's 'fq' filtering is used as
            # opposed to filtering using 'q'.
            pattern = ' OR '.join([
                '"%s"' % c.full_name for c in self.categories])
            sqs = sqs.narrow('category_exact:(%s)' % pattern)
        sqs = sqs.filter(status='Saved')
        if self.user and self.user.pk:
            sqs = sqs.filter(owner=self.user.pk)
        if self.date:
            if self.date['year']:
                y = int(self.date['year'])
                if self.date['month']:
                    m = int(self.date['month'])
                    if self.date['day']:
                        d = int(self.date['day'])
                        sqs = sqs.filter(date_updated__gte=datetime.datetime(y, m, d, 0, 0, 0),
                                         date_updated__lte=datetime.datetime(y, m, d, 23, 59, 59))
                    else:
                        if m == 12:
                            sqs = sqs.filter(date_updated__gte=datetime.date(y, m, 1),
                                             date_updated__lt=datetime.date(y + 1, 1, 1))
                        else:
                            sqs = sqs.filter(date_updated__gte=datetime.date(y, m, 1),
                                             date_updated__lt=datetime.date(y, m + 1, 1))
                else:
                    sqs = sqs.filter(date_updated__gte=datetime.date(y, 1, 1),
                                     date_updated__lt=datetime.date(y + 1, 1, 1))
        return sqs


class SimpleComputerSearchHandler(MultipleObjectMixin):
    """
    A basic implementation of the full-featured SearchHandler that has no
    faceting support, but doesn't require a Haystack backend. It only
    supports category browsing.

    Note that is meant as a replacement search handler and not as a view
    mixin; the mixin just does most of what we need it to do.
    """
    paginate_by = settings.OSCAR_PRODUCTS_PER_PAGE

    def __init__(self, request_data, full_path, categories=None, comp_date=None):
        self.date = comp_date
        self.categories = categories
        self.kwargs = {'page': request_data.get('page', 1)}
        self.object_list = self.get_queryset()

    def get_queryset(self):
        qs = Computer.saved.all()
        if self.categories:
            qs = qs.filter(categories__in=self.categories).distinct()

        if self.date:
            if self.date['year']:
                qs = qs.filter(date_created__year=self.date['year'])
            if self.date['month']:
                qs = qs.filter(date_created__month=self.date['month'])
            if self.date['day']:
                qs = qs.filter(date_created__day=self.date['day'])

        return qs

    def get_search_context_data(self, context_object_name):
        # Set the context_object_name instance property as it's needed
        # internally by MultipleObjectMixin
        self.context_object_name = context_object_name
        context = self.get_context_data(object_list=self.object_list)
        context[context_object_name] = context['page_obj'].object_list
        return context
