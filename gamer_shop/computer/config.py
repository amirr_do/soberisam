from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ComputerConfig(AppConfig):
    label = 'computer'
    name = 'gamer_shop.computer'
    verbose_name = _('Computer')

    def ready(self):
        from . import receivers  # noqa
