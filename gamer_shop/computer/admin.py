from django.contrib import admin
from oscar.core.loading import get_model

PCModel = get_model('computer', 'Computer')
ComputerHDD = get_model('computer', 'ComputerHDD')
ComputerODD = get_model('computer', 'ComputerODD')
ComputerPSU = get_model('computer', 'ComputerPSU')
ComputerRAM = get_model('computer', 'ComputerRAM')
ComputerSSD = get_model('computer', 'ComputerSSD')
ComputerVideoCard = get_model('computer', 'ComputerVideoCard')


class HDDInline(admin.TabularInline):
    model = ComputerHDD


class ODDInline(admin.TabularInline):
    model = ComputerODD


class PSUInline(admin.TabularInline):
    model = ComputerPSU


class RAMInline(admin.TabularInline):
    model = ComputerRAM


class SSDInline(admin.TabularInline):
    model = ComputerSSD


class VideoCardInline(admin.TabularInline):
    model = ComputerVideoCard


class PCModelAdmin(admin.ModelAdmin):
    inlines = [HDDInline, ODDInline, PSUInline, RAMInline, SSDInline, VideoCardInline]


admin.site.register(PCModel, PCModelAdmin)
