from django.contrib import admin

from gamer_shop.computer.reviews_computer.models import ComputerReview, VoteComputer


class ComputerReviewAdmin(admin.ModelAdmin):
    list_display = ('computer', 'title', 'score', 'status', 'total_votes',
                    'delta_votes', 'date_created')
    readonly_fields = ('total_votes', 'delta_votes')


class VoteComputerAdmin(admin.ModelAdmin):
    list_display = ('review', 'user', 'delta', 'date_created')

admin.site.register(ComputerReview, ComputerReviewAdmin)
admin.site.register(VoteComputer, VoteComputerAdmin)
