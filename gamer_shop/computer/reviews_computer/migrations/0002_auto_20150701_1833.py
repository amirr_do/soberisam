# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews_computer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='computerreview',
            name='email',
            field=models.EmailField(verbose_name='Email', max_length=254, blank=True),
        ),
    ]
