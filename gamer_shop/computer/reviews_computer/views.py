from django.shortcuts import get_object_or_404, redirect
from django.views.generic import ListView, DetailView, CreateView, View
from django.contrib import messages
from oscar.core.loading import get_model
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_classes
from oscar.core.utils import redirect_to_referrer
from gamer_shop.computer.reviews_computer.signals import review_added

ComputerReviewForm, VoteForm, SortReviewsForm = get_classes(
    'computer.reviews_computer.forms',
    ['ComputerReviewForm', 'VoteForm', 'SortReviewsForm'])
Vote = get_model('reviews_computer', 'VoteComputer')
ComputerReview = get_model('reviews_computer', 'ComputerReview')
Computer = get_model('computer', 'Computer')


class CreateComputerReview(CreateView):
    template_name = "computer/reviews/review_form.html"
    model = ComputerReview
    computer_model = Computer
    form_class = ComputerReviewForm
    view_signal = review_added

    def dispatch(self, request, *args, **kwargs):
        self.computer = get_object_or_404(
            self.computer_model, pk=kwargs['computer_pk'])
        # check permission to leave review
        if not self.computer.is_review_permitted(request.user):
            if self.computer.has_review_by(request.user):
                message = _("You have already reviewed this product!")
            else:
                message = _("You can't leave a review for this product.")
            messages.warning(self.request, message)
            return redirect(self.computer.get_absolute_url())

        return super(CreateComputerReview, self).dispatch(
            request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreateComputerReview, self).get_context_data(**kwargs)
        context['computer'] = self.computer
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateComputerReview, self).get_form_kwargs()
        kwargs['computer'] = self.computer
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        response = super(CreateComputerReview, self).form_valid(form)
        self.send_signal(self.request, response, self.object)
        return response

    def get_success_url(self):
        messages.success(
            self.request, _("Thank you for reviewing this product"))
        return self.computer.get_absolute_url()

    def send_signal(self, request, response, review):
        self.view_signal.send(sender=self, review=review, user=request.user,
                              request=request, response=response)


class ComputerReviewDetail(DetailView):
    template_name = "computer/reviews/review_detail.html"
    context_object_name = 'review'
    model = ComputerReview

    def get_context_data(self, **kwargs):
        context = super(ComputerReviewDetail, self).get_context_data(**kwargs)
        context['computer'] = get_object_or_404(
            Computer, pk=self.kwargs['computer_pk'])
        return context


class AddVoteView(View):
    """
    Simple view for voting on a review.

    We use the URL path to determine the product and review and use a 'delta'
    POST variable to indicate it the vote is up or down.
    """

    def post(self, request, *args, **kwargs):
        computer = get_object_or_404(Computer, pk=self.kwargs['computer_pk'])
        review = get_object_or_404(ComputerReview, pk=self.kwargs['pk'])

        form = VoteForm(review, request.user, request.POST)
        if form.is_valid():
            if form.is_up_vote:
                review.vote_up(request.user)
            elif form.is_down_vote:
                review.vote_down(request.user)
            messages.success(request, _("Thanks for voting!"))
        else:
            for error_list in form.errors.values():
                for msg in error_list:
                    messages.error(request, msg)
        return redirect_to_referrer(request, computer.get_absolute_url())


class ComputerReviewList(ListView):
    """
    Browse reviews for a product
    """
    template_name = 'computer/reviews/review_list.html'
    context_object_name = "reviews"
    model = ComputerReview
    computer_model = Computer
    paginate_by = 20

    def get_queryset(self):
        qs = self.model.approved.filter(computer=self.kwargs['computer_pk'])
        self.form = SortReviewsForm(self.request.GET)
        if self.form.is_valid():
            sort_by = self.form.cleaned_data['sort_by']
            if sort_by == SortReviewsForm.SORT_BY_RECENCY:
                return qs.order_by('-date_created')
        return qs.order_by('-score')

    def get_context_data(self, **kwargs):
        context = super(ComputerReviewList, self).get_context_data(**kwargs)
        context['computer'] = get_object_or_404(
            self.computer_model, pk=self.kwargs['computer_pk'])
        context['form'] = self.form
        return context
