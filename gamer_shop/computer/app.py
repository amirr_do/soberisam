from django.conf.urls import url, include

from oscar.core.application import Application
from oscar.core.loading import get_class
from gamer_shop.computer.reviews_computer.app import application as reviews_app


class DateComputerApplication(Application):
    name = 'all'
    computer_date_view = get_class('computer.views', 'ComputerDateView')
    computer_type_view = get_class('computer.views', 'ComputerTypeView')

    def __init__(self, *args, **kwargs):
        if kwargs.get('my'):
            self.name = 'my'
            self.computer_date_view = get_class('computer.views', 'MyComputerDateView')
            self.computer_type_view = get_class('computer.views', 'MyComputerTypeView')
        super(DateComputerApplication, self).__init__(*args, **kwargs)

    def get_urls(self):
        urlpatterns = super(DateComputerApplication, self).get_urls()
        urlpatterns += [
            url(r'^$',
                self.computer_type_view.as_view(), name='type2'),
            url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/$',
                self.computer_date_view.as_view(), name='filter_day'),
            url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/$',
                self.computer_date_view.as_view(), name='filter_month'),
            url(r'^(?P<year>\d{4})/$',
                self.computer_date_view.as_view(), name='filter_year'),
        ]
        return self.post_process_urls(urlpatterns)


date_computer_app = DateComputerApplication(my=False)
my_computer_app = DateComputerApplication(my=True)


class BaseComputerApplication(Application):
    name = 'computer'
    detail_view = get_class('computer.views', 'ComputerDetailView')
    edit_view = get_class('computer.views', 'ComputerEditView')
    pre_edit_view = get_class('computer.views', 'ComputerPreEditView')
    pre_save_view = get_class('computer.views', 'AuthView')
    catalogue_view = get_class('computer.views', 'CatalogueView')
    # range_view = get_class('offer.views', 'RangeDetailView')

    def get_urls(self):
        urlpatterns = super(BaseComputerApplication, self).get_urls()
        urlpatterns += [
            # url(r'^$', self.catalogue_view.as_view(), name='index'),
            url(r'^detail/(?P<computer_slug>[\w-]*)_(?P<pk>\d+)/$',
                self.detail_view.as_view(), name='detail'),
            url(r'^edit/$',
                self.edit_view.as_view(), name='edit'),
            url(r'^pre_edit/(?P<pk>\d+)/$',
                self.pre_edit_view.as_view(), name='pre_edit'),
            url(r'^pre_save_login/$',
                self.pre_save_view.as_view(), name='pre_save_login'),
            url(r'^my_builds/', include(my_computer_app.urls), name='my'),
            url(r'^(?P<category_slug>[\w-]+([\w-]+)*)/', include(date_computer_app.urls), name='all'),
        ]
        return self.post_process_urls(urlpatterns)


class ReviewsApplication(Application):
    name = None
    reviews_app = reviews_app

    def get_urls(self):
        urlpatterns = super(ReviewsApplication, self).get_urls()
        urlpatterns += [
            url(r'^(?P<computer_slug>[\w-]*)_(?P<computer_pk>\d+)/reviews/',
                include(self.reviews_app.urls)),
        ]
        return self.post_process_urls(urlpatterns)


class ComputerApplication(BaseComputerApplication, ReviewsApplication):
    """
    Composite class combining Products with Reviews
    """


application = ComputerApplication()
