import os
import logging

from django.utils import six
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.conf import settings
from django.contrib.staticfiles.finders import find
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Sum, Count
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, pgettext_lazy
from django.utils.functional import cached_property
from django.contrib.contenttypes.generic import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from oscar.core.decorators import deprecated
from oscar.core.utils import slugify
from oscar.core.loading import get_classes
from oscar.models.fields import NullCharField
from oscar.core.compat import AUTH_USER_MODEL
from gamer_shop.catalogue.models import ProductAttributeValue

from gamer_shop.catalogue.models import ProductAttributesContainer, ProductAttribute

from gamer_shop.catalogue.models import ProductClass, Category, ProductImage


def product_class_default():
    return ProductClass.objects.get(slug='computer_type')


def category_default():
    return Category.objects.get(slug='user_builds')


ComputerManager, BrowsableComputerManager, OpenComputerManager, SavedComputerManager = get_classes(
    'computer.managers', ['ComputerManager', 'BrowsableComputerManager', 'OpenComputerManager', 'SavedComputerManager'])


@python_2_unicode_compatible
class Computer(models.Model):
    """
    The base product object

    There's three kinds of products; they're distinguished by the structure
    field.

    - A stand alone product. Regular product that lives by itself.
    - A child product. All child products have a parent product. They're a
      specific version of the parent.
    - A parent product. It essentially represents a set of products.

    An example could be a yoga course, which is a parent product. The different
    times/locations of the courses would be associated with the child products.
    """

    owner = models.ForeignKey(
        AUTH_USER_MODEL, related_name='computers', null=True,
        verbose_name=_("Owner"))
    short_description = models.TextField(_('Short Description'), blank=True)
    upc = NullCharField(
        _("UPC"), max_length=64, blank=True, null=True, unique=True,
        help_text=_("Universal Product Code (UPC) is an identifier for "
                    "a product which is not specific to a particular "
                    " supplier. Eg an ISBN for a book."))

    # Title is mandatory for canonical products but optional for child products
    title = models.CharField(pgettext_lazy(u'Computer title', u'Title'),
                             max_length=255, blank=True)
    slug = models.SlugField(_('Slug'), max_length=255, unique=False)
    description = models.TextField(_('Description'), blank=True)

    #: "Kind" of product, e.g. T-Shirt, Book, etc.
    #: None for child products, they inherit their parent's product class
    product_class = models.ForeignKey(
        'catalogue.ProductClass', null=True, on_delete=models.PROTECT, default=product_class_default,
        verbose_name=_('Product type'), related_name="computers",
        help_text=_("Choose what type of product this is"))

    OPEN, SAVED, FROZEN = (
        "Open", "Saved", "Frozen")
    STATUS_CHOICES = (
        (OPEN, _("Open - currently active")),
        (SAVED, _("Saved - for items to be purchased later")),
        (FROZEN, _("Frozen - the basket cannot be modified")),
    )
    status = models.CharField(
        _("Status"), max_length=128, default=OPEN, choices=STATUS_CHOICES)
    editable_statuses = (OPEN, SAVED)

    attributes = models.ManyToManyField(
        'catalogue.ProductAttribute',
        through='ComputerAttributeValue',
        verbose_name=_("Attributes"),
        help_text=_("A product attribute is something that this product may "
                    "have, such as a size, as specified by its class"))
    #: It's possible to have options product class-wide, and per product.
    product_options = models.ManyToManyField(
        'catalogue.Option', blank=True, verbose_name=_("Product options"),
        help_text=_("Options are values that can be associated with a item "
                    "when it is added to a customer's basket.  This could be "
                    "something like a personalised message to be printed on "
                    "a T-shirt."))

    # Denormalised product rating - used by reviews app.
    # Product has no ratings if rating is None
    rating = models.FloatField(_('Rating'), null=True, editable=False)

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)

    # This field is used by Haystack to reindex search
    date_updated = models.DateTimeField(
        _("Date updated"), auto_now=True, db_index=True)

    categories = models.ManyToManyField(
        'catalogue.Category', through='ComputerCategory',
        verbose_name=_("Categories"))

    #: Determines if a product may be used in an offer. It is illegal to
    #: discount some types of product (e.g. ebooks) and this field helps
    #: merchants from avoiding discounting such products
    #: Note that this flag is ignored for child products; they inherit from
    #: the parent product.
    is_discountable = models.BooleanField(
        _("Is discountable?"), default=True, help_text=_(
            "This flag indicates if this product can be used in an offer "
            "or not"))

    case = models.ForeignKey('catalogue.Case', related_name='computer_models', related_query_name='computer_model',
                             on_delete=models.PROTECT, verbose_name=_('Case of model'), blank=True, null=True)
    cooler = models.ForeignKey('catalogue.Cooler', related_name='computer_models', related_query_name='computer_model',
                               on_delete=models.PROTECT, verbose_name=_('Cooler of model'), blank=True, null=True)
    cpu = models.ForeignKey('catalogue.CPU', related_name='computer_models', related_query_name='computer_model',
                            on_delete=models.PROTECT, verbose_name=_('CPU of model'), blank=True, null=True)
    motherboard = models.ForeignKey('catalogue.Motherboard', related_name='computer_models',
                                    related_query_name='computer_model', on_delete=models.PROTECT,
                                    verbose_name=_('Motherboard of model'), blank=True, null=True)
    hdd = models.ManyToManyField('catalogue.HDD', related_name='computer_models', related_query_name='computer_model',
                                 through='ComputerHDD', verbose_name=_('HDD of model'), blank=True)
    odd = models.ManyToManyField('catalogue.ODD', related_name='computer_models', related_query_name='computer_model',
                                 through='ComputerODD', verbose_name=_('ODD of model'), blank=True)
    psu = models.ManyToManyField('catalogue.PSU', related_name='computer_models', related_query_name='computer_model',
                                 through='ComputerPSU', verbose_name=_('PSU of model'), blank=True)
    ram = models.ManyToManyField('catalogue.RAM', related_name='computer_models', related_query_name='computer_model',
                                 through='ComputerRAM', verbose_name=_('RAM of model'), blank=True)
    ssd = models.ManyToManyField('catalogue.SSD', related_name='computer_models', related_query_name='computer_model',
                                 through='ComputerSSD', verbose_name=_('SSD of model'), blank=True)
    video_card = models.ManyToManyField('catalogue.VideoCard', related_name='computer_models',
                                        related_query_name='computer_model', through='ComputerVideoCard',
                                        verbose_name=_('Video card of model'), blank=True)

    objects = ComputerManager()
    browsable = BrowsableComputerManager()
    open = OpenComputerManager()
    saved = SavedComputerManager()

    class Meta:
        app_label = 'computer'
        ordering = ['-date_created']
        verbose_name = _('Computer')
        verbose_name_plural = _('Computers')

    def __init__(self, *args, **kwargs):
        super(Computer, self).__init__(*args, **kwargs)
        self.attr = ProductAttributesContainer(product=self)

    def __str__(self):
        if self.title:
            return self.title
        if self.attribute_summary:
            return u"%s (%s)" % (self.get_title(), self.attribute_summary)
        else:
            return self.get_title()

    def get_absolute_url(self):
        """
        Return a product's absolute url
        """
        return reverse('computer:detail',
                       kwargs={'computer_slug': self.slug, 'pk': self.id})

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.get_title())
        super(Computer, self).save(*args, **kwargs)

        cache_delete = lambda k, vs: cache.delete(make_template_fragment_key(k, vs))

        cache_delete('current_comp_price', [self.pk])
        cache_delete('current_build_price', [self.pk])
        cache_delete('comp_price', [self.pk])
        cache_delete('comp_button_buy', [self.pk])
        cache_delete('short_comp_info', [self.pk])

        if not self.title or self.title == 'User computer' or self.title == 'Сборка №None':
            self.title = 'Сборка №{}'.format(self.id)
            self.slug = 'user-build'
            self.save()
        self.attr.save()

    def is_current(self, product):

        is_current = False
        if product.product_class_id == 1:
            if self.cpu == product.cpu:
                return True
            return False
        elif product.product_class_id == 2:
            if self.cooler == product.cooler:
                return True
            return False
        elif product.product_class_id == 3:
            if self.motherboard == product.motherboard:
                return True
            return False
        elif product.product_class_id == 4:
            is_current = self.computerram_set.filter(ram=product.ram, computer=self).exists()
        elif product.product_class_id == 5:
            is_current = self.computervideocard_set.filter(video_card=product.videocard, computer=self).exists()
        elif product.product_class_id == 6:
            is_current = self.computerhdd_set.filter(hdd=product.hdd, computer=self).exists()
        elif product.product_class_id == 7:
            is_current = self.computerssd_set.filter(ssd=product.ssd, computer=self).exists()
        elif product.product_class_id == 8:
            is_current = self.computerodd_set.filter(odd=product.odd, computer=self).exists()
        elif product.product_class_id == 9:
            if self.case == product.case:
                return True
            return False
        elif product.product_class_id == 10:
            is_current = self.computerpsu_set.filter(psu=product.psu, computer=self).exists()
        return is_current

    def cleaning(self):
        self.cpu = None
        self.cooler = None
        self.motherboard = None
        self.case = None
        self.computerram_set.filter(computer=self).delete()
        self.computervideocard_set.filter(computer=self).delete()
        self.computerhdd_set.filter(computer=self).delete()
        self.computerssd_set.filter(computer=self).delete()
        self.computerodd_set.filter(computer=self).delete()
        self.computerpsu_set.filter(computer=self).delete()
        ComputerImage.objects.filter(computer=self).delete()
        self.save()

    cleaning.alters_data = True
    cleaning = cleaning

    def delete_product(self, product):

        if product.product_class_id == 1:
            self.cpu = None
        elif product.product_class_id == 2:
            self.cooler = None
        elif product.product_class_id == 3:
            self.motherboard = None
        elif product.product_class_id == 4:
            self.computerram_set.filter(ram=product.ram, computer=self).delete()
        elif product.product_class_id == 5:
            self.computervideocard_set.filter(video_card=product.videocard, computer=self).delete()
        elif product.product_class_id == 6:
            self.computerhdd_set.filter(hdd=product.hdd, computer=self).delete()
        elif product.product_class_id == 7:
            self.computerssd_set.filter(ssd=product.ssd, computer=self).delete()
        elif product.product_class_id == 8:
            self.computerodd_set.filter(odd=product.odd, computer=self).delete()
        elif product.product_class_id == 9:
            self.case = None
            ComputerImage.objects.filter(computer=self).delete()
        elif product.product_class_id == 10:
            self.computerpsu_set.filter(psu=product.psu, computer=self).delete()
        self.save()

    delete_product.alters_data = True
    delete_product = delete_product

    def add_product(self, product, quantity, options):
        if options is None:
            options = []
        if not self.id:
            self.save()
        # Ensure that all lines are the same currency
        price_currency = self.currency
        stock_info = self.strategy.fetch_for_product(product)
        if price_currency and stock_info.price.currency != price_currency:
            raise ValueError((
                                 "Basket lines must all have the same currency. Proposed "
                                 "line has currency %s, while basket has currency %s")
                             % (stock_info.price.currency, price_currency))

        if stock_info.stockrecord is None:
            raise ValueError((
                                 "Basket lines must all have stock records. Strategy hasn't "
                                 "found any stock record for product %s") % product)

        # Determine price to store (if one exists).  It is only stored for
        # audit and sometimes caching.
        if product.product_class_id == 1:
            product_socket = ProductAttributeValue.objects.get(
                product=product, attribute=ProductAttribute.objects.get(code='socket', product_class=1)
            ).value_text
            computer_socket = ComputerAttributeValue.objects.get(
                computer=self, attribute=ProductAttribute.objects.get(code='socket_computer')
            ).value_text
            if product_socket == computer_socket or ('FM2' in product_socket and 'FM2' in computer_socket):
                self.cpu = product.cpu
            else:
                msg = 'Сокет вашей сборки не совпадает с сокетом процессора.' \
                      'Выберите другой процессор или измените сокет сборки.'
                return False, msg
        elif product.product_class_id == 2:
            self.cooler = product.cooler
        elif product.product_class_id == 3:
            product_socket = ProductAttributeValue.objects.get(
                product=product, attribute=ProductAttribute.objects.get(code='socket', product_class=3)
            ).value_text
            computer_socket = ComputerAttributeValue.objects.get(
                computer=self, attribute=ProductAttribute.objects.get(code='socket_computer')
            ).value_text
            if product_socket in computer_socket or ('FM2' in product_socket and 'FM2' in computer_socket):
                self.motherboard = product.motherboard
            else:
                msg = 'Сокет вашей сборки не совпадает с сокетом мат. платы.' \
                      'Выберите другую мат. плату или измените сокет сборки.'
                return False, msg
        elif product.product_class_id == 4:
            ram = self.computerram_set.filter(computer=self)
            if ram:
                self.computerram_set.update(ram=product.ram, count=quantity)
            else:
                self.computerram_set.create(
                    ram=product.ram,
                    computer=self,
                    count=quantity)
        elif product.product_class_id == 5:
            videocard = self.computervideocard_set.filter(computer=self)
            if videocard:
                self.computervideocard_set.update(video_card=product.videocard, count=quantity)
            else:
                self.computervideocard_set.create(
                    video_card=product.videocard,
                    computer=self,
                    count=quantity)
        elif product.product_class_id == 6:
            hdd = self.computerhdd_set.filter(computer=self)
            if hdd:
                self.computerhdd_set.update(hdd=product.hdd, count=quantity)
            else:
                self.computerhdd_set.create(
                    hdd=product.hdd,
                    computer=self,
                    count=quantity)
        elif product.product_class_id == 7:
            ssd = self.computerssd_set.filter(computer=self)
            if ssd:
                self.computerssd_set.update(ssd=product.ssd, count=quantity)
            else:
                self.computerssd_set.create(
                    ssd=product.ssd,
                    computer=self,
                    count=quantity)
        elif product.product_class_id == 8:
            odd = self.computerodd_set.filter(computer=self)
            if odd:
                self.computerodd_set.update(odd=product.odd, count=quantity)
            else:
                self.computerodd_set.create(
                    odd=product.odd,
                    computer=self,
                    count=quantity)
        elif product.product_class_id == 9:
            self.case = product.case
            ComputerImage.objects.filter(computer=self).delete()

            images = ProductImage.objects.filter(product=self.case)
            for i, image in enumerate(images):
                image_name = 'build{}_{}.{}'.format(i, self.pk, image.original.name.split('.')[-1])
                image_file = image.original.file

                product_image = ComputerImage.objects.create(computer=self, display_order=i)
                product_image.original.save(image_name, image_file)

        elif product.product_class_id == 10:
            psu = self.computerpsu_set.filter(computer=self)
            if psu:
                self.computerpsu_set.update(psu=product.psu, count=quantity)
            else:
                self.computerpsu_set.create(
                    psu=product.psu,
                    computer=self,
                    count=quantity)
        self.save()
        return True, None

    add_product.alters_data = True
    add = add_product

    @property
    def is_socket(self):
        socket_exists = ComputerAttributeValue.objects.filter(
            computer=self, attribute=ProductAttribute.objects.get(code='socket_computer')).exists()
        return socket_exists

    @property
    def currency(self):
        return u'UAH'

    @property
    def rams(self):
        return ComputerRAM.objects.filter(computer=self)

    @property
    def video_cards(self):
        return ComputerVideoCard.objects.filter(computer=self)

    @property
    def hdds(self):
        return ComputerHDD.objects.filter(computer=self)

    @property
    def ssds(self):
        return ComputerSSD.objects.filter(computer=self)

    @property
    def odds(self):
        return ComputerODD.objects.filter(computer=self)

    @property
    def psus(self):
        return ComputerPSU.objects.filter(computer=self)

    @property
    def ram_volume(self):
        summ = 0
        for computer in self.rams:
            summ += int(computer.ram.attr.volume or 0) * computer.count
        return summ

    @property
    def hdd_volume(self):
        summ = 0
        for computer in self.hdds:
            summ += int(computer.hdd.attr.volume or 0) * computer.count
        return summ

    @property
    def ssd_volume(self):
        summ = 0
        for computer in self.ssds:
            summ += int(computer.ssd.attr.volume or 0) * computer.count
        return summ

    @property
    def video_models(self):
        video = list()
        for computer in self.video_cards:
            video.append(computer.video_card.attr.model)
        return video

    @property
    def num_items(self):
        return 10

    def product_quantity(self, product):
        return 1

    def freeze(self):
        """
        Freezes the basket so it cannot be modified.
        """
        self.status = self.FROZEN
        self.save()

    freeze.alters_data = True

    # Properties

    @property
    def is_empty(self):
        return self.id is None or not any((self.case, self.cooler, self.cpu, self.motherboard, self.hdds, self.odds,
                                           self.psus, self.rams, self.ssds, self.video_cards))

    @property
    def is_full(self):
        return all((self.case, self.cpu, self.motherboard, self.rams)) and any((self.hdds, self.ssds))\
               and any((self.psus, self.case.attr.power))

    @property
    def options(self):
        """
        Returns a set of all valid options for this product.
        It's possible to have options product class-wide, and per product.
        """
        pclass_options = self.get_product_class().options.all()
        return set(pclass_options) or set(self.product_options.all())

    @property
    def is_shipping_required(self):
        return self.get_product_class().requires_shipping

    @property
    def has_stockrecords(self):
        """
        Test if this product has any stockrecords
        """
        return self.stockrecords.exists()

    @property
    def num_stockrecords(self):
        return self.stockrecords.count()

    @property
    def attribute_summary(self):
        """
        Return a string of all of a product's attributes
        """
        attributes = self.attribute_values.all()
        pairs = [attribute.summary() for attribute in attributes]
        return ", ".join(pairs)

    @property
    @deprecated
    def is_top_level(self):
        """
        Test if this product is a stand-alone or parent product
        """
        return True

    @property
    @deprecated
    def is_group(self):
        """
        Test if this is a parent product
        """
        return False

    @property
    @deprecated
    def is_variant(self):
        """Return True if a product is not a top level product"""
        return False

    # Wrappers for child products

    def get_title(self):
        """
        Return a product's title or it's parent's title if it has no title
        """
        if not self.id:
            return 'Сборка №{}'.format(self.id)
        if not self.title or self.title == 'User computer' or self.title == 'Сборка №None':
            self.title = 'Сборка №{}'.format(self.id)
        title = self.title
        return title

    get_title.short_description = pgettext_lazy(u"Computer title", u"Title")

    def get_product_class(self):
        """
        Return a product's item class. Child products inherit their parent's.
        """
        return self.product_class

    get_product_class.short_description = _("Computer class")

    def get_is_discountable(self):
        """
        At the moment, is_discountable can't be set individually for child
        products; they inherit it from their parent.
        """
        return self.is_discountable

    def get_categories(self):
        """
        Return a product's categories or parent's if there is a parent product.
        """
        return self.categories

    get_categories.short_description = _("Categories")

    # Images

    def get_missing_image(self):
        """
        Returns a missing image object.
        """
        # This class should have a 'name' property so it mimics the Django file
        # field.
        return MissingComputerImage()

    def primary_image(self):
        """
        Returns the primary image for a product. Usually used when one can
        only display one product image, e.g. in a list of products.
        """
        images = self.images.all()
        ordering = self.images.model._meta.ordering
        if not ordering or ordering[0] != 'display_order':
            # Only apply order_by() if a custom model doesn't use default
            # ordering. Applying order_by() busts the prefetch cache of
            # the ProductManager
            images = images.order_by('display_order')
        try:
            return images[0]
        except IndexError:
            # We return a dict with fields that mirror the key properties of
            # the ProductImage class so this missing image can be used
            # interchangeably in templates.  Strategy pattern ftw!
            return {
                'original': self.get_missing_image(),
                'caption': '',
                'is_missing': True}

    # Updating methods

    def update_rating(self):
        """
        Recalculate rating field
        """
        self.rating = self.calculate_rating()
        self.save()

    update_rating.alters_data = True

    def calculate_rating(self):
        """
        Calculate rating value
        """
        result = self.reviews.filter(
            status=self.reviews.model.APPROVED
        ).aggregate(
            sum=Sum('score'), count=Count('id'))
        reviews_sum = result['sum'] or 0
        reviews_count = result['count'] or 0
        rating = None
        if reviews_count > 0:
            rating = float(reviews_sum) / reviews_count
        return rating

    def has_review_by(self, user):
        if user.is_anonymous():
            return False
        return self.reviews.filter(user=user).exists()

    def is_review_permitted(self, user):
        """
        Determines whether a user may add a review on this product.

        Default implementation respects OSCAR_ALLOW_ANON_REVIEWS and only
        allows leaving one review per user and product.

        Override this if you want to alter the default behaviour; e.g. enforce
        that a user purchased the product to be allowed to leave a review.
        :arg user = current user
        """
        if user.is_authenticated() or settings.OSCAR_ALLOW_ANON_REVIEWS:
            return not self.has_review_by(user)
        else:
            return False

    @cached_property
    def num_approved_reviews(self):
        return self.reviews.filter(
            status=self.reviews.model.APPROVED).count()


@python_2_unicode_compatible
class ComputerAttributeValue(models.Model):
    """
    The "through" model for the m2m relationship between catalogue.Product and
    catalogue.ProductAttribute.  This specifies the value of the attribute for
    a particular product

    For example: number_of_pages = 295
    """
    attribute = models.ForeignKey(
        'catalogue.ProductAttribute', verbose_name=_("Attribute"))
    computer = models.ForeignKey(
        'computer.Computer', related_name='attribute_values',
        verbose_name=_("Computer"))

    value_text = models.TextField(_('Text'), blank=True, null=True)
    value_integer = models.IntegerField(_('Integer'), blank=True, null=True)
    value_boolean = models.NullBooleanField(_('Boolean'), blank=True)
    value_float = models.FloatField(_('Float'), blank=True, null=True)
    value_richtext = models.TextField(_('Richtext'), blank=True, null=True)
    value_date = models.DateField(_('Date'), blank=True, null=True)
    value_option = models.ForeignKey(
        'catalogue.AttributeOption', blank=True, null=True,
        verbose_name=_("Value option"))
    value_file = models.FileField(
        upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255,
        blank=True, null=True)
    value_image = models.ImageField(
        upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255,
        blank=True, null=True)
    value_entity = GenericForeignKey(
        'entity_content_type', 'entity_object_id')

    entity_content_type = models.ForeignKey(
        ContentType, null=True, blank=True, editable=False)
    entity_object_id = models.PositiveIntegerField(
        null=True, blank=True, editable=False)

    def _get_value(self):
        return getattr(self, 'value_%s' % self.attribute.type)

    def _set_value(self, new_value):
        if self.attribute.is_option and isinstance(new_value, str):
            # Need to look up instance of AttributeOption
            new_value = self.attribute.option_group.options.get(
                option=new_value)
        setattr(self, 'value_%s' % self.attribute.type, new_value)

    value = property(_get_value, _set_value)

    class Meta:
        app_label = 'computer'
        unique_together = ('attribute', 'computer')
        verbose_name = _('Computer attribute value')
        verbose_name_plural = _('Computer attribute values')

    def __str__(self):
        return self.summary()

    def summary(self):
        """
        Gets a string representation of both the attribute and it's value,
        used e.g in product summaries.
        """
        return u"%s: %s" % (self.attribute.name, self.value_as_text)

    @property
    def value_as_text(self):
        """
        Returns a string representation of the attribute's value. To customise
        e.g. image attribute values, declare a _image_as_text property and
        return something appropriate.
        """
        property_name = '_%s_as_text' % self.attribute.type
        return getattr(self, property_name, self.value)

    @property
    def _richtext_as_text(self):
        return strip_tags(self.value)

    @property
    def _entity_as_text(self):
        """
        Returns the unicode representation of the related model. You likely
        want to customise this (and maybe _entity_as_html) if you use entities.
        """
        return six.text_type(self.value)

    @property
    def value_as_html(self):
        """
        Returns a HTML representation of the attribute's value. To customise
        e.g. image attribute values, declare a _image_as_html property and
        return e.g. an <img> tag.  Defaults to the _as_text representation.
        """
        property_name = '_%s_as_html' % self.attribute.type
        return getattr(self, property_name, self.value_as_text)

    @property
    def _richtext_as_html(self):
        return mark_safe(self.value)


@python_2_unicode_compatible
class ComputerCategory(models.Model):
    """
    Joining model between products and categories. Exists to allow customising.
    """
    computer = models.ForeignKey('computer.Computer', verbose_name=_("Computer"))
    category = models.ForeignKey('catalogue.Category', default=category_default,
                                 verbose_name=_("Category"))

    class Meta:
        app_label = 'computer'
        ordering = ['computer', 'category']
        unique_together = ('computer', 'category')
        verbose_name = _('Computer category')
        verbose_name_plural = _('Computer categories')

    def __str__(self):
        return u"<computercategory for computer '%s'>" % self.computer


class MissingComputerImage(object):
    """
    Mimics a Django file field by having a name property.

    sorl-thumbnail requires all it's images to be in MEDIA_ROOT. This class
    tries symlinking the default "missing image" image in STATIC_ROOT
    into MEDIA_ROOT for convenience, as that is necessary every time an Oscar
    project is setup. This avoids the less helpful NotFound IOError that would
    be raised when sorl-thumbnail tries to access it.
    """

    def __init__(self, name=None):
        self.name = name if name else settings.OSCAR_MISSING_IMAGE_URL
        media_file_path = os.path.join(settings.MEDIA_ROOT, self.name)
        # don't try to symlink if MEDIA_ROOT is not set (e.g. running tests)
        if settings.MEDIA_ROOT and not os.path.exists(media_file_path):
            self.symlink_missing_image(media_file_path)

    def symlink_missing_image(self, media_file_path):
        static_file_path = find('oscar/img/%s' % self.name)
        if static_file_path is not None:
            try:
                os.symlink(static_file_path, media_file_path)
            except OSError:
                raise ImproperlyConfigured((
                                               "Please copy/symlink the "
                                               "'missing image' image at %s into your MEDIA_ROOT at %s. "
                                               "This exception was raised because Oscar was unable to "
                                               "symlink it for you.") % (media_file_path,
                                                                         settings.MEDIA_ROOT))
            else:
                logging.info((
                                 "Symlinked the 'missing image' image at %s into your "
                                 "MEDIA_ROOT at %s") % (media_file_path,
                                                        settings.MEDIA_ROOT))


@python_2_unicode_compatible
class ComputerImage(models.Model):
    """
    An image of a product
    """
    computer = models.ForeignKey(
        'computer.Computer', related_name='images', verbose_name=_("Computer"))
    original = models.ImageField(
        _("Original"), upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255)
    caption = models.CharField(_("Caption"), max_length=200, blank=True)

    #: Use display_order to determine which is the "primary" image
    display_order = models.PositiveIntegerField(
        _("Display order"), default=0,
        help_text=_("An image with a display order of zero will be the primary"
                    " image for a product"))
    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)

    class Meta:
        app_label = 'computer'
        # Any custom models should ensure that this ordering is unchanged, or
        # your query count will explode. See AbstractProduct.primary_image.
        ordering = ["display_order"]
        unique_together = ("computer", "display_order")
        verbose_name = _('Computer image')
        verbose_name_plural = _('Computer images')

    def __str__(self):
        return u"Image of '%s'" % self.computer

    def is_primary(self):
        """
        Return bool if image display order is 0
        """
        return self.display_order == 0

    def delete(self, *args, **kwargs):
        """
        Always keep the display_order as consecutive integers. This avoids
        issue #855.
        """
        super(ComputerImage, self).delete(*args, **kwargs)
        for idx, image in enumerate(self.computer.images.all()):
            image.display_order = idx
            image.save()


@python_2_unicode_compatible
class ComputerHDD(models.Model):
    computer = models.ForeignKey('computer.Computer', verbose_name=_("Computer"))
    hdd = models.ForeignKey('catalogue.HDD', verbose_name=_("HDD"))
    count = models.IntegerField(default=0, help_text=_("How many HDDs computer have?"), verbose_name="Count of HDD")

    class Meta:
        ordering = ['computer', 'hdd']
        verbose_name = _('HDD of computer')
        verbose_name_plural = _('HDDs of computer')

    def __str__(self):
        return u"%s hdd of computer '%s'" % (self.count, self.computer)


@python_2_unicode_compatible
class ComputerODD(models.Model):
    computer = models.ForeignKey('computer.Computer', verbose_name=_("Computer"))
    odd = models.ForeignKey('catalogue.ODD', verbose_name=_("ODD"))
    count = models.IntegerField(default=0, help_text=_("How many ODDs computer have?"), verbose_name="Count of ODD")

    class Meta:
        ordering = ['computer', 'odd']
        verbose_name = _('ODD of computer')
        verbose_name_plural = _('ODDs of computer')

    def __str__(self):
        return u"%s odd of computer '%s'" % (self.count, self.computer)


@python_2_unicode_compatible
class ComputerPSU(models.Model):
    computer = models.ForeignKey('computer.Computer', verbose_name=_("Computer"))
    psu = models.ForeignKey('catalogue.PSU', verbose_name=_("PSU"))
    count = models.IntegerField(default=0, help_text=_("How many PSUs computer have?"), verbose_name="Count of PSU")

    class Meta:
        ordering = ['computer', 'psu']
        verbose_name = _('PSU of computer')
        verbose_name_plural = _('PSUs of computer')

    def __str__(self):
        return u"%s psu of computer '%s'" % (self.count, self.computer)


@python_2_unicode_compatible
class ComputerRAM(models.Model):
    computer = models.ForeignKey('computer.Computer', verbose_name=_("Computer"))
    ram = models.ForeignKey('catalogue.RAM', verbose_name=_("RAM"))
    count = models.IntegerField(default=0, help_text=_("How many RAMs computer have?"), verbose_name="Count of RAM")

    class Meta:
        ordering = ['computer', 'ram']
        verbose_name = _('RAM of computer')
        verbose_name_plural = _('RAMs of computer')

    def __str__(self):
        return u"%s ram of computer '%s'" % (self.count, self.computer)


@python_2_unicode_compatible
class ComputerSSD(models.Model):
    computer = models.ForeignKey('computer.Computer', verbose_name=_("Computer"))
    ssd = models.ForeignKey('catalogue.SSD', verbose_name=_("SSD"))
    count = models.IntegerField(default=0, help_text=_("How many SSDs computer have?"), verbose_name="Count of SSD")

    class Meta:
        ordering = ['computer', 'ssd']
        verbose_name = _('SSD of computer')
        verbose_name_plural = _('SSDs of computer')

    def __str__(self):
        return u"%s ssd of computer '%s'" % (self.count, self.computer)


@python_2_unicode_compatible
class ComputerVideoCard(models.Model):
    computer = models.ForeignKey('computer.Computer', verbose_name=_("Computer"))
    video_card = models.ForeignKey('catalogue.VideoCard', verbose_name=_("Video card"))
    count = models.IntegerField(default=0, help_text=_("How many video cards computer have?"),
                                verbose_name="Count of video card")

    class Meta:
        ordering = ['computer', 'video_card']
        verbose_name = _('Video card of computer')
        verbose_name_plural = _('Video cards of computer')

    def __str__(self):
        return u"%s video card of computer '%s'" % (self.count, self.computer)
