from django.db import models


class ComputerQuerySet(models.query.QuerySet):

    def base_queryset(self):
        """
        Applies select_related and prefetch_related for commonly related
        models to save on queries
        """
        return self.select_related('product_class')\
            .prefetch_related('product_options',
                              'product_class__options',
                              # 'stockrecords',
                              'images',
                              )

    def browsable(self):
        """
        Excludes non-canonical products.
        """
        return self.filter()


class ComputerManager(models.Manager):
    """
    Uses ProductQuerySet and proxies its methods to allow chaining

    Once Django 1.7 lands, this class can probably be removed:
    https://docs.djangoproject.com/en/dev/releases/1.7/#calling-custom-queryset-methods-from-the-manager  # noqa
    """

    def get_queryset(self):
        return ComputerQuerySet(self.model, using=self._db)

    def browsable(self):
        return self.get_queryset().browsable()

    def base_queryset(self):
        return self.get_queryset().base_queryset()


class BrowsableComputerManager(ComputerManager):
    """
    Excludes non-canonical products

    Could be deprecated after Oscar 0.7 is released
    """

    def get_queryset(self):
        return super(BrowsableComputerManager, self).get_queryset().browsable()


class OpenComputerManager(models.Manager):
    """For searching/creating OPEN baskets only."""
    status_filter = "Open"

    def get_queryset(self):
        return super(OpenComputerManager, self).get_queryset().filter(
            status=self.status_filter)

    def get_or_create(self, **kwargs):
        return self.get_queryset().get_or_create(
            status=self.status_filter, **kwargs)


class SavedComputerManager(models.Manager):
    """For searching/creating SAVED baskets only."""
    status_filter = "Saved"

    def get_queryset(self):
        return super(SavedComputerManager, self).get_queryset().filter(
            status=self.status_filter)

    def create(self, **kwargs):
        return self.get_queryset().create(status=self.status_filter, **kwargs)

    def get_or_create(self, **kwargs):
        return self.get_queryset().get_or_create(
            status=self.status_filter, **kwargs)
