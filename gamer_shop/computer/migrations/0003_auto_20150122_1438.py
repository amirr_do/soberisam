# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('computer', '0002_auto_20150115_1304'),
    ]

    operations = [
        migrations.AlterField(
            model_name='computer',
            name='case',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='Case of model', blank=True, to='catalogue.Case', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='cooler',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='Cooler of model', blank=True, to='catalogue.Cooler', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='cpu',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='CPU of model', blank=True, to='catalogue.CPU', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='motherboard',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='Motherboard of model', blank=True, to='catalogue.Motherboard', null=True),
            preserve_default=True,
        ),
    ]
