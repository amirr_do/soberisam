# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('computer', '0004_auto_20150428_1744'),
    ]

    operations = [
        migrations.AlterField(
            model_name='computer',
            name='hdd',
            field=models.ManyToManyField(blank=True, to='catalogue.HDD', related_query_name='computer_model', verbose_name='HDD of model', through='computer.ComputerHDD', related_name='computer_models'),
        ),
        migrations.AlterField(
            model_name='computer',
            name='odd',
            field=models.ManyToManyField(blank=True, to='catalogue.ODD', related_query_name='computer_model', verbose_name='ODD of model', through='computer.ComputerODD', related_name='computer_models'),
        ),
        migrations.AlterField(
            model_name='computer',
            name='psu',
            field=models.ManyToManyField(blank=True, to='catalogue.PSU', related_query_name='computer_model', verbose_name='PSU of model', through='computer.ComputerPSU', related_name='computer_models'),
        ),
        migrations.AlterField(
            model_name='computer',
            name='ram',
            field=models.ManyToManyField(blank=True, to='catalogue.RAM', related_query_name='computer_model', verbose_name='RAM of model', through='computer.ComputerRAM', related_name='computer_models'),
        ),
        migrations.AlterField(
            model_name='computer',
            name='ssd',
            field=models.ManyToManyField(blank=True, to='catalogue.SSD', related_query_name='computer_model', verbose_name='SSD of model', through='computer.ComputerSSD', related_name='computer_models'),
        ),
        migrations.AlterField(
            model_name='computer',
            name='video_card',
            field=models.ManyToManyField(blank=True, to='catalogue.VideoCard', related_query_name='computer_model', verbose_name='Video card of model', through='computer.ComputerVideoCard', related_name='computer_models'),
        ),
    ]
