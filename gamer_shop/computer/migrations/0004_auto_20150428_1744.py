# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gamer_shop.computer.models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('computer', '0003_auto_20150122_1438'),
    ]

    operations = [
        migrations.AlterField(
            model_name='computer',
            name='case',
            field=models.ForeignKey(related_query_name='computer_model', null=True, blank=True, to='catalogue.Case', on_delete=django.db.models.deletion.PROTECT, verbose_name='Case of model', related_name='computer_models'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='cooler',
            field=models.ForeignKey(related_query_name='computer_model', null=True, blank=True, to='catalogue.Cooler', on_delete=django.db.models.deletion.PROTECT, verbose_name='Cooler of model', related_name='computer_models'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='cpu',
            field=models.ForeignKey(related_query_name='computer_model', null=True, blank=True, to='catalogue.CPU', on_delete=django.db.models.deletion.PROTECT, verbose_name='CPU of model', related_name='computer_models'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='hdd',
            field=models.ManyToManyField(related_name='computer_models', null=True, blank=True, through='computer.ComputerHDD', to='catalogue.HDD', verbose_name='HDD of model', related_query_name='computer_model'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='motherboard',
            field=models.ForeignKey(related_query_name='computer_model', null=True, blank=True, to='catalogue.Motherboard', on_delete=django.db.models.deletion.PROTECT, verbose_name='Motherboard of model', related_name='computer_models'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='odd',
            field=models.ManyToManyField(related_name='computer_models', null=True, blank=True, through='computer.ComputerODD', to='catalogue.ODD', verbose_name='ODD of model', related_query_name='computer_model'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='product_class',
            field=models.ForeignKey(null=True, to='catalogue.ProductClass', on_delete=django.db.models.deletion.PROTECT, verbose_name='Product type', help_text='Choose what type of product this is', default=gamer_shop.computer.models.product_class_default, related_name='computers'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='psu',
            field=models.ManyToManyField(related_name='computer_models', null=True, blank=True, through='computer.ComputerPSU', to='catalogue.PSU', verbose_name='PSU of model', related_query_name='computer_model'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='ram',
            field=models.ManyToManyField(related_name='computer_models', null=True, blank=True, through='computer.ComputerRAM', to='catalogue.RAM', verbose_name='RAM of model', related_query_name='computer_model'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='ssd',
            field=models.ManyToManyField(related_name='computer_models', null=True, blank=True, through='computer.ComputerSSD', to='catalogue.SSD', verbose_name='SSD of model', related_query_name='computer_model'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='status',
            field=models.CharField(max_length=128, verbose_name='Status', default='Open', choices=[('Open', 'Open - currently active'), ('Saved', 'Saved - for items to be purchased later'), ('Frozen', 'Frozen - the basket cannot be modified')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computer',
            name='video_card',
            field=models.ManyToManyField(related_name='computer_models', null=True, blank=True, through='computer.ComputerVideoCard', to='catalogue.VideoCard', verbose_name='Video card of model', related_query_name='computer_model'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerattributevalue',
            name='value_file',
            field=models.FileField(max_length=255, null=True, blank=True, upload_to='images/products/%Y/%m/'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerattributevalue',
            name='value_image',
            field=models.ImageField(max_length=255, null=True, blank=True, upload_to='images/products/%Y/%m/'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computercategory',
            name='category',
            field=models.ForeignKey(to='catalogue.Category', verbose_name='Category', default=gamer_shop.computer.models.category_default),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerhdd',
            name='count',
            field=models.IntegerField(verbose_name='Count of HDD', help_text='How many HDDs computer have?', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerimage',
            name='original',
            field=models.ImageField(upload_to='images/products/%Y/%m/', verbose_name='Original', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerodd',
            name='count',
            field=models.IntegerField(verbose_name='Count of ODD', help_text='How many ODDs computer have?', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerpsu',
            name='count',
            field=models.IntegerField(verbose_name='Count of PSU', help_text='How many PSUs computer have?', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerram',
            name='count',
            field=models.IntegerField(verbose_name='Count of RAM', help_text='How many RAMs computer have?', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computerssd',
            name='count',
            field=models.IntegerField(verbose_name='Count of SSD', help_text='How many SSDs computer have?', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='computervideocard',
            name='count',
            field=models.IntegerField(verbose_name='Count of video card', help_text='How many video cards computer have?', default=0),
            preserve_default=True,
        ),
    ]
