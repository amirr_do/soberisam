# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import oscar.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0003_auto_20141219_1545'),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Computer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_description', models.TextField(verbose_name='Short Description', blank=True)),
                ('upc', oscar.models.fields.NullCharField(max_length=64, help_text='Universal Product Code (UPC) is an identifier for a product which is not specific to a particular  supplier. Eg an ISBN for a book.', unique=True, verbose_name='UPC')),
                ('title', models.CharField(max_length=255, verbose_name='Title', blank=True)),
                ('slug', models.SlugField(max_length=255, verbose_name='Slug')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('rating', models.FloatField(verbose_name='Rating', null=True, editable=False)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name='Date updated', db_index=True)),
                ('is_discountable', models.BooleanField(default=True, help_text='This flag indicates if this product can be used in an offer or not', verbose_name='Is discountable?')),
            ],
            options={
                'ordering': ['-date_created'],
                'verbose_name': 'Computer',
                'verbose_name_plural': 'Computers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerAttributeValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_text', models.TextField(null=True, verbose_name='Text', blank=True)),
                ('value_integer', models.IntegerField(null=True, verbose_name='Integer', blank=True)),
                ('value_boolean', models.NullBooleanField(verbose_name='Boolean')),
                ('value_float', models.FloatField(null=True, verbose_name='Float', blank=True)),
                ('value_richtext', models.TextField(null=True, verbose_name='Richtext', blank=True)),
                ('value_date', models.DateField(null=True, verbose_name='Date', blank=True)),
                ('value_file', models.FileField(max_length=255, null=True, upload_to=b'images/products/%Y/%m/', blank=True)),
                ('value_image', models.ImageField(max_length=255, null=True, upload_to=b'images/products/%Y/%m/', blank=True)),
                ('entity_object_id', models.PositiveIntegerField(null=True, editable=False, blank=True)),
                ('attribute', models.ForeignKey(verbose_name='Attribute', to='catalogue.ProductAttribute')),
                ('computer', models.ForeignKey(related_name='attribute_values', verbose_name='Computer', to='computer.Computer')),
                ('entity_content_type', models.ForeignKey(blank=True, editable=False, to='contenttypes.ContentType', null=True)),
                ('value_option', models.ForeignKey(verbose_name='Value option', blank=True, to='catalogue.AttributeOption', null=True)),
            ],
            options={
                'verbose_name': 'Computer attribute value',
                'verbose_name_plural': 'Computer attribute values',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.ForeignKey(verbose_name='Category', to='catalogue.Category')),
                ('computer', models.ForeignKey(verbose_name='Computer', to='computer.Computer')),
            ],
            options={
                'ordering': ['computer', 'category'],
                'verbose_name': 'Computer category',
                'verbose_name_plural': 'Computer categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerHDD',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=0, help_text='How many HDDs computer have?', verbose_name=b'Count of HDD')),
                ('computer', models.ForeignKey(verbose_name='Computer', to='computer.Computer')),
                ('hdd', models.ForeignKey(verbose_name='HDD', to='catalogue.HDD')),
            ],
            options={
                'ordering': ['computer', 'hdd'],
                'verbose_name': 'HDD of computer',
                'verbose_name_plural': 'HDDs of computer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('original', models.ImageField(upload_to=b'images/products/%Y/%m/', max_length=255, verbose_name='Original')),
                ('caption', models.CharField(max_length=200, verbose_name='Caption', blank=True)),
                ('display_order', models.PositiveIntegerField(default=0, help_text='An image with a display order of zero will be the primary image for a product', verbose_name='Display order')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('computer', models.ForeignKey(related_name='images', verbose_name='Computer', to='computer.Computer')),
            ],
            options={
                'ordering': ['display_order'],
                'verbose_name': 'Computer image',
                'verbose_name_plural': 'Computer images',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerODD',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=0, help_text='How many ODDs computer have?', verbose_name=b'Count of ODD')),
                ('computer', models.ForeignKey(verbose_name='Computer', to='computer.Computer')),
                ('odd', models.ForeignKey(verbose_name='ODD', to='catalogue.ODD')),
            ],
            options={
                'ordering': ['computer', 'odd'],
                'verbose_name': 'ODD of computer',
                'verbose_name_plural': 'ODDs of computer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerPSU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=0, help_text='How many PSUs computer have?', verbose_name=b'Count of PSU')),
                ('computer', models.ForeignKey(verbose_name='Computer', to='computer.Computer')),
                ('psu', models.ForeignKey(verbose_name='PSU', to='catalogue.PSU')),
            ],
            options={
                'ordering': ['computer', 'psu'],
                'verbose_name': 'PSU of computer',
                'verbose_name_plural': 'PSUs of computer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerRAM',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=0, help_text='How many RAMs computer have?', verbose_name=b'Count of RAM')),
                ('computer', models.ForeignKey(verbose_name='Computer', to='computer.Computer')),
                ('ram', models.ForeignKey(verbose_name='RAM', to='catalogue.RAM')),
            ],
            options={
                'ordering': ['computer', 'ram'],
                'verbose_name': 'RAM of computer',
                'verbose_name_plural': 'RAMs of computer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerSSD',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=0, help_text='How many SSDs computer have?', verbose_name=b'Count of SSD')),
                ('computer', models.ForeignKey(verbose_name='Computer', to='computer.Computer')),
                ('ssd', models.ForeignKey(verbose_name='SSD', to='catalogue.SSD')),
            ],
            options={
                'ordering': ['computer', 'ssd'],
                'verbose_name': 'SSD of computer',
                'verbose_name_plural': 'SSDs of computer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComputerVideoCard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=0, help_text='How many video cards computer have?', verbose_name=b'Count of video card')),
                ('computer', models.ForeignKey(verbose_name='Computer', to='computer.Computer')),
                ('video_card', models.ForeignKey(verbose_name='Video card', to='catalogue.VideoCard')),
            ],
            options={
                'ordering': ['computer', 'video_card'],
                'verbose_name': 'Video card of computer',
                'verbose_name_plural': 'Video cards of computer',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='computerimage',
            unique_together=set([('computer', 'display_order')]),
        ),
        migrations.AlterUniqueTogether(
            name='computercategory',
            unique_together=set([('computer', 'category')]),
        ),
        migrations.AlterUniqueTogether(
            name='computerattributevalue',
            unique_together=set([('attribute', 'computer')]),
        ),
        migrations.AddField(
            model_name='computer',
            name='attributes',
            field=models.ManyToManyField(help_text='A product attribute is something that this product may have, such as a size, as specified by its class', to='catalogue.ProductAttribute', verbose_name='Attributes', through='computer.ComputerAttributeValue'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='case',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='Case of model', to='catalogue.Case'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='categories',
            field=models.ManyToManyField(to='catalogue.Category', verbose_name='Categories', through='computer.ComputerCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='cooler',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='Cooler of model', to='catalogue.Cooler'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='cpu',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='CPU of model', to='catalogue.CPU'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='hdd',
            field=models.ManyToManyField(related_query_name=b'computer_model', related_name='computer_models', to='catalogue.HDD', through='computer.ComputerHDD', blank=True, null=True, verbose_name='HDD of model'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='motherboard',
            field=models.ForeignKey(related_query_name=b'computer_model', related_name='computer_models', on_delete=django.db.models.deletion.PROTECT, verbose_name='Motherboard of model', to='catalogue.Motherboard'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='odd',
            field=models.ManyToManyField(related_query_name=b'computer_model', related_name='computer_models', to='catalogue.ODD', through='computer.ComputerODD', blank=True, null=True, verbose_name='ODD of model'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='product_class',
            field=models.ForeignKey(related_name='computers', on_delete=django.db.models.deletion.PROTECT, verbose_name='Product type', to='catalogue.ProductClass', help_text='Choose what type of product this is', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='product_options',
            field=models.ManyToManyField(help_text="Options are values that can be associated with a item when it is added to a customer's basket.  This could be something like a personalised message to be printed on a T-shirt.", to='catalogue.Option', verbose_name='Product options', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='psu',
            field=models.ManyToManyField(related_query_name=b'computer_model', related_name='computer_models', to='catalogue.PSU', through='computer.ComputerPSU', blank=True, null=True, verbose_name='PSU of model'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='ram',
            field=models.ManyToManyField(related_query_name=b'computer_model', related_name='computer_models', to='catalogue.RAM', through='computer.ComputerRAM', blank=True, null=True, verbose_name='RAM of model'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='ssd',
            field=models.ManyToManyField(related_query_name=b'computer_model', related_name='computer_models', to='catalogue.SSD', through='computer.ComputerSSD', blank=True, null=True, verbose_name='SSD of model'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='video_card',
            field=models.ManyToManyField(related_query_name=b'computer_model', related_name='computer_models', to='catalogue.VideoCard', through='computer.ComputerVideoCard', blank=True, null=True, verbose_name='Video card of model'),
            preserve_default=True,
        ),
    ]
