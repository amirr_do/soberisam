# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('computer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='computer',
            name='owner',
            field=models.ForeignKey(related_name='computers', verbose_name='Owner', to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='computer',
            name='status',
            field=models.CharField(default=b'Open', max_length=128, verbose_name='Status', choices=[(b'Open', 'Open - currently active'), (b'Saved', 'Saved - for items to be purchased later'), (b'Frozen', 'Frozen - the basket cannot be modified')]),
            preserve_default=True,
        ),
    ]
