from oscar.templatetags.basket_tags import *
# from django import template
# from oscar.core.loading import get_model
#
# from oscar.core.loading import get_class

AddComputerToBasketForm = get_class('basket.forms', 'AddComputerToBasketForm')
SimpleAddComputerToBasketForm = get_class('basket.forms', 'SimpleAddComputerToBasketForm')
AddToBuildForm = get_class('build.forms', 'AddToBuildForm')
SimpleAddToBuildForm = get_class('build.forms', 'SimpleAddToBuildForm')
Computer = get_model('computer', 'Computer')

# register = template.Library()
#
# QNT_SINGLE, QNT_MULTIPLE = 'single', 'multiple'


@register.assignment_tag()
def computer_basket_form(request, computer, quantity_type='single'):
    if not isinstance(computer, Computer):
        return ''

    initial = {}
    initial['computer_id'] = computer.id

    form_class = AddComputerToBasketForm
    if quantity_type == QNT_SINGLE:
        form_class = SimpleAddComputerToBasketForm

    form = form_class(request.basket, computer=computer, initial=initial)

    return form


@register.assignment_tag()
def build_form(request, product, quantity_type='single'):
    if not isinstance(product, Product):
        return ''

    initial = {}
    if not product.is_parent:
        initial['product_id'] = product.id

    form_class = AddToBuildForm
    if quantity_type == QNT_SINGLE:
        form_class = SimpleAddToBuildForm

    form = form_class(request.computer, product=product, initial=initial)

    return form
