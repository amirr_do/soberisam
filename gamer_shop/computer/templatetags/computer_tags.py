from django import template
from django.template.loader import select_template

from oscar.core.loading import get_model

register = template.Library()

Computer = get_model('computer', 'Computer')
Product = get_model('catalogue', 'Product')


@register.simple_tag(takes_context=True)
def render_computer(context, computer):
    """
    Render a product snippet as you would see in a browsing display.

    This templatetag looks for different templates depending on the UPC and
    product class of the passed product.  This allows alternative templates to
    be used for different product classes.
    """
    if not computer:
        # Search index is returning products that don't exist in the
        # database...
        return ''

    names = ['computer/partials/product/upc-%s.html' % computer.upc,
             'computer/partials/product/class-%s.html'
             % computer.get_product_class().slug,
             'computer/partials/product.html']
    template_ = select_template(names)
    # Ensure the passed product is in the context as 'product'
    context['computer'] = computer
    return template_.render(context)


@register.simple_tag(takes_context=True)
def render_computer_or_product(context, item):
    """
    Render a product snippet as you would see in a browsing display.

    This templatetag looks for different templates depending on the UPC and
    product class of the passed product.  This allows alternative templates to
    be used for different product classes.
    """
    if not item:
        # Search index is returning products that don't exist in the
        # database...
        return ''

    if item.__class__ == Product:
        names = ['catalogue/partials/product/upc-%s.html' % item.upc,
                 'catalogue/partials/product/class-%s.html'
                 % item.get_product_class().slug,
                 'catalogue/partials/product.html']
        context['product'] = item
    elif item.__class__ == Computer:
        names = ['computer/partials/product/upc-%s.html' % item.upc,
                 'computer/partials/product/class-%s.html'
                 % item.get_product_class().slug,
                 'computer/partials/product.html']
        context['computer'] = item
    else:
        return ''
    template_ = select_template(names)
    # Ensure the passed product is in the context as 'product'
    return template_.render(context)
