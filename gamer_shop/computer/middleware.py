from django.conf import settings
from django.core.signing import Signer, BadSignature
from django.utils.functional import SimpleLazyObject, empty

from oscar.core.loading import get_model
from oscar.core.loading import get_class

# Applicator = get_class('offer.utils', 'Applicator')
Computer = get_model('computer', 'Computer')
Selector = get_class('partner.strategy', 'Selector')

selector = Selector()


class ComputerMiddleware(object):
    # Middleware interface methods

    def process_request(self, request):
        # Keep track of cookies that need to be deleted (which can only be done
        # when we're processing the response instance).
        request.cookies_to_delete = []

        # Load stock/price strategy and assign to request (it will later be
        # assigned to the computer too).
        strategy = selector.strategy(request=request, user=request.user)
        request.strategy = strategy

        # We lazily load the computer so use a private variable to hold the
        # cached instance.
        request._computer_cache = None

        def load_full_computer():
            """
            Return the computer after applying offers.
            """
            computer = self.get_computer(request)
            computer.strategy = request.strategy
            # self.apply_offers_to_computer(request, computer)

            return computer

        def load_computer_hash():
            """
            Load the computer and return the computer hash

            Note that we don't apply offers or check that every line has a
            stockrecord here.
            """
            computer = self.get_computer(request)
            if computer.id:
                return self.get_computer_hash(computer.id)

        # Use Django's SimpleLazyObject to only perform the loading work
        # when the attribute is accessed.
        request.computer = SimpleLazyObject(load_full_computer)
        # print('Computer.id = {}'.format(request.computer.id))
        request.computer_hash = SimpleLazyObject(load_computer_hash)

    def process_template_response(self, request, response):
        if hasattr(response, 'context_data'):
            if response.context_data is None:
                response.context_data = {}
            if 'computer' not in response.context_data:
                response.context_data['computer'] = request.computer
            else:
                # Occasionally, a view will want to pass an alternative computer
                # to be rendered.  This can happen as part of checkout
                # processes where the submitted computer is frozen when the
                # customer is redirected to another site (eg PayPal).  When the
                # customer returns and we want to show the order preview
                # template, we need to ensure that the frozen computer gets
                # rendered (not request.computer).  We still keep a reference to
                # the request computer (just in case).
                response.context_data['request_computer'] = request.computer
        return response

    def process_response(self, request, response):
        # Delete any surplus cookies
        cookies_to_delete = getattr(request, 'cookies_to_delete', [])
        for cookie_key in cookies_to_delete:
            response.delete_cookie(cookie_key)

        if not hasattr(request, 'computer'):
            return response

        # If the computer was never initialized we can safely return
        if isinstance(request.computer, SimpleLazyObject) and request.computer._wrapped is empty:
            return response

        cookie_key = self.get_cookie_key()
        # Check if we need to set a cookie. If the cookies is already available
        # but is set in the cookies_to_delete list then we need to re-set it.
        has_computer_cookie = cookie_key in request.COOKIES and cookie_key not in cookies_to_delete

        # If a computer has had products added to it, but the user is anonymous
        # then we need to assign it to a cookie

        # If a computer has had products added to it, but the user is anonymous
        # then we need to assign it to a cookie
        if (request.computer.id and (not request.user.is_authenticated() or request.user.is_staff) and
                not has_computer_cookie):
            cookie = self.get_computer_hash(request.computer.id)
            response.set_cookie(
                    cookie_key, cookie,
                    max_age=settings.OSCAR_BASKET_COOKIE_LIFETIME, httponly=True)
        return response

    @staticmethod
    def get_cookie_key():
        return settings.OSCAR_COMPUTER_COOKIE_OPEN

    # Helper methods

    def get_computer(self, request):
        """
        Return the open computer for this request
        """
        # if request._computer_cache is not None and request._computer_cache.id is not None:
        if request._computer_cache is not None:
            return request._computer_cache

        manager = Computer.open
        cookie_key = self.get_cookie_key()
        cookie_computer = self.get_cookie_computer(cookie_key, request)

        if hasattr(request, 'user') and request.user.is_authenticated():
            # Signed-in user: if they have a cookie computer too, it means
            # that they have just signed in and we need to merge their cookie
            # computer into their user computer, then delete the cookie.
            if request.user.is_staff:
                if cookie_computer:
                    computer = cookie_computer
                else:
                    computer = Computer()
            else:
                try:
                    computer = manager.get(owner=request.user)
                    if cookie_computer and computer != cookie_computer and not cookie_computer.owner:
                        self.freeze_or_delete(computer)
                        computer = cookie_computer
                        computer.owner = request.user
                        computer.save()
                except Computer.DoesNotExist:
                    computer = self.cookie_computer_not_owner(cookie_computer, manager)
                    computer.owner = request.user
                    computer.save()
                except Computer.MultipleObjectsReturned:
                    computer = self.cookie_computer_not_owner(cookie_computer, manager)
                    computer.owner = request.user
                    computer.save()
                    # Not sure quite how we end up here with multiple computers.
                    # We merge them and create a fresh one

                    old_computers = list(manager.filter(owner=request.user))
                    for old_computer in old_computers:
                        self.freeze_or_delete(old_computer)

                # Assign user onto computer to prevent further SQL queries when
                # computer.owner is accessed.
                if cookie_computer:
                    request.cookies_to_delete.append(cookie_key)

        elif cookie_computer:
            # Anonymous user with a computer tied to the cookie
            computer = cookie_computer
        else:
            # Anonymous user with no computer - instantiate a new computer
            # instance.  No need to save yet.
            # we need to.
            # computer = Computer.objects.create()
            computer = Computer()

        # Cache computer instance for the during of this request
        request._computer_cache = computer

        return computer

    @staticmethod
    def freeze_or_delete(computer):
        if computer.is_full:
            computer.freeze()
        else:
            computer.delete()

    @staticmethod
    def cookie_computer_not_owner(cookie_computer, manager):
        if cookie_computer and not cookie_computer.owner:
            return cookie_computer
        return manager.create()

    @staticmethod
    def get_cookie_computer(cookie_key, request):
        """
        Looks for a computer which is referenced by a cookie.

        If a cookie key is found with no matching computer, then we add
        it to the list to be deleted.
        """
        computer = None
        if cookie_key in request.COOKIES:
            computer_hash = request.COOKIES[cookie_key]
            try:
                computer_id = Signer().unsign(computer_hash)
                computer = Computer.objects.get(pk=computer_id,
                                                status=Computer.OPEN)
            except (BadSignature, Computer.DoesNotExist):
                request.cookies_to_delete.append(cookie_key)
        return computer

    @staticmethod
    def get_computer_hash(computer_id):
        return Signer().sign(computer_id)
