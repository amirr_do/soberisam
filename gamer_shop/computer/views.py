import json
from collections import defaultdict

from django.contrib import messages
from django.contrib.auth import login
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import InvalidPage
from django.core.signing import Signer
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.http import urlquote
from django.http import HttpResponsePermanentRedirect, HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import DetailView, TemplateView, FormView, UpdateView
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_class, get_model, get_classes
# from oscar.apps.catalogue.signals import product_viewed
from gamer_shop import settings
from gamer_shop.mixins.mixins import DateContextMixin

PageTitleMixin = get_class('customer.mixins', 'PageTitleMixin')
EmailAuthenticationForm, EmailUserCreationForm = get_classes('customer.forms', ['EmailUserCreationForm',
                                                                                'EmailAuthenticationForm'])
AccountAuthView = get_class('customer.views', 'AccountAuthView')
# GatewayForm = get_class('computer.forms', 'GatewayForm')
ProductAttribute = get_model('catalogue', 'ProductAttribute')
Product = get_model('catalogue', 'Product')
Computer = get_model('computer', 'computer')
ComputerImage = get_model('computer', 'ComputerImage')
ComputerAttributeValue = get_model('computer', 'ComputerAttributeValue')
ComputerCategory = get_model('computer', 'ComputerCategory')
ComputerHDD = get_model('computer', 'ComputerHDD')
ComputerSSD = get_model('computer', 'ComputerSSD')
ComputerODD = get_model('computer', 'ComputerODD')
ComputerRAM = get_model('computer', 'ComputerRAM')
ComputerVideoCard = get_model('computer', 'ComputerVideoCard')
ComputerPSU = get_model('computer', 'ComputerPSU')
ComputerReview = get_model('reviews_computer', 'ComputerReview')
Category = get_model('catalogue', 'category')
ComputerAlert = get_model('customer', 'ComputerAlert')
ComputerAlertForm = get_class('customer.forms', 'ComputerAlertForm')
get_computer_search_handler_class = get_class(
        'computer.search_handlers', 'get_computer_search_handler_class')
ComputerRamFormSet = get_class('computer.forms', 'ComputerRamFormSet')
ComputerHDDFormSet = get_class('computer.forms', 'ComputerHDDFormSet')
ComputerSSDFormSet = get_class('computer.forms', 'ComputerSSDFormSet')
ComputerVideoCardFormSet = get_class('computer.forms', 'ComputerVideoCardFormSet')
ComputerODDFormSet = get_class('computer.forms', 'ComputerODDFormSet')
ComputerPSUFormSet = get_class('computer.forms', 'ComputerPSUFormSet')
ComputerForm = get_class('computer.forms', 'ComputerForm')


class ComputerMixins(object):
    def create_new_computer(self, computer_id, owner, add_builds=True):
        computer_new = Computer.objects.get(pk=computer_id)

        if add_builds:
            hdd = ComputerHDD.objects.filter(computer=computer_new)
            ssd = ComputerSSD.objects.filter(computer=computer_new)
            odd = ComputerODD.objects.filter(computer=computer_new)
            ram = ComputerRAM.objects.filter(computer=computer_new)
            video = ComputerVideoCard.objects.filter(computer=computer_new)
            psu = ComputerPSU.objects.filter(computer=computer_new)
            builds = (hdd, ssd, odd, ram, video, psu)
        else:
            builds = tuple

        socket = ComputerAttributeValue.objects.get(computer=computer_new,
                                                    attribute=ProductAttribute.objects.get(code='socket_computer'))
        computer_images = ComputerImage.objects.filter(computer=computer_new)

        computer_new.pk = None
        computer_new.status = 'Open'
        computer_new.owner = owner
        computer_new.save()
        computer_new.title = 'Сборка №{}'.format(computer_new.id)
        computer_new.save()

        for i, image in enumerate(computer_images):
            image_name = 'build{}_{}.{}'.format(i, computer_new.pk, image.original.name.split('.')[-1])
            image_file = image.original.file

            computer_image = ComputerImage.objects.create(computer=computer_new, display_order=i)
            computer_image.original.save(image_name, image_file)

        if add_builds:
            self.clone_computer_builds(builds, computer_new)

        socket.pk = None
        socket.computer = computer_new
        socket.save()
        computer_id = computer_new.pk

        msgs = []
        msgs.append('* Create new build: "Build #{}"'.format(computer_id))
        clean_msgs = [m.replace('* ', '') for m in msgs if m.startswith('* ')]
        messages.success(self.request, ",".join(clean_msgs))

        return computer_new

    @staticmethod
    def clone_computer_builds(product_type, computer):
        for products in product_type:
            products = product_type[products] if isinstance(product_type, dict) else products
            for product in products:
                product.pk = None
                product.computer = computer
                product.save()


class AjaxMixin(object):
    def json_response(self, ctx, messages=None, redirect_url=None):
        if ctx:
            login_html = render_to_string(
                    'customer/log_reg.html',
                    RequestContext(self.request, ctx))
        else:
            login_html = ''
        messages_html = render_to_string('partials/alert_messages.html',
                                         RequestContext(self.request, {'messages': messages}))
        content_json = {
            'content_html': login_html,
            'messages_html': messages_html,
            'redirect_url': redirect_url}
        return HttpResponse(json.dumps(content_json),
                            content_type="application/json")


class AuthView(AccountAuthView, AjaxMixin):
    computer = None

    def get(self, request, *args, **kwargs):
        raise Http404

    def validate_login_form(self):
        form = self.get_login_form(bind_data=True)
        if self.request.is_ajax():
            if form.is_valid():
                user = form.get_user()

                login(self.request, form.get_user())
                self.computer = self.request.computer
                self.computer.status = 'Saved'
                self.computer.owner = user
                self.computer.save()

                msg = self.get_login_success_message(form)
                messages.success(self.request, msg)

                redirect_url = self.get_login_success_url(form)
                return self.json_response({}, None, redirect_url)

            ctx = self.get_context_data(login_form=form)
            return self.json_response(ctx)
        else:
            raise Http404

    def validate_registration_form(self):
        form = self.get_registration_form(bind_data=True)
        if self.request.is_ajax():
            if form.is_valid():
                self.register_user(form)
                self.computer = self.request.computer
                self.computer.status = 'Saved'
                self.computer.owner = self.request.user
                self.computer.save()

                msg = self.get_registration_success_message(form)
                messages.success(self.request, msg)

                redirect_url = self.get_registration_success_url(form)
                return self.json_response({}, None, redirect_url)

            ctx = self.get_context_data(registration_form=form)
            return self.json_response(ctx)
        else:
            raise Http404

    def get_registration_success_url(self, form):
        return self.computer.get_absolute_url()

    def get_login_success_url(self, form):
        return self.computer.get_absolute_url()


class ComputerDetailView(DetailView, DateContextMixin):
    context_object_name = 'computer'
    model = Computer
    # view_signal = product_viewed
    template_folder = "computer"

    # Whether to redirect to the URL with the right path
    enforce_paths = True

    # Whether to redirect child products to their parent's URL
    enforce_parent = True

    def get(self, request, **kwargs):
        """
        Ensures that the correct URL is used before rendering a response
        """
        self.object = computer = self.get_object()

        redirect = self.redirect_if_necessary(request.path, computer)
        if redirect is not None:
            return redirect

        response = super(ComputerDetailView, self).get(request, **kwargs)
        # self.send_signal(request, response, computer)
        return response

    def get_object(self, queryset=None):
        # Check if self.object is already set to prevent unnecessary DB calls
        if hasattr(self, 'object'):
            return self.object
        else:
            return super(ComputerDetailView, self).get_object(queryset)

    def redirect_if_necessary(self, current_path, computer):
        if self.enforce_paths:
            expected_path = computer.get_absolute_url()
            if expected_path != urlquote(current_path):
                return HttpResponsePermanentRedirect(expected_path)

    def get_context_data(self, **kwargs):
        ctx = super(ComputerDetailView, self).get_context_data(**kwargs)
        ctx['reviews'] = self.get_reviews()
        ctx['alert_form'] = self.get_alert_form()
        ctx['has_active_alert'] = self.get_alert_status()
        categories = Category.objects.filter(slug__contains='solarwind')
        ctx["categories_menu"] = categories

        ctx.update(self.get_date_context())
        return ctx

    def get_alert_status(self):
        # Check if this user already have an alert for this product
        has_alert = False
        if self.request.user.is_authenticated():
            alerts = ComputerAlert.objects.filter(
                    computer=self.object, user=self.request.user,
                    status=ComputerAlert.ACTIVE)
            has_alert = alerts.exists()
        return has_alert

    def get_alert_form(self):
        return ComputerAlertForm(
                user=self.request.user, computer=self.object)

    def get_reviews(self):
        return self.object.reviews.filter(status=ComputerReview.APPROVED)

    def send_signal(self, request, response, product):
        self.view_signal.send(
                sender=self, product=product, user=request.user, request=request,
                response=response)

    def get_template_names(self):
        """
        Return a list of possible templates.

        If an overriding class sets a template name, we use that. Otherwise,
        we try 2 options before defaulting to catalogue/detail.html:
            1). detail-for-upc-<upc>.html
            2). detail-for-class-<classname>.html

        This allows alternative templates to be provided for a per-product
        and a per-item-class basis.
        """
        if self.template_name:
            return [self.template_name]

        return [
            '%s/detail-for-upc-%s.html' % (
                self.template_folder, self.object.upc),
            '%s/detail-for-class-%s.html' % (
                self.template_folder, self.object.get_product_class().slug),
            '%s/detail.html' % (self.template_folder)]


class PreSaveLoginView(FormView):
    # form_class = GatewayForm
    computer = None

    def get_success_url(self):
        return self.computer.get_absolute_url()

    def get_form_kwargs(self):
        kwargs = super(PreSaveLoginView, self).get_form_kwargs()
        email = None
        if email:
            kwargs['initial'] = {
                'username': email,
            }
        return kwargs

    def form_valid(self, form):
        if form.is_new_account_checkout():
            email = form.cleaned_data['username']

            if form.is_new_account_checkout():
                messages.info(
                        self.request,
                        _("Create your account and then you will be redirected "
                          "back to the checkout process"))
                self.success_url = "%s?next=%s&email=%s" % (
                    reverse('customer:register'),
                    reverse('checkout:shipping-address'),
                    email
                )
        else:
            user = form.get_user()
            login(self.request, user)
            self.computer = self.request.computer
            self.computer.status = 'Saved'
            self.computer.owner = user
            self.computer.save()

        return redirect(self.get_success_url())


class LoginFormView(AjaxMixin):
    login_form_class = EmailAuthenticationForm
    registration_form_class = EmailUserCreationForm
    login_prefix, registration_prefix = 'login', 'registration'
    redirect_field_name = 'next'

    def get_login_form(self, bind_data=False):
        return self.login_form_class(
                **self.get_login_form_kwargs(bind_data))

    def get_login_form_kwargs(self, bind_data=False):
        kwargs = {}
        kwargs['host'] = self.request.get_host()
        kwargs['prefix'] = self.login_prefix
        kwargs['initial'] = {
            'redirect_url': self.request.GET.get(self.redirect_field_name, ''),
        }
        if bind_data and self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def get_registration_form(self, bind_data=False):
        return self.registration_form_class(
                **self.get_registration_form_kwargs(bind_data))

    def get_registration_form_kwargs(self, bind_data=False):
        kwargs = {}
        kwargs['host'] = self.request.get_host()
        kwargs['prefix'] = self.registration_prefix
        kwargs['initial'] = {
            'redirect_url': self.request.GET.get(self.redirect_field_name, ''),
        }
        if bind_data and self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs


class ComputerEditView(UpdateView, PageTitleMixin, LoginFormView, ComputerMixins):
    model = Computer
    form_class = ComputerForm
    template_name = 'computer/edit.html'
    active_tab = 'edit_build'

    def get_page_title(self):
        return u'%s %s' % (_('Edit build'), self.request.computer.get_title())

    def get_object(self, queryset=None):
        return self.request.computer

    def get_success_url(self, computer=None):
        if computer:
            return computer.get_absolute_url()
        return self.get_object().get_absolute_url()

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        ram_formset = self.get_form(ComputerRamFormSet)
        hdd_formset = self.get_form(ComputerHDDFormSet)
        ssd_formset = self.get_form(ComputerSSDFormSet)
        video_card_formset = self.get_form(ComputerVideoCardFormSet)
        odd_formset = self.get_form(ComputerODDFormSet)
        psu_formset = self.get_form(ComputerPSUFormSet)
        # form = self.get_login_form(GatewayForm)
        return self.render_to_response(self.get_context_data(
                hdd_formset=hdd_formset, ram_formset=ram_formset,
                ssd_formset=ssd_formset, video_card_formset=video_card_formset,
                odd_formset=odd_formset, psu_formset=psu_formset))

    def get_context_data(self, **kwargs):
        """
        Insert the form into the context dict.
        """
        kwargs.setdefault('form', self.get_form())
        return super(ComputerEditView, self).get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action', '')
        if action == 'clear':
            self.get_object().cleaning()

            # TODO update_index

            if self.request.is_ajax():
                redirect_url = HttpResponseRedirect(reverse('computer:edit'))
                return self.json_response(None, redirect_url=redirect_url.url)
            else:
                return HttpResponseRedirect(reverse('computer:edit'))

        if 'delete' in action:
            product = Product.objects.get(pk=int(action.split('-')[1]))
            self.get_object().delete_product(product)

            # TODO update_index

            if self.request.is_ajax():
                redirect_url = HttpResponseRedirect(reverse('computer:edit'))
                return self.json_response(None, redirect_url=redirect_url.url)
            else:
                return HttpResponseRedirect(reverse('computer:edit'))

        self.object = self.get_object()
        ram_formset = self.get_form(ComputerRamFormSet)
        hdd_formset = self.get_form(ComputerHDDFormSet)
        ssd_formset = self.get_form(ComputerSSDFormSet)
        video_card_formset = self.get_form(ComputerVideoCardFormSet)
        odd_formset = self.get_form(ComputerODDFormSet)
        psu_formset = self.get_form(ComputerPSUFormSet)

        is_valid = self.build_is_valid(request, psu_formset, ram_formset, hdd_formset, ssd_formset)

        if not (self.object.is_full and
                request.strategy.fetch_for_computer(self.object).availability.is_available_to_buy):
            messages.error(request, _('One of details is unavailable.'))
            is_valid = False

        if not is_valid:
            if self.request.is_ajax():
                return self.json_response(None, messages)
            else:
                return HttpResponseRedirect(reverse('computer:edit'))

        hardware = [ram_formset, hdd_formset, ssd_formset, video_card_formset, odd_formset, psu_formset]

        formsets_valid = all((ram_formset.is_valid(), hdd_formset.is_valid(), ssd_formset.is_valid(),
                              video_card_formset.is_valid(), odd_formset.is_valid(), psu_formset.is_valid()))

        if formsets_valid:
            if request.user.is_staff:
                form = self.get_form()
                return self.form_valid(hardware, form) if form.is_valid() else self.form_invalid(hardware)
            else:
                return self.form_valid(hardware)
        else:
            return self.form_invalid(hardware)

    def build_is_valid(self, request, psu, ram, hdd, ssd):
        is_valid = True
        if not self.object.cpu:
            messages.error(request, _('None CPU.'))
            is_valid = False
        if not self.object.case:
            messages.error(request, _('None case.'))
            is_valid = False
        else:
            if not (psu.cleaned_data or self.object.case.attr.power):
                messages.error(request, _('None psu.'))
                is_valid = False
        if not self.object.motherboard:
            messages.error(request, _('None motherboard.'))
            is_valid = False
        if not ram.cleaned_data:
            messages.error(request, _('None RAM.'))
            is_valid = False
        if not (hdd.cleaned_data or ssd.cleaned_data):
            messages.error(request, _('None HDD or SSD.'))
            is_valid = False

        return is_valid

    def form_invalid(self, hardware, form=None):
        ram_formset, hdd_formset, ssd_formset, video_card_formset, odd_formset, psu_formset = hardware
        return self.render_to_response(self.get_context_data(
                hdd_formset=hdd_formset, ram_formset=ram_formset,
                ssd_formset=ssd_formset, video_card_formset=video_card_formset,
                odd_formset=odd_formset, psu_formset=psu_formset))

    def form_valid(self, hardware, form=None):
        """
        If the form is valid, save the associated model.
        """
        for h in hardware:
            h.save()

        if self.request.user.is_staff and form:
            computer = form.save(commit=False)
            if form.cleaned_data.get('save_as_staff', False):
                computer.owner = self.request.user
            ComputerCategory.objects.filter(computer=computer).delete()
            for computer_category in form.cleaned_data.get('categories', list()):
                ComputerCategory.objects.get_or_create(computer=computer, category=computer_category)
        else:
            computer = self.get_object()
            if self.request.user.is_authenticated():
                ComputerCategory.objects.get_or_create(computer=computer,
                                                       category=Category.objects.get(slug='user_builds'))

        computer.status = 'Saved'
        computer.save()

        # TODO update_index

        if self.request.POST.get('action', '') == 'buy':
            self.request.basket.add_computer(computer, 1, None)
            cache.delete(make_template_fragment_key('basket_total', [self.request.basket_hash]))
            if self.request.is_ajax():
                redirect_url = HttpResponseRedirect(reverse('basket:summary'))
                return self.json_response(None, redirect_url=redirect_url.url)
            else:
                return HttpResponseRedirect(reverse('basket:summary'))

        if self.request.is_ajax():
            redirect_url = HttpResponseRedirect(self.get_success_url())
            return self.json_response(None, redirect_url=redirect_url.url)
        else:
            return HttpResponseRedirect(self.get_success_url())


class ComputerPreEditView(TemplateView, ComputerMixins):
    @staticmethod
    def get_computer_hash(computer_id):
        return Signer().sign(computer_id)

    @staticmethod
    def get_cookie_key():
        return settings.OSCAR_COMPUTER_COOKIE_OPEN

    def post(self, request, *args, **kwargs):

        response = HttpResponseRedirect(self.get_success_url())

        user = request.user
        computer_id = kwargs.get('pk')

        self.edit_request_computer(request.computer, user)

        if not user.is_authenticated():
            computer_id = self.create_new_computer(computer_id, None).id
            self.set_cookie(response, computer_id)
        elif user.is_staff:
            create_new_computer = request.POST.get('create', False)
            if create_new_computer:
                owner = Computer.objects.get(pk=computer_id).owner
                edit_computer = self.create_new_computer(computer_id, owner)
            else:
                edit_computer = Computer.objects.get(pk=computer_id)
                edit_computer.status = 'Open'
                edit_computer.save()
            self.set_cookie(response, edit_computer.id)
        else:
            self.create_new_computer(computer_id, user)

        # TODO update_index

        return response

    def edit_request_computer(self, request_computer, user):
        try:
            request_computer = Computer.objects.get(pk=request_computer.id)
            if user.is_authenticated():
                if user.is_staff:
                    if request_computer.owner == user:
                        self.freeze_or_delete(request_computer)
                    else:
                        request_computer.status = 'Saved'
                        request_computer.save()
                else:
                    self.freeze_or_delete(request_computer)
            else:
                request_computer.delete()
        except ObjectDoesNotExist:
            pass

    def set_cookie(self, response, computer_id):
        cookie_key = self.get_cookie_key()
        cookie = self.get_computer_hash(computer_id)
        return response.set_cookie(
                cookie_key, cookie,
                max_age=settings.OSCAR_BASKET_COOKIE_LIFETIME, httponly=True)

    @staticmethod
    def freeze_or_delete(computer):
        if computer.is_full:
            computer.freeze()
        else:
            computer.delete()

    @staticmethod
    def get_success_url():
        return reverse('computer:edit')


class CatalogueView(TemplateView):
    """
    Browse all products in the catalogue
    """
    context_object_name = "computers"
    template_name = 'computer/browse.html'

    # def get_queryset(self):
    #     return Computer.saved.all()

    def get(self, request, *args, **kwargs):
        try:
            self.search_handler = self.get_search_handler(
                    self.request.GET, request.get_full_path(), [])
        except InvalidPage:
            # Redirect to page one.
            messages.error(request, _('The given page number was invalid.'))
            return redirect('computer:index')
        return super(CatalogueView, self).get(request, *args, **kwargs)

    def get_search_handler(self, *args, **kwargs):
        return get_computer_search_handler_class()(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {}
        ctx['summary'] = _("User computers")
        search_context = self.search_handler.get_search_context_data(
                self.context_object_name)
        ctx.update(search_context)
        return ctx


class ComputerTypeView(TemplateView, DateContextMixin):
    """
    Browse products in a given category
    """
    context_object_name = "computers"
    template_name = 'computer/category.html'
    date = {'year': None, 'month': None, 'day': None}
    enforce_paths = True

    def get(self, request, *args, **kwargs):
        # Fetch the category; return 404 or redirect as needed
        self.category = self.get_category()

        try:
            self.search_handler = self.get_search_handler(
                    request.GET, request.get_full_path(), self.get_categories(), self.date)
        except InvalidPage:
            messages.error(request, _('The given page number was invalid.'))
            return redirect(self.category.get_absolute_url())

        return super(ComputerTypeView, self).get(request, *args, **kwargs)

    def get_category(self):
        if 'pk' in self.kwargs:
            # Usual way to reach a category page. We just look at the primary
            # key in case the slug changed. If it did, get() will redirect
            # appropriately
            filters = {'pk': self.kwargs['pk']}
        else:
            # For SEO reasons, we allow chopping off bits of the URL. If that
            # happened, no primary key will be available.
            filters = {'slug': self.kwargs['category_slug']}
        return get_object_or_404(Category, **filters)

    def redirect_if_necessary(self, current_path, category):
        if self.enforce_paths:
            # Categories are fetched by primary key to allow slug changes.
            # If the slug has changed, issue a redirect.
            expected_path = category.get_absolute_url()
            if expected_path != urlquote(current_path):
                return HttpResponsePermanentRedirect(expected_path)

    def get_search_handler(self, *args, **kwargs):
        return get_computer_search_handler_class()(*args, **kwargs)

    def get_categories(self):
        """
        Return a list of the current category and its ancestors
        """
        return self.category.get_descendants_and_self()

    def get_context_data(self, **kwargs):
        context = super(ComputerTypeView, self).get_context_data(**kwargs)
        context['category'] = self.category
        search_context = self.search_handler.get_search_context_data(
                self.context_object_name)
        context.update(search_context)

        categories = Category.objects.filter(slug__contains='solarwind')
        context["categories_menu"] = categories

        context.update(self.get_date_context())

        return context


class ComputerDateView(ComputerTypeView):
    year = None
    month = None
    day = None
    date = {'year': None, 'month': None, 'day': None}

    def get(self, request, *args, **kwargs):
        if 'year' in self.kwargs:
            self.year = self.kwargs['year']
        if 'month' in self.kwargs:
            self.month = self.kwargs['month']
        if 'day' in self.kwargs:
            self.day = self.kwargs['day']
        self.date = {
            'year': self.year,
            'month': self.month,
            'day': self.day,
        }
        return super(ComputerDateView, self).get(request, *args, **kwargs)


class MyComputerTypeView(ComputerTypeView):
    def get(self, request, *args, **kwargs):
        if not (request.user and request.user.pk):
            return HttpResponseRedirect(reverse('computer:all:type2', kwargs={'category_slug': 'user_builds'}))

        return super(MyComputerTypeView, self).get(request, *args, **kwargs)

    def get_category(self):
        return Category.objects.get(slug='user_builds')

    def get_search_handler(self, *args, **kwargs):
        return get_computer_search_handler_class()(user=self.request.user, *args, **kwargs)


class MyComputerDateView(ComputerDateView, MyComputerTypeView):
    pass
