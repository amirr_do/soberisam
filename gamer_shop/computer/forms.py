from django import forms
from django.forms import inlineformset_factory
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_model

Line = get_model('basket', 'line')
ComputerLine = get_model('basket', 'ComputerLine')
Basket = get_model('basket', 'basket')
Product = get_model('catalogue', 'product')
Category = get_model('catalogue', 'Category')
ComputerRam = get_model('computer', 'ComputerRam')
ComputerHDD = get_model('computer', 'ComputerHDD')
ComputerSSD = get_model('computer', 'ComputerSSD')
ComputerODD = get_model('computer', 'ComputerODD')
ComputerPSU = get_model('computer', 'ComputerPSU')
ComputerVideoCard = get_model('computer', 'ComputerVideoCard')
Computer = get_model('computer', 'Computer')


class ComputerForm(forms.ModelForm):
    save_as_staff = forms.BooleanField(required=False, label=_('Save computer with staff owner'))

    class Meta:
        model = Computer
        fields = ('title', 'slug', 'description', 'categories',  'save_as_staff',)

    def __init__(self, **kwargs):
        super(ComputerForm, self).__init__(**kwargs)
        self.fields['categories'].queryset = Category.objects.exclude(path__startswith='0001')


class ProductLineForm(forms.ModelForm):
    max_qty = 4
    product = None

    class Meta:
        model = None
        fields = ('count',)

    def clean_count(self):
        qty = self.cleaned_data['count']
        if qty < 1 or qty > self.max_qty:
            raise forms.ValidationError(_('Max quantity of {} = {}.'.format(self.product, self.max_qty)))
        return qty


class RamLineForm(ProductLineForm):
    product = 'RAM'

    class Meta:
        model = ComputerRam
        fields = ('count',)


class HddLineForm(ProductLineForm):
    product = 'HDD'

    class Meta:
        model = ComputerHDD
        fields = ('count',)


class SsdLineForm(ProductLineForm):
    product = 'SSD'

    class Meta:
        model = ComputerSSD
        fields = ('count',)


class OddLineForm(ProductLineForm):
    product = 'ODD'

    class Meta:
        model = ComputerODD
        fields = ('count',)


class PsuLineForm(ProductLineForm):
    product = 'PSU'

    class Meta:
        model = ComputerPSU
        fields = ('count',)


class VideoCardLineForm(ProductLineForm):
    product = 'Video card'

    class Meta:
        model = ComputerVideoCard
        fields = ('count',)


ComputerRamFormSet = inlineformset_factory(Computer, ComputerRam, form=RamLineForm, extra=0, can_delete=True)
ComputerHDDFormSet = inlineformset_factory(Computer, ComputerHDD, form=HddLineForm, extra=0, can_delete=True)
ComputerSSDFormSet = inlineformset_factory(Computer, ComputerSSD, form=SsdLineForm, extra=0, can_delete=True)
ComputerODDFormSet = inlineformset_factory(Computer, ComputerODD, form=OddLineForm, extra=0, can_delete=True)
ComputerPSUFormSet = inlineformset_factory(Computer, ComputerPSU, form=PsuLineForm, extra=0, can_delete=True)
ComputerVideoCardFormSet = inlineformset_factory(
    Computer, ComputerVideoCard, form=VideoCardLineForm, extra=0, can_delete=True)
