# -*- coding: utf-8 -*-
from django.db.models.signals import pre_delete
from django.conf import settings

from oscar.core.loading import get_model

Computer = get_model('computer', 'Computer')
ComputerHDD = get_model('computer', 'ComputerHDD')
ComputerSSD = get_model('computer', 'ComputerSSD')
ComputerODD = get_model('computer', 'ComputerODD')
ComputerRAM = get_model('computer', 'ComputerRAM')
ComputerVideoCard = get_model('computer', 'ComputerVideoCard')
ComputerPSU = get_model('computer', 'ComputerPSU')


def delete_computer_parts(sender, instance, **kwargs):
    ComputerRAM.objects.filter(computer=instance).delete()
    ComputerVideoCard.objects.filter(computer=instance).delete()
    ComputerHDD.objects.filter(computer=instance).delete()
    ComputerSSD.objects.filter(computer=instance).delete()
    ComputerODD.objects.filter(computer=instance).delete()
    ComputerPSU.objects.filter(computer=instance).delete()

sender = Computer
pre_delete.connect(delete_computer_parts, sender=sender)

if settings.OSCAR_DELETE_IMAGE_FILES:

    from oscar.core.loading import get_model

    from django.db import models
    from django.db.models.signals import post_delete

    from sorl import thumbnail
    from sorl.thumbnail.helpers import ThumbnailError

    ComputerImage = get_model('computer', 'ComputerImage')

    def delete_image_files(sender, instance, **kwargs):
        """
        Deletes the original image, created thumbnails, and any entries
        in sorl's key-value store.
        """
        image_fields = (models.ImageField, thumbnail.ImageField)
        for field in instance._meta.fields:
            if isinstance(field, image_fields):
                # Make Django return ImageFieldFile instead of ImageField
                fieldfile = getattr(instance, field.name)
                try:
                    thumbnail.delete(fieldfile)
                except ThumbnailError:
                    pass

    # connect for all models with ImageFields - add as needed
    models_with_images = [ComputerImage]
    for sender in models_with_images:
        post_delete.connect(delete_image_files, sender=sender)
