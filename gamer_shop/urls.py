from django.conf.urls import include, url
from django.contrib import admin
from oscar.app import application
from django.conf import settings
from django.conf.urls.static import static
from gamer_shop.computer.app import application as computer_app
from gamer_shop.build.app import application as build_app
from gamer_shop.customer.app import callback_app
from gamer_shop.catalogue.views import update_index_view


urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),

    # The Django admin is not officially supported; expect breakage.
    # Nonetheless, it's often useful for debugging.
    url(r'^admin/', include(admin.site.urls)),

    url(r'^computer/', include(computer_app.urls)),
    url(r'^build/', include(build_app.urls)),
    url(r'^callback/', include(callback_app.urls)),
    url(r'^dashboard/catalogue/update_index/$', update_index_view),

    url(r'', include(application.urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
