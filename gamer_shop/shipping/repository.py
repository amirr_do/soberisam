from oscar.apps.shipping.repository import *
from django.utils.translation import ugettext_lazy as _


class NoShipping(shipping_methods.NoShippingRequired):
    """
    This is a special shipping method that indicates that no shipping is
    actually required (eg for digital goods).
    """
    code = 'no-shipping-kiev'
    name = _('No shipping Kiev')


class ShippingKiev(shipping_methods.FixedPrice):
    """
    This is a special shipping method that indicates that no shipping is
    actually required (eg for digital goods).
    """
    code = 'shipping-kiev'
    name = _('Shipping Kiev')


class ShippingNovaPoshta(shipping_methods.FixedPrice):
    """
    This is a special shipping method that indicates that no shipping is
    actually required (eg for digital goods).
    """
    code = 'shipping-nova-poshta'
    name = _('Shipping Nova Poshta')


class Repository(Repository):

    def get_available_shipping_methods(
            self, basket, shipping_addr=None, **kwargs):
        """
        Return a list of all applicable shipping method instances for a given
        basket, address etc. This method is intended to be overridden.
        """
        return NoShipping(), ShippingKiev(50, 50), ShippingNovaPoshta(20, 20)
