from oscar.apps.shipping import config


class ShippingConfig(config.ShippingConfig):
    name = 'gamer_shop.shipping'
