from django.db import models
from django.utils.translation import ugettext_lazy as _
from oscar.apps.address.abstract_models import AbstractPartnerAddress


class PartnerAddress(AbstractPartnerAddress):
    def clean(self):
        # Strip all whitespace
        for field in ['first_name', 'last_name', 'line1', 'line2', 'line3',
                      'line4', 'state', 'postcode']:
            if self.__dict__[field]:
                self.__dict__[field] = self.__dict__[field].strip()

# from oscar.apps.partner.models import *  # noqa
import django

from oscar.core.loading import is_model_registered
from oscar.apps.address.abstract_models import AbstractPartnerAddress
from oscar.apps.partner.abstract_models import (
    AbstractPartner, AbstractStockRecord, AbstractStockAlert)

__all__ = []


if not is_model_registered('partner', 'Partner'):
    class Partner(AbstractPartner):
        pass

    __all__.append('Partner')


if not is_model_registered('partner', 'StockRecord'):
    class StockRecord(AbstractStockRecord):

        def save(self, *args, **kwargs):
            super(StockRecord, self).save(*args, **kwargs)
            self.product.save()

    __all__.append('StockRecord')


if not is_model_registered('partner', 'StockAlert'):
    class StockAlert(AbstractStockAlert):
        pass

    __all__.append('StockAlert')


class ExchangeRates(models.Model):
    rate = models.DecimalField(_('Exchange Rate'), decimal_places=2, max_digits=12,
                               blank=False, null=False)
    partner = models.ForeignKey(
        'partner.Partner', verbose_name=_("Partner"),
        related_name='exchange_rates')
    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)


if django.VERSION < (1, 7):
    from . import receivers  # noqa
