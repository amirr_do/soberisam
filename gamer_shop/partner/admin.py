from oscar.apps.partner.admin import *  # noqa
from gamer_shop.partner.models import ExchangeRates


class ExchangeRatesAdmin(admin.ModelAdmin):
    list_display = ('partner', 'rate', 'date_created')

admin.site.register(ExchangeRates, ExchangeRatesAdmin)
