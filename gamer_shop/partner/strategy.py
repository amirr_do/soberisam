from decimal import Decimal as D
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from oscar.apps.partner import strategy, prices, availability
from oscar.core.loading import get_model

ExchangeRates = get_model('partner', 'ExchangeRates')
Partner = get_model('partner', 'Partner')


class Selector(object):

    def strategy(self, request=None, user=None, **kwargs):
        return ComputerStrategy(request)


class UseSoberisamStockRecord(object):
    """
    Stockrecord selection mixin for use with the ``Structured`` base strategy.
    This mixin picks the first (normally only) stockrecord to fulfil a product.

    This is backwards compatible with Oscar<0.6 where only one stockrecord per
    product was permitted.
    """

    def select_stockrecord(self, product):
        try:
            # return product.stockrecords.get(partner=Partner.objects.get(code='pending'))
            return product.stockrecords.get(partner=Partner.objects.get(code='soberisam'))
        except ObjectDoesNotExist:
            return None


class ComputerStrategy(UseSoberisamStockRecord, strategy.NoTax,
                       strategy.StockRequired, strategy.Structured):

    def fetch_for_computer(self, computer):
        component_stock = self.select_component_stockrecords(computer)
        return strategy.PurchaseInfo(
            price=self.computer_pricing_policy(computer, component_stock),
            availability=self.computer_availability_policy(computer, component_stock),
            stockrecord=None
        )

    def fetch_for_computer_line(self, line):
        return self.fetch_for_computer(line.computer)

    def select_component_stockrecords(self, computer):
        records = []
        components = {
            'case': [(computer.case, 1)],
            'cooler': [(computer.cooler, 1)],
            'cpu': [(computer.cpu, 1)],
            'motherboard': [(computer.motherboard, 1)],
            'hdd': [],
            'ram': [],
            'video_card': [],
            'ssd': [],
            'odd': [],
            'psu': [],
        }
        for hdd in computer.computerhdd_set.all():
            components['hdd'].append((hdd.hdd, hdd.count))
        for ram in computer.computerram_set.all():
            components['ram'].append((ram.ram, ram.count))
        for video_card in computer.computervideocard_set.all():
            components['video_card'].append((video_card.video_card, video_card.count))
        for ssd in computer.computerssd_set.all():
            components['ssd'].append((ssd.ssd, ssd.count))
        for odd in computer.computerodd_set.all():
            components['odd'].append((odd.odd, odd.count))
        for psu in computer.computerpsu_set.all():
            components['psu'].append((psu.psu, psu.count))
        for key in components.keys():
            for component in components[key]:
                if component[0] is not None:
                    records.append((component[0], self.select_stockrecord(component[0]), component[1]))
        return records

    def computer_pricing_policy(self, computer, component_stock):
        stockrecords = [(x[1], x[2]) for x in component_stock if x[1] is not None]
        if not stockrecords:
            return prices.Unavailable()
        else:
            currency = stockrecords[0][0].price_currency
            excl_tax = 0

        for stockrecord in stockrecords:
            if stockrecord[0] is None or stockrecord[0].price_currency != currency:
                return prices.Unavailable()
            else:
                excl_tax += stockrecord[0].price_excl_tax * stockrecord[1]
        return prices.FixedPrice(
            currency=currency,
            excl_tax=excl_tax * self.rate_partner('soberisam') + settings.BUILD_COST,
            tax=D('0.00')
        )

    def computer_availability_policy(self, computer, component_stock):
        if not component_stock:
            return availability.Unavailable()
        else:
            try:
                currency = component_stock[0][1].price_currency
            except AttributeError:
                return availability.Unavailable()
        for product, stockrecord, count in component_stock:
            try:
                currency = component_stock[0][1].price_currency
                if stockrecord.price_currency != currency:
                    return availability.Unavailable()
                policy = self.availability_policy(computer, stockrecord)
            except AttributeError:
                return availability.Unavailable()
            if not policy.is_available_to_buy:
                return availability.Unavailable()
        return availability.Available()

    def rate_partner(self, partner_code):
        exchange_rate = ExchangeRates.objects.filter(partner__code=partner_code).latest('date_created')
        if not exchange_rate:
            exchange_rate = ExchangeRates.objects.filter(partner__code='brain').latest('date_created')
        return exchange_rate.rate

    def pricing_policy(self, product, stockrecord):
        # Check stockrecord has the appropriate data
        if not stockrecord or stockrecord.price_excl_tax is None:
            return prices.Unavailable()
        return prices.FixedPrice(
            currency=stockrecord.price_currency,
            excl_tax=stockrecord.price_excl_tax * self.rate_partner('soberisam'),
            tax=D('0.00'))
