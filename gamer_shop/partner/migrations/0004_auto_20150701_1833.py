# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0003_exchangerates'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='partner',
            options={'verbose_name': 'Fulfillment partner', 'permissions': (('dashboard_access', 'Can access dashboard'),), 'verbose_name_plural': 'Fulfillment partners'},
        ),
        migrations.AlterField(
            model_name='partner',
            name='users',
            field=models.ManyToManyField(verbose_name='Users', blank=True, to=settings.AUTH_USER_MODEL, related_name='partners'),
        ),
    ]
