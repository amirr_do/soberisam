# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0002_auto_20141007_2032'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExchangeRates',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('rate', models.DecimalField(max_digits=12, verbose_name='Exchange Rate', decimal_places=2)),
                ('date_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('partner', models.ForeignKey(verbose_name='Partner', to='partner.Partner', related_name='exchange_rates')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
