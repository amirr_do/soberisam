from django import forms
from django.utils.translation import ugettext_lazy as _

from oscar.apps.dashboard.catalogue.forms import ProductSearchForm as BaseProductSearchForm
from oscar.apps.dashboard.catalogue.forms import *

Partner = get_model('partner', 'Partner')


class ProductSearchForm(BaseProductSearchForm):
    partner = forms.ChoiceField(required=False, label=_('Partner'))

    def __init__(self, *args, **kwargs):
        super(ProductSearchForm, self).__init__(*args, **kwargs)
        partners = [(0, '----------------'), ]
        partners += Partner.objects.values_list('id', 'name')
        self.fields['partner'].choices = partners
