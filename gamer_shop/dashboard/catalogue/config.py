from oscar.apps.dashboard.catalogue import config


class CatalogueDashboardConfig(config.CatalogueDashboardConfig):
    name = 'gamer_shop.dashboard.catalogue'
