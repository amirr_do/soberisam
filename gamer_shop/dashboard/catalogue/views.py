from oscar.apps.dashboard.catalogue.views import *
from oscar.apps.dashboard.catalogue.views import ProductListView as BaseProductListView


class ProductListView(BaseProductListView):

    def apply_search(self, queryset):
        qs = super(ProductListView, self).apply_search(queryset)
        data = self.form.cleaned_data
        partner = data.get('partner', 0)
        if partner and int(partner):
            stockrecord = StockRecord.objects.filter(partner=partner).distinct().values_list('product_id', flat=True)
            qs = qs.filter(id__in=stockrecord)
        return qs
