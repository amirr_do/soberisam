"use strict";

var gulp = require('gulp'),
    prefix = require('gulp-autoprefixer'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    minifyCSS = require('gulp-minify-css'),
    uncss = require('gulp-uncss'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass');

gulp.task('css', function () {
    gulp.src('gamer_shop/static/soberisam/sass/styles.sass')
        .pipe(sass({indentedSyntax: true}))
        .pipe(prefix('last 15 versions'))
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest('gamer_shop/public/static/dev/css'));
});

gulp.task('css_prod', function () {
    gulp.src('gamer_shop/static/soberisam/sass/styles.sass')
        .pipe(sass({indentedSyntax: true}))
        .pipe(prefix('last 15 versions'))
        .pipe(minifyCSS())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest('gamer_shop/public/static/dev/css'));
});

gulp.task('font', function () {
    gulp.src('gamer_shop/static/soberisam/fonts/*')
        .pipe(gulp.dest('gamer_shop/public/static/dev/fonts'));
    gulp.src('gamer_shop/static/soberisam/favicon.ico')
        .pipe(gulp.dest('gamer_shop/public/static/dev'));
});


gulp.task('js', function () {
    gulp.src('gamer_shop/static/soberisam/js/**/*.js')
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(gulp.dest('gamer_shop/public/static/dev/js'));
});

gulp.task('watch', function () {
    gulp.watch('gamer_shop/static/soberisam/sass/*.sass', ['css']);
    gulp.watch('gamer_shop/static/soberisam/sass/*.scss', ['css']);
    gulp.watch('gamer_shop/static/soberisam/js/**/*.js', ['js']);

});

gulp.task('develop', ['css', 'js']);

gulp.task('production', ['css_prod', 'js', 'font']);

gulp.task('default', ['develop', 'watch']);